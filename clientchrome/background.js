var db = openDB();
//	TODO: call before executing a query
//populateCookiesAndExtensions();
populateExtensions();

executeSplitPig();

function executeSplitPig()
{
		makeQueryRequestAndGetQueries(analystId);
		setTimeout(executeSplitPig, QUERY_REQUEST_INTERVAL * 1000);
}


function processURL(url, ref)
{
	url = fixChars(url);
	ref = fixChars(ref);
	
	var domain = url.substring(url.indexOf("://") + 3);
	domain = domain.substring(0, domain.indexOf("/"));

	var date = new Date();
	var timestamp = Math.round(date.getTime()/1000.0);
//	console.log("date: " + date + ", timestamp: " + timestamp);
//	console.log("url: " + url);
//	console.log("domain: " + domain);

	var historyItem = getHistoryItem(url, ref, domain, date, timestamp);
	
	// XXX: insert historyItem into a local database
	insertHistoryItem(db, historyItem);

	var searchItem = getSearchItem(url, domain, date, timestamp);
	if (searchItem != null)
	{
		insertSearchItem(db, searchItem);
	}
	
}

chrome.extension.onMessage.addListener(
  function(request, sender, sendResponse) {
//	console.log(sender.tab ? "from a content script: " + sender.tab.url : "from the extension");
	var ref = "";
    if (request.msg == "referrer")
    {
    	ref = request.content;
    	sendResponse({value: "background page ack: " + sender.tab.url});
    }
    
   	processURL(sender.tab.url.toLowerCase(), ref.toLowerCase());

});

function processCookieEvent(cookieEventItem)
{
	insertCookieEventItem(db, cookieEventItem);
}

function processExtensionEvent(extensionEventItem)
{
	insertExtensionEventItem(db, extensionEventItem);
}

function populateCookiesAndExtensions()
{
	// XXX: DISABLED
//	populateCookies();
	populateExtensions();
}

function populateExtensions()
{
	var extensionList = getAllExtensions();
	insertExtensions(db, extensionList);	
}

/*
function populateCookies()
{
	var cookieList = getAllCookies();
	insertCookies(db, cookieList);

}

chrome.cookies.onChanged.addListener(function(info) {
	//console.log("onChanged: " + info.cause.toUpperCase() + " " + JSON.stringify(info));

	var cookieEventItem = getCookieEventItem(info);
	processCookieEvent(cookieEventItem);

});
*/

chrome.management.onDisabled.addListener(function(info) {
	var extensionEventItem = getExtensionEventItem("disable", info);
	processExtensionEvent(extensionEventItem);

});

chrome.management.onEnabled.addListener(function(info) {
	var extensionEventItem = getExtensionEventItem("enable", info);
	processExtensionEvent(extensionEventItem);

});

chrome.management.onInstalled.addListener(function(info) {
	var extensionEventItem = getExtensionEventItem("install", info);
	processExtensionEvent(extensionEventItem);

});

chrome.management.onUninstalled.addListener(function(id) {
	var extensionEventItem = getExtensionEventItem("uninstall", {"id":id});
	processExtensionEvent(extensionEventItem);

});
