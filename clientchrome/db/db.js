function openDB()
{
	var db = openDatabase('split-pig_db', '2.0', 'Local Database for Split PIG', 5 * 1024 * 1024);
	initDB(db);
	
	return db;
}

function executeSelect(db, aid, versionId, query, callbackFunction)
{
	db.transaction(function (tx) {
		tx.analystId = aid;
		tx.versionId = versionId;
		tx.query = query;
		tx.executeSql(query.sql, [], callbackFunction);
	});
}

function initDB(db)
{
	db.transaction(function (tx) {
		var searchTableSQL = 'CREATE TABLE IF NOT EXISTS SearchItems ' + 
			'(search_item_id INTEGER PRIMARY KEY AUTOINCREMENT, search_date, search_timestamp, search_engine, search_term, search_type)';
		
		var historyTableSQL = 'CREATE TABLE IF NOT EXISTS HistoryItems ' +
			'(history_item_id INTEGER PRIMARY KEY AUTOINCREMENT, history_date, history_timestamp, history_domain, ' +
			'history_url, history_referrer, history_referrer_domain, history_method_finding, ' +
			'history_referrer_search_engine, history_referrer_search_term, history_referrer_search_type, history_referrer_search_rank)';
		
//		var cookieTableSQL = 'CREATE TABLE IF NOT EXISTS Cookies ' +
//			'(cookie_domain, cookie_name, cookie_store_id, cookie_value, cookie_session, ' +
//			'cookie_host_only, cookie_expiration_date, cookie_path, cookie_http_only, cookie_secure)';
		
//		var cookieEventTableSQL = 'CREATE TABLE IF NOT EXISTS CookieEventItems ' +
//			'(cookie_event_item_id INTEGER PRIMARY KEY AUTOINCREMENT, cookie_event_date, cookie_event_timestamp, ' +
//			'cookie_event_cause, cookie_domain, cookie_name, cookie_store_id, cookie_value, cookie_session, cookie_host_only, ' +
//			'cookie_expiration_date, cookie_path, cookie_http_only, cookie_secure, cookie_removed)';
		
		var extensionTableSQL = 'CREATE TABLE IF NOT EXISTS Extensions ' +
			'(extension_id, extension_name, extension_enabled, extension_homepage_url, extension_host_permissions, ' +
			'extension_install_type, extension_is_app, extension_may_disable, extension_offline_enabled, extension_permissions, ' +
			'extension_type, extension_version)';
		
		var extensionEventTableSQL = 'CREATE TABLE IF NOT EXISTS ExtensionEventItems ' +
			'(extension_event_item_id INTEGER PRIMARY KEY AUTOINCREMENT, extension_event_date, extension_event_timestamp, ' +
			'extension_event_type, extension_name, extension_id, extension_enabled, extension_homepage_url, extension_host_permissions, ' +
			'extension_install_type, extension_is_app, extension_may_disable, extension_offline_enabled, extension_permissions, ' +
			'extension_type, extension_version)';
		
		tx.executeSql(searchTableSQL);
		tx.executeSql(historyTableSQL);
//		tx.executeSql(cookieTableSQL);
//		tx.executeSql(cookieEventTableSQL);
		tx.executeSql(extensionTableSQL);
		tx.executeSql(extensionEventTableSQL);
	});
}

function insertSearchItem(db, searchItem)
{
	db.transaction(function (tx) {
		var insertSearchSQL = 'INSERT INTO SearchItems (search_date, search_timestamp, search_engine, search_term, search_type) ' +
					'VALUES (?, ?, ?, ?, ?)';
		var itemParams = [];
		for (var k in searchItem)
		{
			itemParams.push(searchItem[k]);
		}
		tx.executeSql(insertSearchSQL, itemParams);
	});
}

function insertHistoryItem(db, historyItem)
{
	db.transaction(function (tx) {
		var insertHistorySQL = 'INSERT INTO HistoryItems (history_date, history_timestamp, history_domain, history_url, history_referrer, ' +
					'history_referrer_domain, history_method_finding, history_referrer_search_engine, history_referrer_search_term, ' +
					'history_referrer_search_type, history_referrer_search_rank) ' +
					'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
		var itemParams = [];
		for (var k in historyItem)
		{
			itemParams.push(historyItem[k]);
		}
		tx.executeSql(insertHistorySQL, itemParams);
	});

}

function insertCookies(db, cookieList)
{
	db.transaction(function (tx) {
		var deleteCookiesSQL = 'DELETE FROM Cookies';
		tx.executeSql(deleteCookiesSQL);
		for (var i = 0; i < cookieList.length; i++)
		{
			var cookie = cookieList[i];
			var insertCookieSQL = 'INSERT INTO Cookies (cookie_domain, cookie_name, cookie_store_id, cookie_value, cookie_session, ' +
			'cookie_host_only, cookie_expiration_date, cookie_path, cookie_http_only, cookie_secure) ' +
				'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
				var itemParams = [];
				for (var k in cookie)
				{
					itemParams.push(cookie[k]);
				}
			tx.executeSql(insertCookieSQL, itemParams);
		}
	});
	
}

function insertCookieEventItem(db, cookieEventItem)
{
	db.transaction(function (tx) {
		var insertCookieEventSQL = 'INSERT INTO CookieEventItems (cookie_event_date, cookie_event_timestamp, ' +
			'cookie_event_cause, cookie_domain, cookie_name, cookie_store_id, cookie_value, cookie_session, cookie_host_only, ' +
			'cookie_expiration_date, cookie_path, cookie_http_only, cookie_secure, cookie_removed) ' +
			'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
		var itemParams = [];
		for (var k in cookieEventItem)
		{
			itemParams.push(cookieEventItem[k]);
		}
		tx.executeSql(insertCookieEventSQL, itemParams);
	});
}

function insertExtensions(db, extensionList)
{
	db.transaction(function (tx) {
		var deleteExtensionsSQL = 'DELETE FROM Extensions';
		tx.executeSql(deleteExtensionsSQL);
		for (var i = 0; i < extensionList.length; i++)
		{
			var extension = extensionList[i];
			var insertExtensionSQL = 'INSERT INTO Extensions (extension_id, extension_name, ' +
				'extension_enabled, extension_homepage_url, extension_host_permissions, ' +
				'extension_install_type, extension_is_app, extension_may_disable, extension_offline_enabled, extension_permissions, ' +
				'extension_type, extension_version) ' +
				'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
				var itemParams = [];
				for (var k in extension)
				{
					itemParams.push(extension[k]);
				}
			tx.executeSql(insertExtensionSQL, itemParams);
		}
	});
}

function insertExtensionEventItem(db, extensionEventItem)
{
	db.transaction(function (tx) {
		var insertExtensionEventSQL = 'INSERT INTO ExtensionEventItems (extension_event_date, extension_event_timestamp, ' +
			'extension_event_type, extension_name, extension_id, extension_enabled, extension_homepage_url, extension_host_permissions, ' +
			'extension_install_type, extension_is_app, extension_may_disable, extension_offline_enabled, extension_permissions, ' +
			'extension_type, extension_version) ' +
			'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
		var itemParams = [];
		for (var k in extensionEventItem)
		{
			itemParams.push(extensionEventItem[k]);
		}
		tx.executeSql(insertExtensionEventSQL, itemParams);
	});	
}

