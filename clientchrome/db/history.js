function getHistoryItem(url, ref, domain, date, timestamp)
{
	var historyItem = new Object();

	// XXX: visited sites and how they were found
	var methodFinding = "DIRECT";

	var refDomain = "";
	var refSearchEngineUsed = "";
	var refSearchTerm = "";
	var refSearchType = "";
	var refSearchRank = "";

	if (ref != "")
	{
		methodFinding = "REFERRAL";
		
		// XXX: search engine from referrer
		refDomain = ref.substring(ref.indexOf("://") + 3);
		refDomain = refDomain.substring(0, refDomain.indexOf("/"));
//		console.log("referrer: " + ref);
//		console.log("referrer domain: " + refDomain);
		refSearchEngineUsed = getSearchEngine(refDomain);
		
		if (refSearchEngineUsed != "")
		{
			methodFinding = "SEARCH";
			var refSearchParams = ref.substring(ref.indexOf(refDomain) + refDomain.length);
			if (refSearchParams.indexOf("?") != -1)
			{
				refSearchParams = refSearchParams.substring(refSearchParams.indexOf("?") + 1);
			}
			else if (refSearchParams.indexOf("#") != -1)
			{
				refSearchParams = refSearchParams.substring(refSearchParams.indexOf("#") + 1);
			}
//			console.log("ref search params: " + refSearchParams);
			
			var refSearchArgs = getSearchArguments(ref, refSearchEngineUsed, refSearchParams);
			
			refSearchTerm = refSearchArgs.searchTerm;
			refSearchType = refSearchArgs.searchType;
			refSearchRank = refSearchArgs.searchRank;
//			console.log("ref search term: " + refSearchTerm + ", ref search type: " + refSearchType + ", ref search rank: " + refSearchRank);
		}
	}

	historyItem.date = date;
	historyItem.timestamp = timestamp;
	historyItem.domain = domain;
	historyItem.url = url;
	historyItem.referrer = ref;
	historyItem.refDomain = refDomain;
	historyItem.methodFinding = methodFinding;
	historyItem.refSearchEngineUsed = refSearchEngineUsed;
	historyItem.refSearchTerm = refSearchTerm;
	historyItem.refSearchType = refSearchType;
	historyItem.refSearchRank = refSearchRank;
	
//	console.log(JSON.stringify(historyItem));

	return historyItem;
}
