
if (!chrome.cookies) 
{
  chrome.cookies = chrome.experimental.cookies;
}

function getCookieEventItem(info)
{
	var date = new Date();
	var timestamp = Math.round(date.getTime()/1000.0);

	var cookieEventItem = new Object();

	cookieEventItem.date = date;
	cookieEventItem.timestamp = timestamp;
	
	cookieEventItem.cause = info.cause;

	cookieEventItem.cookieDomain = info.cookie.domain;
	cookieEventItem.cookieName = info.cookie.name;
	cookieEventItem.cookieStoreId = info.cookie.storeId;
	cookieEventItem.cookieValue = info.cookie.value;
	cookieEventItem.cookieSession = info.cookie.session;
	cookieEventItem.cookieHostOnly = info.cookie.hostOnly;
	cookieEventItem.cookieExpirationDate = info.cookie.expirationDate;
	cookieEventItem.cookiePath = info.cookie.path;
	cookieEventItem.cookieHttpOnly = info.cookie.httpOnly;
	cookieEventItem.cookieSecure = info.cookie.secure;
	cookieEventItem.cookieRemoved = info.removed;

	return cookieEventItem;
}

function getCookie(info)
{
//	console.log(info);
	var cookie = new Object();
	
	cookie.domain = info.domain;
	cookie.name = info.name;
	cookie.storeId = info.storeId;
	cookie.value = info.value;
	cookie.session = info.session;
	cookie.hostOnly = info.hostOnly;
	cookie.expirationDate = info.expirationDate;
	cookie.path = info.path;
	cookie.httpOnly = info.httpOnly;
	cookie.secure = info.secure;
	
	return cookie;
}

function getAllCookies()
{
	var cookieList = [];
	
	chrome.cookies.getAll({}, function(cookies) {
		for (var i = 0; i < cookies.length; i++)
		{
			cookieList.push(getCookie(cookies[i]));
		}
	});
	
	return cookieList;
}

