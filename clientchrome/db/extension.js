function getExtensionEventItem(eventType, info)
{
	console.log(info);
	var date = new Date();
	var timestamp = Math.round(date.getTime()/1000.0);
	
	var extensionEventItem = new Object();
	
	extensionEventItem.date = date;
	extensionEventItem.timestamp = timestamp;

	extensionEventItem.eventType = eventType;
	
	extensionEventItem.name = info.name;
	extensionEventItem.id = info.id;
	
	extensionEventItem.extensionEnabled = info.enabled;
	extensionEventItem.extensionHomepageUrl = info.homepageUrl;
	extensionEventItem.extensionHostPermissions = JSON.stringify(info.hostPermissions);
	extensionEventItem.extensionInstallType = info.installType;
	extensionEventItem.extensionIsApp = info.isApp;
	extensionEventItem.extensionMayDisable = info.mayDisable;
	extensionEventItem.extensionOfflineEnabled = info.offlineEnabled;
	extensionEventItem.extensionPermissions = JSON.stringify(info.permissions);
	extensionEventItem.extensionType = info.type;
	extensionEventItem.extensionVersion = info.version;
	
	return extensionEventItem;
}

function getExtension(info)
{
//	console.log(info);
	var extension = new Object();
	
	extension.id = info.id;
	extension.name = info.name;
	extension.enabled = info.enabled;
	extension.homepageUrl = info.homepageUrl;
	extension.hostPermissions = JSON.stringify(info.hostPermissions);
	extension.installType = info.installType;
	extension.isApp = info.isApp;
	extension.mayDisable = info.mayDisable;
	extension.offlineEnabled = info.offlineEnabled;
	extension.permissions = JSON.stringify(info.permissions);
	extension.type = info.type;
	extension.version = info.version;
	
	return extension;
}

function getAllExtensions()
{
	var extensionList = [];
	
	chrome.management.getAll(function(info) {
		for (var i = 0; i < info.length; i++)
		{
			extensionList.push(getExtension(info[i]));
		}
	});
	
	return extensionList;
}