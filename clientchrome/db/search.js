function getGoogleSearchType(stype)
{
	var searchType = "Web";

	if (stype == "isch")
	{
		searchType = "Images";
	}
	else if (stype == "vid")
	{
		searchType = "Videos";
	}
	else if (stype == "nws")
	{
		searchType = "News";
	}
	else if (stype == "shop")
	{
		searchType = "Shopping";
	}
	else if (stype == "app")
	{
		searchType = "Applications";
	}
	else if (stype == "bks")
	{
		searchType = "Books";
	}
	else if (stype == "plcs")
	{
		searchType = "Places";
	}
	else if (stype == "blg")
	{
		searchType = "Blogs";
	}
	else if (stype == "flm")
	{
		searchType = "Flights";
	}
	else if (stype == "dsc")
	{
		searchType = "Discussions";
	}
	else if (stype == "rcp")
	{
		searchType = "Recipes";
	}
	else if (stype == "pts")
	{
		searchType = "Patents";
	}

	return searchType;
}

function getBingSearchType(url)
{
	var searchType = "Web";
	
	if (url.indexOf("/images/search?") != -1)
	{
		searchType = "Images";
	}
	else if (url.indexOf("/videos/search?") != -1)
	{
		searchType = "Videos";
	}
	else if (url.indexOf("/maps/") != -1)
	{
		searchType = "Maps";
	}
	else if (url.indexOf("/news/search?") != -1)
	{
		searchType = "News";
	}
	else if (url.indexOf("/shopping/search?") != -1)
	{
		searchType = "Shopping";
	}
	else if (url.indexOf("/movies?") != -1)
	{
		searchType = "Movies";
	}
	else if (url.indexOf("/music?") != -1)
	{
		searchType = "Music";
	}
	else if (url.indexOf("/weather?") != -1)
	{
		searchType = "Weather";
	}
	else if (url.indexOf("/local/?") != -1)
	{
		searchType = "Local";
	}
	
	return searchType;
}

function getYahooSearchType(url)
{
	var searchType = "";
	
	if (url.indexOf("search.yahoo") != -1)
	{
		searchType = "Web";
	}
	else if (url.indexOf("images.search") != -1)
	{
		searchType = "Images";
	}
	else if (url.indexOf("video.search") != -1)
	{
		searchType = "Videos";
	}
	else if (url.indexOf("news.search") != -1)
	{
		searchType = "News";
	}
	else if (url.indexOf("shopping.yahoo") != -1)
	{
		searchType = "Shopping";
	}
	else if (url.indexOf("blog.search") != -1)
	{
		searchType = "Blogs";
	}
	else if (url.indexOf("recipes.search") != -1)
	{
		searchType = "Recipes";
	}
	else if (url.indexOf("sports.search") != -1)
	{
		searchType = "Sports";
	}
	else if (url.indexOf("games.search") != -1)
	{
		searchType = "Games";
	}
	else if (url.indexOf("movies.search") != -1)
	{
		searchType = "Movies";
	}
	else if (url.indexOf("finance.search") != -1)
	{
		searchType = "Finance";
	}
	else if (url.indexOf("local.search") != -1)
	{
		searchType = "Local";
	}

	return searchType;
}

function getAskComSearchType(url)
{
	var searchType = "Web";
	
	if (url.indexOf("/pictures?") != -1)
	{
		searchType = "Images";
	}
	
	return searchType;
}

function getBaiduSearchType(url)
{
	var searchType = "Web";
	
	if (url.indexOf("music.baidu.") != -1)
	{
		searchType = "Music";
	}
	else if (url.indexOf("image.baidu.") != -1)
	{
		searchType = "Images";
	}
	else if (url.indexOf("video.baidu.") != -1)
	{
		searchType = "Videos";
	}
	else if (url.indexOf("map.baidu.") != -1)
	{
		searchType = "Maps";
	}
	
	return searchType;
}

function getSearchArguments(url, searchEngine, searchParams)
{
	var searchArgs = new Object();
	var searchTerm = "";
	var searchType = "";
	var searchRank = "";
	
	var tokens = searchParams.split("&");
	
	if (searchEngine == "Google")
	{
		for (var i = 0; i < tokens.length; i++)
		{
			if (tokens[i].indexOf("q=") == 0)
			{
				searchTerm = tokens[i].substring(2);
				if (searchType != "" && searchRank != "")
				{
					break;
				}
			}
			else if (tokens[i].indexOf("tbm=") == 0)
			{
				var stype = tokens[i].substring(4);
				searchType = getGoogleSearchType(stype);
				if (searchTerm != "" && searchRank != "")
				{
					console.log(searchTerm);
					break;
				}
			}
			else if (tokens[i].indexOf("cd=") == 0)
			{
				searchRank = tokens[i].substring(3);
				if (searchTerm != "" && searchType != "")
				{
					console.log(searchRank);
					break;
				}
			}
		}
		// XXX: google maps special case
		if (url.indexOf("maps.google.") != -1)
		{
			searchType = "Maps";
		}
		else if (searchType == "")
		{
			searchType = "Web";
		}
	}
	else if (searchEngine == "Bing")
	{
		for (var i = 0; i < tokens.length; i++)
		{
			if (tokens[i].indexOf("q=") == 0)
			{
				searchTerm = tokens[i].substring(2);
				break;
			}
		}
		searchType = getBingSearchType(url);
		
	}
	else if (searchEngine == "Yahoo")
	{
		for (var i = 0; i < tokens.length; i++)
		{
			if (tokens[i].indexOf("p=") == 0)
			{
				searchTerm = tokens[i].substring(2);
				break;				
			}
		}
		searchType = getYahooSearchType(url);
		
		if (searchType == "")
		{
			searchTerm = "";
		}
	}
	else if (searchEngine == "Shopping.com")
	{
		for (var i = 0; i < tokens.length; i++)
		{
			if (tokens[i].indexOf("kw=") == 0)
			{
				searchTerm = tokens[i].substring(3);
				break;				
			}
		}
		searchType = "Products";
	}
	else if (searchEngine == "Ebay")
	{
		for (var i = 0; i < tokens.length; i++)
		{
			if (tokens[i].indexOf("_nkw=") == 0)
			{
				searchTerm = tokens[i].substring(5);
				break;				
			}
		}
		searchType = "Products";
	}
	else if (searchEngine == "Amazon")
	{
		for (var i = 0; i < tokens.length; i++)
		{
			if (tokens[i].indexOf("field-keywords=") == 0)
			{
				searchTerm = tokens[i].substring(15);
				break;				
			}
		}
		searchType = "Products";		
	}
	else if (searchEngine == "Ask.com")
	{
		for (var i = 0; i < tokens.length; i++)
		{
			if (tokens[i].indexOf("q=") == 0)
			{
				searchTerm = tokens[i].substring(2);
				break;				
			}
		}
		searchType = getAskComSearchType(url);
	}
	else if (searchEngine == "Baidu")
	{
		for (var i = 0; i < tokens.length; i++)
		{
			if (tokens[i].indexOf("wd=") == 0)
			{
				searchTerm = tokens[i].substring(3);
				break;				
			}
			else if (tokens[i].indexOf("key=") == 0)
			{
				searchTerm = tokens[i].substring(4);
				break;								
			}
			else if (tokens[i].indexOf("word=") == 0)
			{
				searchTerm = tokens[i].substring(5);
				break;								
			}
		}
		searchType = getBaiduSearchType(url);
		
	}

	searchArgs.searchTerm = searchTerm;
	searchArgs.searchType = searchType;
	searchArgs.searchRank = searchRank;

	return searchArgs;
}

function getSearchEngine(domain)
{
	var searchEngine = "";

	if (domain.indexOf(".google.") != -1)
	{
		searchEngine = "Google";
	}
	else if (domain.indexOf(".bing.") != -1)
	{
		searchEngine = "Bing";
	}
	else if (domain.indexOf(".yahoo.") != -1)
	{
		searchEngine = "Yahoo";
	}
	else if (domain.indexOf(".shopping.") != -1)
	{
		searchEngine = "Shopping.com";
	}
	else if (domain.indexOf(".ebay.") != -1)
	{
		searchEngine = "Ebay";
	}
	else if (domain.indexOf(".amazon.") != -1)
	{
		searchEngine = "Amazon";
	}
	else if (domain.indexOf(".ask.") != -1)
	{
		searchEngine = "Ask.com";
	}
	else if (domain.indexOf(".baidu.") != -1)
	{
		searchEngine = "Baidu";
	}

	return searchEngine;
}

function getSearchItem(url, domain, date, timestamp)
{
	// XXX: search sites and terms
	var searchEngine = "";
	var searchTerm = "";
	var searchType = "";
	var searchRank = "";
	
	searchEngine = getSearchEngine(domain);
	if (searchEngine != "")
	{
		var searchParams = url.substring(url.indexOf(domain) + domain.length);
		if (searchParams.indexOf("?") != -1)
		{
			searchParams = searchParams.substring(searchParams.indexOf("?") + 1);
		}
		else if (searchParams.indexOf("#") != -1)
		{
			searchParams = searchParams.substring(searchParams.indexOf("#") + 1);
		}
//		console.log("search params: " + searchParams);
		
		var searchArgs = getSearchArguments(url, searchEngine, searchParams);
		searchTerm = searchArgs.searchTerm;
		searchType = searchArgs.searchType;
		searchRank = searchArgs.searchRank;
		
		if (searchTerm == "")
		{
			searchType = "";
			searchEngine = "";
		}

//		console.log("search term: " + searchTerm + ", search type: " + searchType);
	}
	
	if (searchTerm != "")
	{
		var searchItem = new Object();
		searchItem.date = date;
		searchItem.timestamp = timestamp;
		searchItem.searchEngine = searchEngine;
		searchItem.searchTerm = searchTerm;
		searchItem.searchType = searchType;

//		console.log(JSON.stringify(searchItem));
		
		return searchItem;
	}

	return null;
}
