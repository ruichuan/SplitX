//
// Autogenerated by Thrift Compiler (0.8.0)
//
// DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
//

BucketType = {
'NUMERICAL_MATCHING' : 0,
'STRING_MATCHING' : 1
};
Buckets = function(args) {
  this.type = null;
  this.ranges = null;
  if (args) {
    if (args.type !== undefined) {
      this.type = args.type;
    }
    if (args.ranges !== undefined) {
      this.ranges = args.ranges;
    }
  }
};
Buckets.prototype = {};
Buckets.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.I32) {
        this.type = input.readI32().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 2:
      if (ftype == Thrift.Type.LIST) {
        var _size0 = 0;
        var _rtmp34;
        this.ranges = [];
        var _etype3 = 0;
        _rtmp34 = input.readListBegin();
        _etype3 = _rtmp34.etype;
        _size0 = _rtmp34.size;
        for (var _i5 = 0; _i5 < _size0; ++_i5)
        {
          var elem6 = null;
          elem6 = input.readString().value;
          this.ranges.push(elem6);
        }
        input.readListEnd();
      } else {
        input.skip(ftype);
      }
      break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

Buckets.prototype.write = function(output) {
  output.writeStructBegin('Buckets');
  if (this.type) {
    output.writeFieldBegin('type', Thrift.Type.I32, 1);
    output.writeI32(this.type);
    output.writeFieldEnd();
  }
  if (this.ranges) {
    output.writeFieldBegin('ranges', Thrift.Type.LIST, 2);
    output.writeListBegin(Thrift.Type.STRING, this.ranges.length);
    for (var iter7 in this.ranges)
    {
      if (this.ranges.hasOwnProperty(iter7))
      {
        iter7 = this.ranges[iter7];
        output.writeString(iter7);
      }
    }
    output.writeListEnd();
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

Query = function(args) {
  this.queryId = null;
  this.queryStartTime = null;
  this.queryEndTime = null;
  this.repeatInterval = null;
  this.epsilon = null;
  this.sql = null;
  this.buckets = null;
  this.percentileBound = null;
  this.absoluteBound = null;
  if (args) {
    if (args.queryId !== undefined) {
      this.queryId = args.queryId;
    }
    if (args.queryStartTime !== undefined) {
      this.queryStartTime = args.queryStartTime;
    }
    if (args.queryEndTime !== undefined) {
      this.queryEndTime = args.queryEndTime;
    }
    if (args.repeatInterval !== undefined) {
      this.repeatInterval = args.repeatInterval;
    }
    if (args.epsilon !== undefined) {
      this.epsilon = args.epsilon;
    }
    if (args.sql !== undefined) {
      this.sql = args.sql;
    }
    if (args.buckets !== undefined) {
      this.buckets = args.buckets;
    }
    if (args.percentileBound !== undefined) {
      this.percentileBound = args.percentileBound;
    }
    if (args.absoluteBound !== undefined) {
      this.absoluteBound = args.absoluteBound;
    }
  }
};
Query.prototype = {};
Query.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.I64) {
        this.queryId = input.readI64().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 2:
      if (ftype == Thrift.Type.I64) {
        this.queryStartTime = input.readI64().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 3:
      if (ftype == Thrift.Type.I64) {
        this.queryEndTime = input.readI64().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 4:
      if (ftype == Thrift.Type.I64) {
        this.repeatInterval = input.readI64().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 5:
      if (ftype == Thrift.Type.DOUBLE) {
        this.epsilon = input.readDouble().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 6:
      if (ftype == Thrift.Type.STRING) {
        this.sql = input.readString().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 7:
      if (ftype == Thrift.Type.STRUCT) {
        this.buckets = new Buckets();
        this.buckets.read(input);
      } else {
        input.skip(ftype);
      }
      break;
      case 8:
      if (ftype == Thrift.Type.DOUBLE) {
        this.percentileBound = input.readDouble().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 9:
      if (ftype == Thrift.Type.I32) {
        this.absoluteBound = input.readI32().value;
      } else {
        input.skip(ftype);
      }
      break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

Query.prototype.write = function(output) {
  output.writeStructBegin('Query');
  if (this.queryId) {
    output.writeFieldBegin('queryId', Thrift.Type.I64, 1);
    output.writeI64(this.queryId);
    output.writeFieldEnd();
  }
  if (this.queryStartTime) {
    output.writeFieldBegin('queryStartTime', Thrift.Type.I64, 2);
    output.writeI64(this.queryStartTime);
    output.writeFieldEnd();
  }
  if (this.queryEndTime) {
    output.writeFieldBegin('queryEndTime', Thrift.Type.I64, 3);
    output.writeI64(this.queryEndTime);
    output.writeFieldEnd();
  }
  if (this.repeatInterval) {
    output.writeFieldBegin('repeatInterval', Thrift.Type.I64, 4);
    output.writeI64(this.repeatInterval);
    output.writeFieldEnd();
  }
  if (this.epsilon) {
    output.writeFieldBegin('epsilon', Thrift.Type.DOUBLE, 5);
    output.writeDouble(this.epsilon);
    output.writeFieldEnd();
  }
  if (this.sql) {
    output.writeFieldBegin('sql', Thrift.Type.STRING, 6);
    output.writeString(this.sql);
    output.writeFieldEnd();
  }
  if (this.buckets) {
    output.writeFieldBegin('buckets', Thrift.Type.STRUCT, 7);
    this.buckets.write(output);
    output.writeFieldEnd();
  }
  if (this.percentileBound) {
    output.writeFieldBegin('percentileBound', Thrift.Type.DOUBLE, 8);
    output.writeDouble(this.percentileBound);
    output.writeFieldEnd();
  }
  if (this.absoluteBound) {
    output.writeFieldBegin('absoluteBound', Thrift.Type.I32, 9);
    output.writeI32(this.absoluteBound);
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

QueryFile = function(args) {
  this.analystId = null;
  this.versionId = null;
  this.time = null;
  this.queries = null;
  if (args) {
    if (args.analystId !== undefined) {
      this.analystId = args.analystId;
    }
    if (args.versionId !== undefined) {
      this.versionId = args.versionId;
    }
    if (args.time !== undefined) {
      this.time = args.time;
    }
    if (args.queries !== undefined) {
      this.queries = args.queries;
    }
  }
};
QueryFile.prototype = {};
QueryFile.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.STRING) {
        this.analystId = input.readString().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 2:
      if (ftype == Thrift.Type.I64) {
        this.versionId = input.readI64().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 3:
      if (ftype == Thrift.Type.I64) {
        this.time = input.readI64().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 4:
      if (ftype == Thrift.Type.LIST) {
        var _size8 = 0;
        var _rtmp312;
        this.queries = [];
        var _etype11 = 0;
        _rtmp312 = input.readListBegin();
        _etype11 = _rtmp312.etype;
        _size8 = _rtmp312.size;
        for (var _i13 = 0; _i13 < _size8; ++_i13)
        {
          var elem14 = null;
          elem14 = new Query();
          elem14.read(input);
          this.queries.push(elem14);
        }
        input.readListEnd();
      } else {
        input.skip(ftype);
      }
      break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

QueryFile.prototype.write = function(output) {
  output.writeStructBegin('QueryFile');
  if (this.analystId) {
    output.writeFieldBegin('analystId', Thrift.Type.STRING, 1);
    output.writeString(this.analystId);
    output.writeFieldEnd();
  }
  if (this.versionId) {
    output.writeFieldBegin('versionId', Thrift.Type.I64, 2);
    output.writeI64(this.versionId);
    output.writeFieldEnd();
  }
  if (this.time) {
    output.writeFieldBegin('time', Thrift.Type.I64, 3);
    output.writeI64(this.time);
    output.writeFieldEnd();
  }
  if (this.queries) {
    output.writeFieldBegin('queries', Thrift.Type.LIST, 4);
    output.writeListBegin(Thrift.Type.STRUCT, this.queries.length);
    for (var iter15 in this.queries)
    {
      if (this.queries.hasOwnProperty(iter15))
      {
        iter15 = this.queries[iter15];
        iter15.write(output);
      }
    }
    output.writeListEnd();
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

