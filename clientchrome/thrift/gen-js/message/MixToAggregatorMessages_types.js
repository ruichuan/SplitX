//
// Autogenerated by Thrift Compiler (0.8.0)
//
// DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
//

QueryNoisyAnswers = function(args) {
  this.analystId = null;
  this.queryId = null;
  this.answers = null;
  this.noise = null;
  if (args) {
    if (args.analystId !== undefined) {
      this.analystId = args.analystId;
    }
    if (args.queryId !== undefined) {
      this.queryId = args.queryId;
    }
    if (args.answers !== undefined) {
      this.answers = args.answers;
    }
    if (args.noise !== undefined) {
      this.noise = args.noise;
    }
  }
};
QueryNoisyAnswers.prototype = {};
QueryNoisyAnswers.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.STRING) {
        this.analystId = input.readString().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 2:
      if (ftype == Thrift.Type.I64) {
        this.queryId = input.readI64().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 3:
      if (ftype == Thrift.Type.LIST) {
        var _size0 = 0;
        var _rtmp34;
        this.answers = [];
        var _etype3 = 0;
        _rtmp34 = input.readListBegin();
        _etype3 = _rtmp34.etype;
        _size0 = _rtmp34.size;
        for (var _i5 = 0; _i5 < _size0; ++_i5)
        {
          var elem6 = null;
          elem6 = input.readString().value;
          this.answers.push(elem6);
        }
        input.readListEnd();
      } else {
        input.skip(ftype);
      }
      break;
      case 4:
      if (ftype == Thrift.Type.I32) {
        this.noise = input.readI32().value;
      } else {
        input.skip(ftype);
      }
      break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

QueryNoisyAnswers.prototype.write = function(output) {
  output.writeStructBegin('QueryNoisyAnswers');
  if (this.analystId) {
    output.writeFieldBegin('analystId', Thrift.Type.STRING, 1);
    output.writeString(this.analystId);
    output.writeFieldEnd();
  }
  if (this.queryId) {
    output.writeFieldBegin('queryId', Thrift.Type.I64, 2);
    output.writeI64(this.queryId);
    output.writeFieldEnd();
  }
  if (this.answers) {
    output.writeFieldBegin('answers', Thrift.Type.LIST, 3);
    output.writeListBegin(Thrift.Type.STRING, this.answers.length);
    for (var iter7 in this.answers)
    {
      if (this.answers.hasOwnProperty(iter7))
      {
        iter7 = this.answers[iter7];
        output.writeString(iter7);
      }
    }
    output.writeListEnd();
    output.writeFieldEnd();
  }
  if (this.noise) {
    output.writeFieldBegin('noise', Thrift.Type.I32, 4);
    output.writeI32(this.noise);
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

