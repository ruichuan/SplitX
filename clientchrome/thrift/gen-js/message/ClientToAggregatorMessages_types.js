//
// Autogenerated by Thrift Compiler (0.8.0)
//
// DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
//

QueryRequest = function(args) {
  this.analystId = null;
  this.latestVersionId = null;
  if (args) {
    if (args.analystId !== undefined) {
      this.analystId = args.analystId;
    }
    if (args.latestVersionId !== undefined) {
      this.latestVersionId = args.latestVersionId;
    }
  }
};
QueryRequest.prototype = {};
QueryRequest.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.STRING) {
        this.analystId = input.readString().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 2:
      if (ftype == Thrift.Type.I64) {
        this.latestVersionId = input.readI64().value;
      } else {
        input.skip(ftype);
      }
      break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

QueryRequest.prototype.write = function(output) {
  output.writeStructBegin('QueryRequest');
  if (this.analystId) {
    output.writeFieldBegin('analystId', Thrift.Type.STRING, 1);
    output.writeString(this.analystId);
    output.writeFieldEnd();
  }
  if (this.latestVersionId) {
    output.writeFieldBegin('latestVersionId', Thrift.Type.I64, 2);
    output.writeI64(this.latestVersionId);
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

