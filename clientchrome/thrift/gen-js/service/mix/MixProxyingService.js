//
// Autogenerated by Thrift Compiler (0.8.0)
//
// DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
//


//HELPER FUNCTIONS AND STRUCTURES

MixProxyingService_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses_args = function(args) {
  this.clientId = null;
  this.queryRequests = null;
  if (args) {
    if (args.clientId !== undefined) {
      this.clientId = args.clientId;
    }
    if (args.queryRequests !== undefined) {
      this.queryRequests = args.queryRequests;
    }
  }
};
MixProxyingService_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses_args.prototype = {};
MixProxyingService_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses_args.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.STRING) {
        this.clientId = input.readString().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 2:
      if (ftype == Thrift.Type.LIST) {
        var _size0 = 0;
        var _rtmp34;
        this.queryRequests = [];
        var _etype3 = 0;
        _rtmp34 = input.readListBegin();
        _etype3 = _rtmp34.etype;
        _size0 = _rtmp34.size;
        for (var _i5 = 0; _i5 < _size0; ++_i5)
        {
          var elem6 = null;
          elem6 = new SplitMessage();
          elem6.read(input);
          this.queryRequests.push(elem6);
        }
        input.readListEnd();
      } else {
        input.skip(ftype);
      }
      break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

MixProxyingService_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses_args.prototype.write = function(output) {
  output.writeStructBegin('MixProxyingService_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses_args');
  if (this.clientId) {
    output.writeFieldBegin('clientId', Thrift.Type.STRING, 1);
    output.writeString(this.clientId);
    output.writeFieldEnd();
  }
  if (this.queryRequests) {
    output.writeFieldBegin('queryRequests', Thrift.Type.LIST, 2);
    output.writeListBegin(Thrift.Type.STRUCT, this.queryRequests.length);
    for (var iter7 in this.queryRequests)
    {
      if (this.queryRequests.hasOwnProperty(iter7))
      {
        iter7 = this.queryRequests[iter7];
        iter7.write(output);
      }
    }
    output.writeListEnd();
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

MixProxyingService_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses_result = function(args) {
  this.success = null;
  if (args) {
    if (args.success !== undefined) {
      this.success = args.success;
    }
  }
};
MixProxyingService_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses_result.prototype = {};
MixProxyingService_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses_result.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 0:
      if (ftype == Thrift.Type.LIST) {
        var _size8 = 0;
        var _rtmp312;
        this.success = [];
        var _etype11 = 0;
        _rtmp312 = input.readListBegin();
        _etype11 = _rtmp312.etype;
        _size8 = _rtmp312.size;
        for (var _i13 = 0; _i13 < _size8; ++_i13)
        {
          var elem14 = null;
          elem14 = new SplitMessage();
          elem14.read(input);
          this.success.push(elem14);
        }
        input.readListEnd();
      } else {
        input.skip(ftype);
      }
      break;
      case 0:
        input.skip(ftype);
        break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

MixProxyingService_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses_result.prototype.write = function(output) {
  output.writeStructBegin('MixProxyingService_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses_result');
  if (this.success) {
    output.writeFieldBegin('success', Thrift.Type.LIST, 0);
    output.writeListBegin(Thrift.Type.STRUCT, this.success.length);
    for (var iter15 in this.success)
    {
      if (this.success.hasOwnProperty(iter15))
      {
        iter15 = this.success[iter15];
        iter15.write(output);
      }
    }
    output.writeListEnd();
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

MixProxyingService_forward_ClientToAggregator_QueryAnswers_args = function(args) {
  this.queryAnswers = null;
  if (args) {
    if (args.queryAnswers !== undefined) {
      this.queryAnswers = args.queryAnswers;
    }
  }
};
MixProxyingService_forward_ClientToAggregator_QueryAnswers_args.prototype = {};
MixProxyingService_forward_ClientToAggregator_QueryAnswers_args.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.LIST) {
        var _size16 = 0;
        var _rtmp320;
        this.queryAnswers = [];
        var _etype19 = 0;
        _rtmp320 = input.readListBegin();
        _etype19 = _rtmp320.etype;
        _size16 = _rtmp320.size;
        for (var _i21 = 0; _i21 < _size16; ++_i21)
        {
          var elem22 = null;
          elem22 = new SplitMessage();
          elem22.read(input);
          this.queryAnswers.push(elem22);
        }
        input.readListEnd();
      } else {
        input.skip(ftype);
      }
      break;
      case 0:
        input.skip(ftype);
        break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

MixProxyingService_forward_ClientToAggregator_QueryAnswers_args.prototype.write = function(output) {
  output.writeStructBegin('MixProxyingService_forward_ClientToAggregator_QueryAnswers_args');
  if (this.queryAnswers) {
    output.writeFieldBegin('queryAnswers', Thrift.Type.LIST, 1);
    output.writeListBegin(Thrift.Type.STRUCT, this.queryAnswers.length);
    for (var iter23 in this.queryAnswers)
    {
      if (this.queryAnswers.hasOwnProperty(iter23))
      {
        iter23 = this.queryAnswers[iter23];
        iter23.write(output);
      }
    }
    output.writeListEnd();
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

MixProxyingService_forward_ClientToAggregator_QueryAnswers_result = function(args) {
};
MixProxyingService_forward_ClientToAggregator_QueryAnswers_result.prototype = {};
MixProxyingService_forward_ClientToAggregator_QueryAnswers_result.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    input.skip(ftype);
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

MixProxyingService_forward_ClientToAggregator_QueryAnswers_result.prototype.write = function(output) {
  output.writeStructBegin('MixProxyingService_forward_ClientToAggregator_QueryAnswers_result');
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

MixProxyingService_forward_ClientToMix_QueryAnswers_args = function(args) {
  this.queryAnswers = null;
  if (args) {
    if (args.queryAnswers !== undefined) {
      this.queryAnswers = args.queryAnswers;
    }
  }
};
MixProxyingService_forward_ClientToMix_QueryAnswers_args.prototype = {};
MixProxyingService_forward_ClientToMix_QueryAnswers_args.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.LIST) {
        var _size24 = 0;
        var _rtmp328;
        this.queryAnswers = [];
        var _etype27 = 0;
        _rtmp328 = input.readListBegin();
        _etype27 = _rtmp328.etype;
        _size24 = _rtmp328.size;
        for (var _i29 = 0; _i29 < _size24; ++_i29)
        {
          var elem30 = null;
          elem30 = new SplitMessage();
          elem30.read(input);
          this.queryAnswers.push(elem30);
        }
        input.readListEnd();
      } else {
        input.skip(ftype);
      }
      break;
      case 0:
        input.skip(ftype);
        break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

MixProxyingService_forward_ClientToMix_QueryAnswers_args.prototype.write = function(output) {
  output.writeStructBegin('MixProxyingService_forward_ClientToMix_QueryAnswers_args');
  if (this.queryAnswers) {
    output.writeFieldBegin('queryAnswers', Thrift.Type.LIST, 1);
    output.writeListBegin(Thrift.Type.STRUCT, this.queryAnswers.length);
    for (var iter31 in this.queryAnswers)
    {
      if (this.queryAnswers.hasOwnProperty(iter31))
      {
        iter31 = this.queryAnswers[iter31];
        iter31.write(output);
      }
    }
    output.writeListEnd();
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

MixProxyingService_forward_ClientToMix_QueryAnswers_result = function(args) {
};
MixProxyingService_forward_ClientToMix_QueryAnswers_result.prototype = {};
MixProxyingService_forward_ClientToMix_QueryAnswers_result.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    input.skip(ftype);
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

MixProxyingService_forward_ClientToMix_QueryAnswers_result.prototype.write = function(output) {
  output.writeStructBegin('MixProxyingService_forward_ClientToMix_QueryAnswers_result');
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

MixProxyingServiceClient = function(input, output) {
    this.input = input;
    this.output = (!output) ? input : output;
    this.seqid = 0;
};
MixProxyingServiceClient.prototype = {};
MixProxyingServiceClient.prototype.forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses = function(clientId, queryRequests) {
  this.send_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses(clientId, queryRequests);
  return this.recv_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses();
};

MixProxyingServiceClient.prototype.send_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses = function(clientId, queryRequests) {
  this.output.writeMessageBegin('forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses', Thrift.MessageType.CALL, this.seqid);
  var args = new MixProxyingService_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses_args();
  args.clientId = clientId;
  args.queryRequests = queryRequests;
  args.write(this.output);
  this.output.writeMessageEnd();
  return this.output.getTransport().flush();
};

MixProxyingServiceClient.prototype.recv_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses = function() {
  var ret = this.input.readMessageBegin();
  var fname = ret.fname;
  var mtype = ret.mtype;
  var rseqid = ret.rseqid;
  if (mtype == Thrift.MessageType.EXCEPTION) {
    var x = new Thrift.TApplicationException();
    x.read(this.input);
    this.input.readMessageEnd();
    throw x;
  }
  var result = new MixProxyingService_forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses_result();
  result.read(this.input);
  this.input.readMessageEnd();

  if (null !== result.success) {
    return result.success;
  }
  throw 'forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses failed: unknown result';
};
MixProxyingServiceClient.prototype.forward_ClientToAggregator_QueryAnswers = function(queryAnswers) {
  this.send_forward_ClientToAggregator_QueryAnswers(queryAnswers);
  this.recv_forward_ClientToAggregator_QueryAnswers();
};

MixProxyingServiceClient.prototype.send_forward_ClientToAggregator_QueryAnswers = function(queryAnswers) {
  this.output.writeMessageBegin('forward_ClientToAggregator_QueryAnswers', Thrift.MessageType.CALL, this.seqid);
  var args = new MixProxyingService_forward_ClientToAggregator_QueryAnswers_args();
  args.queryAnswers = queryAnswers;
  args.write(this.output);
  this.output.writeMessageEnd();
  return this.output.getTransport().flush();
};

MixProxyingServiceClient.prototype.recv_forward_ClientToAggregator_QueryAnswers = function() {
  var ret = this.input.readMessageBegin();
  var fname = ret.fname;
  var mtype = ret.mtype;
  var rseqid = ret.rseqid;
  if (mtype == Thrift.MessageType.EXCEPTION) {
    var x = new Thrift.TApplicationException();
    x.read(this.input);
    this.input.readMessageEnd();
    throw x;
  }
  var result = new MixProxyingService_forward_ClientToAggregator_QueryAnswers_result();
  result.read(this.input);
  this.input.readMessageEnd();

  return;
};
MixProxyingServiceClient.prototype.forward_ClientToMix_QueryAnswers = function(queryAnswers) {
  this.send_forward_ClientToMix_QueryAnswers(queryAnswers);
  this.recv_forward_ClientToMix_QueryAnswers();
};

MixProxyingServiceClient.prototype.send_forward_ClientToMix_QueryAnswers = function(queryAnswers) {
  this.output.writeMessageBegin('forward_ClientToMix_QueryAnswers', Thrift.MessageType.CALL, this.seqid);
  var args = new MixProxyingService_forward_ClientToMix_QueryAnswers_args();
  args.queryAnswers = queryAnswers;
  args.write(this.output);
  this.output.writeMessageEnd();
  return this.output.getTransport().flush();
};

MixProxyingServiceClient.prototype.recv_forward_ClientToMix_QueryAnswers = function() {
  var ret = this.input.readMessageBegin();
  var fname = ret.fname;
  var mtype = ret.mtype;
  var rseqid = ret.rseqid;
  if (mtype == Thrift.MessageType.EXCEPTION) {
    var x = new Thrift.TApplicationException();
    x.read(this.input);
    this.input.readMessageEnd();
    throw x;
  }
  var result = new MixProxyingService_forward_ClientToMix_QueryAnswers_result();
  result.read(this.input);
  this.input.readMessageEnd();

  return;
};
