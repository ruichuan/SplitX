var DEBUG = true;
var consoleWrapper = console;
var console = {};
console.log = function(str)
{
	if (DEBUG)
	{
		consoleWrapper.log(str);
	}
	else
	{
		//consoleWrapper.log("would have logged something");
	}
};

function eval(txt)
{
	return JSON.parse(txt);
}

function fixChars(url)
{
	url = url.replace(/%20/g, " ");
	url = url.replace(/\+/g, " ");
	url = url.replace(/%26/g, "&");
	url = url.replace(/%3d/g, "=");

	return url;
}

