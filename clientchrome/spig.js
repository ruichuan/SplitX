
function sendQueryAnswers(lim, aid, versionId)
{
	var len = pendingQueryAnswers.length;
	// TODO: take the queryEndTime into account
	if (len < lim)
	{
		console.log("too few query answers; will send later");
		return;
	}

	console.log("sending query answers...");

	// TODO: generate split messages
	// TODO: send query answers to mix and aggregator
		
	for (var i = 0; i < len; i++)
	{
		var curAnswer = pendingQueryAnswers.shift();

		var qa1 = new QueryAnswer();
		qa1.analystId = curAnswer.analystId;
		qa1.queryId = curAnswer.queryId;

		var qa2 = new QueryAnswer();
		qa2.analystId = curAnswer.analystId;
		qa2.queryId = curAnswer.queryId;

		// XXX: get a split id
		var sid = generateSplitId();
		var sidstr = sid.toString();
//		console.log("first splitId: " + sidstr);

		qa1.splitId = sidstr;
		qa2.splitId = sidstr;

		// XXX: first split: the answerBits
		var pieces = split(curAnswer.answerBits, true);
//		console.log(pieces);
		qa1.answerBits = pieces[0];
		qa2.answerBits = pieces[1];

		// XXX: serialize these queryAnswers
		// TODO: serialize sm1 and sm2
		var sm1_msg = pidCryptUtil.encodeUTF8(serialize(qa1));
		var sm2_msg = pidCryptUtil.encodeUTF8(serialize(qa2));
//		console.log(sm1_msg);
//		console.log(sm2_msg);

		// TODO: split sm1 (2nd split)
		var pieces_sm1 = split(sm1_msg, true);
		var sm1_sidstr = generateSplitId().toString();
//		console.log("second splitId (sm1): " + sm1_sidstr);

		// TODO: send sm1_1 to mix
		var list_2nd_split = [];
		var sm1_1 = new SplitMessage({splitId:sm1_sidstr, message:pieces_sm1[0]});
		list_2nd_split.push(sm1_1);
//		console.log(sm1_1);

		sendQueryAnswersToMix(mix1Url, list_2nd_split);

		// TODO: serialize sm1_2
		var sm1_2 = new SplitMessage({splitId:sm1_sidstr, message:pieces_sm1[1], dstMixId:"1"});
		var sm1_2_msg = pidCryptUtil.encodeUTF8(serialize(sm1_2));
//		console.log(sm1_2);
//		console.log(sm1_2_msg);
		// TODO: split sm1_2 (3rd split)
		var pieces_sm1_2 = split(sm1_2_msg, true);
		var sm1_2_sidstr = generateSplitId().toString();
//		console.log("third splitId (sm1): " + sm1_2_sidstr);
		
		var list_3rd_split = [];
		var list_3rd_split_2 = [];
		// TODO: send sm1_2_1 (to aggregator via mix 1)
		var sm1_2_1 = new SplitMessage({splitId:sm1_2_sidstr, message:pieces_sm1_2[0]});
		list_3rd_split.push(sm1_2_1);
//		console.log(sm1_2_1);
		
		// TODO: send sm1_2_2 (to aggregator via mix 2)
		var sm1_2_2 = new SplitMessage({splitId:sm1_2_sidstr, message:pieces_sm1_2[1]});
		list_3rd_split_2.push(sm1_2_2);
//		console.log(sm1_2_2);
		
		// TODO: send to aggregator over mix
		sendQueryAnswersToAggregator(mix1Url, list_3rd_split);
		sendQueryAnswersToAggregator(mix2Url, list_3rd_split_2);


		// XXX: same for sm2
		// TODO: split sm2 (2nd split)
		var pieces_sm2 = split(sm2_msg, true);
		var sm2_sidstr = generateSplitId().toString();
//		console.log("second splitId (sm2): " + sm2_sidstr);

		// TODO: send sm2_1 to mix
		var list2_2nd_split = [];
		var sm2_1 = new SplitMessage({splitId:sm2_sidstr, message:pieces_sm2[0]});
		list2_2nd_split.push(sm2_1);
//		console.log(sm2_1);

		sendQueryAnswersToMix(mix2Url, list2_2nd_split);

		// TODO: serialize sm2_2
		var sm2_2 = new SplitMessage({splitId:sm2_sidstr, message:pieces_sm2[1], dstMixId:"0"});
		var sm2_2_msg = pidCryptUtil.encodeUTF8(serialize(sm2_2));
//		console.log(sm2_2);
//		console.log(sm2_2_msg);
		// TODO: split sm2_2 (3rd split)
		var pieces_sm2_2 = split(sm2_2_msg, true);
		var sm2_2_sidstr = generateSplitId().toString();
//		console.log("third splitId (sm2): " + sm2_2_sidstr);
		
		var list2_3rd_split = [];
		var list2_3rd_split_2 = [];
		// TODO: send sm2_2_1 (to aggregator via mix 1)
		var sm2_2_1 = new SplitMessage({splitId:sm2_2_sidstr, message:pieces_sm2_2[0]});
		list2_3rd_split.push(sm2_2_1);
//		console.log(sm2_2_1);
		
		// TODO: send sm2_2_2 (to aggregator via mix 2)
		var sm2_2_2 = new SplitMessage({splitId:sm2_2_sidstr, message:pieces_sm2_2[1]});
		list2_3rd_split_2.push(sm2_2_2);
//		console.log(sm2_2_2);
		
		// TODO: send to aggregator over mix
		sendQueryAnswersToAggregator(mix1Url, list2_3rd_split);
		sendQueryAnswersToAggregator(mix2Url, list2_3rd_split_2);
	}

	console.log("query answers sent.");
	localStorage[aid + '_latestVersionId'] = versionId;
	console.log("latest version set: " + versionId);

}

function processResults(tx, res)
{
	console.log("got result for query: " + tx.query.queryId);

	// XXX: generate answers

	var query = tx.query;
	var results = [];
	var len = res.rows.length;
	for (var i = 0; i < len; i++)
	{
		var it = res.rows.item(i);
		for (var k in it)
		{
			results.push(it[k]);
		}
	}
	
	// TODO: match buckets
	var buckets = query.buckets;
	var bucket_type = buckets.type;
	var bucket_ranges = buckets.ranges;
	console.log("bucket_ranges length: " + bucket_ranges.length);
	
	var answerBits = new BitArray(bucket_ranges.length + 8);
	var answerBytes = [];
	for (var i = 0; i < 8; i++)
	{
		// XXX: reserved bits
		answerBits.set(i, false);
	}
	if (len > 0)
	{
		// XXX: we found some results
		console.log("found some results.");
		answerBits.set(0, true);
	}

	var len = bucket_ranges.length;
	for (var i = 0; i < len; i++)
	{
		var rstr = bucket_ranges[i];
		for (var j = 0; j < results.length; j++)
		{
			var curres = results[j];
			if (bucket_type == 0)
			{
				var tokens = rstr.split(",");
				var min = parseFloat(tokens[0]);
				var max = parseFloat(tokens[1]);
				if (min <= curres && curres <= max)
				{
					answerBits.set(i+8, true);
				}
			}
			else if (bucket_type == 1)
			{
				var tokens = curres.split(" ");
				for (var k = 0; k < tokens.length; k++)
				{
					if (rstr == tokens[k])
					{
//						console.log("result: " + curres + " found token: " + tokens[k]);
						answerBits.set(i+8, true);
						break;
					}
				}
			}
		}
	}
	
	var answerBitsStr = answerBits.toString();
//	console.log("answer: " + answerBitsStr);
	console.log("answer length (bits): " + answerBitsStr.length);
	var answerBitsBytes = answerBits.toByteArray();

	var answer = new Object();
	answer.analystId = tx.analystId;
	answer.queryId = query.queryId;
	// XXX: splitId will be set when we generate the actual message
	var str = pidCryptUtil.byteArray2String(answerBitsBytes);
	answer.answerBits = str;
	pendingQueryAnswers.push(answer);

	sendQueryAnswers(1, tx.analystId, tx.versionId);
}

function processQuery(query, aid, versionId)
{
	console.log("processing query id: " + query.queryId + ", number of buckets: " + query.buckets.ranges.length);

	// XXX: check queryEndTime
	var qn = checkQueryEndTime(query.queryEndTime);
	if (qn)
	{
		console.log("queryEndTime passed");
		return;
	}

	// XXX: check queryId; whether we answered before or not
	// XXX: DISABLED
	var ab = answeredBefore(aid, query.queryId);
//	var ab = false;
	if (ab)
	{
		console.log("answered before: " + aid + " " + query.queryId);
		return;
	}
	
	// XXX: update analyst's deficit with epsilon
	updateDeficit(aid, query.epsilon);
	
	// TODO: execute sql
	executeSelect(db, aid, versionId, query, processResults);
	
}

function processPendingQueryFiles()
{
	if (pendingQueryFiles.length == 0)
	{
		console.log("no queries.");
		return;
	}

	console.log("processing pending query files...");

	for (var i = 0; i < pendingQueryFiles.length; i++)
	{
		var queryFile = pendingQueryFiles.shift();

		console.log("analyst: " + queryFile.analystId + ", versionId: " + queryFile.versionId + ", number of queries: " + queryFile.queries.length);

		// TODO: handle metadata: analystId, versionId, time
		// XXX: update the latest version id
		var aid = queryFile.analystId;
		var versionId = queryFile.versionId;
		if (getLatestVersionId(aid) >= versionId)
		{
			console.log("already processed this version.");
			continue;
		}
		// TODO: extract queries
		var queries = queryFile.queries;
		// TODO: process queries
		var lenqueries = queries.length;

		var answers = [];
		for (var i = 0; i < lenqueries; i++)
		{
			var query = queries[i];
			console.log("processing query no: " + (i+1));
			processQuery(query, aid, versionId);
		}
	}
}

// TODO: call this function periodically
function makeQueryRequestAndGetQueries(aid)
{
	var lastQueryRequestTime = localStorage[aid + '_lastQueryRequestTime'];
	
	var now = new Date().getTime() / 1000.0;
	
	if ((now - lastQueryRequestTime) < QUERY_REQUEST_INTERVAL)
	{
//		console.log("too soon");
		return;
	}
	localStorage[aid + '_lastQueryRequestTime'] = now;

	var latestVersionId = getLatestVersionId(aid);
	
	var queryRequest = new QueryRequest({analystId:aid, latestVersionId:latestVersionId});

	console.log("making a query request: " + queryRequest.analystId + ", latest version id: " + queryRequest.latestVersionId);

	var sid = generateSplitId();
	var sidstr = sid.toString();
//	console.log("sid: " + sidstr);

	var msg = pidCryptUtil.encodeUTF8(serialize(queryRequest));

//	console.log(msg);

	var pieces = split(msg, true);
	var randstr = pieces[0];
	var xorstr = pieces[1];

	var list = [];
	var list2 = [];

	var splitmsg = new SplitMessage({splitId:sidstr, message:randstr});
	list.push(splitmsg);

	var splitmsg2 = new SplitMessage({splitId:sidstr, message:xorstr});
	list2.push(splitmsg2);
	
	// XXX: send rand and xor to different mixes
	try
	{
		var response_mix1 = sendQueryRequestAndRetrieveQueries(mix1Url, list);
		var response_mix2 = sendQueryRequestAndRetrieveQueries(mix2Url, list2);
	}
	catch(e)
	{
//		console.log("proxy down");
		return;
	}
	console.log("query request response received.");
	
//	console.log(response_mix1);
//	console.log(response_mix2);
	// XXX: keep track of received split messages and pair them up
	// XXX: check whether we have a previous split message with the same splitId
	// XXX: if not, add each split message into a global queue
	// XXX: if so, pair them up and put them into a processing queue
	// XXX: periodically process paired up split messages
	for (var i = 0; i < response_mix1.length; i++)
	{
		var sm = response_mix1[i];
		var sid = sm.splitId;
//		console.log("response_mix1: " + sid);
		if (unpairedSplitMessageIds.indexOf(sid) == -1)
		{
//			console.log("unpairable split message; putting into the queue");
			unpairedSplitMessageIds.push(sid);
			unpairedSplitMessages.push(sm);
		}
		else
		{
//			console.log("found an old split message with the same id; gonna pair them up and process it");
			var old_sm = getOldSplitMessage(sid);
			var joinedResponse = joinSplitMessages(sm, old_sm);
			if (joinedResponse != "")
			{
				var queryFile = new QueryFile();
				deserialize(queryFile, joinedResponse);
//				console.log(queryFile);
				pendingQueryFiles.push(queryFile);
			}
		}
	}
	
	for (var i = 0; i < response_mix2.length; i++)
	{
		var sm = response_mix2[i];
		var sid = sm.splitId;
//		console.log("response_mix2: " + sid);
		if (unpairedSplitMessageIds.indexOf(sid) == -1)
		{
//			console.log("unpairable split message; putting into the queue");
			unpairedSplitMessageIds.push(sid);
			unpairedSplitMessages.push(sm);
		}
		else
		{
//			console.log("found an old split message with the same id; gonna pair them up and process it");
			var old_sm = getOldSplitMessage(sid);
			var joinedResponse = joinSplitMessages(sm, old_sm);
			if (joinedResponse != "")
			{
				var queryFile = new QueryFile();
				deserialize(queryFile, joinedResponse);
//				console.log(queryFile);
				pendingQueryFiles.push(queryFile);
			}
		}
	}
	console.log("joined response messages.");
	processPendingQueryFiles();
	
}

