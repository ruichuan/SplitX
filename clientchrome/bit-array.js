/**
 * JavaScript BitArray - v0.2.0
 *
 * Licensed under the revised BSD License.
 * Copyright 2010-2012 Bram Stein
 * All rights reserved.
 */

/**
 * Creates a new empty BitArray with the given length or initialises the BitArray with the given hex representation.
 */
var BitArray = function (size) 
{
    this.values = [];
    for (var i = 0; i < Math.ceil(size / 8); i++) 
    {
    	this.values[i] = 0;
    }
};

/**
 * Returns the total number of bits in this BitArray.
 */
BitArray.prototype.size = function () 
{
	return this.values.length * 8;
};

/**
 * Sets the bit at index to a value (boolean.)
 */
BitArray.prototype.set = function (index, value) 
{
	var i = Math.floor(index / 8);
	// Since "undefined | 1 << index" is equivalent to "0 | 1 << index" we do not need to initialise the array explicitly here.
	if (value) {
		this.values[i] |= 1 << index - i * 8;
	} else {
		this.values[i] &= ~(1 << index - i * 8);
	}
	return this;
};

/**
 * Returns the value of the bit at index (boolean.)
 */
BitArray.prototype.get = function (index) 
{
	var i = Math.floor(index / 8);
	return !!(this.values[i] & (1 << index - i * 8));
};

/**
 * Returns a string representation of the BitArray with bits
 * in logical order.
 */
BitArray.prototype.toString = function () 
{
	return this.toArray().map(function (value) {
		return value ? '1' : '0';
	}).join('');
};

/**
 * Returns a string representation of the BitArray with bits
 * in mathemetical order.
 */
BitArray.prototype.toBinaryString = function () 
{
	return this.toArray().map(function (value) {
		return value ? '1' : '0';
	}).reverse().join('');
};

BitArray.prototype.toByteArray = function ()
{
	var result = [];
	var vals = this.toArray();
	for (var i = 0; i < vals.length; i += 8)
	{
		var b = 0;
//		console.log("i: " + i);
		for (var j = 0; j < 8; j++)
		{
			var k = i + j;
			var m = 7 - j;
	//		console.log("m: " + m + ", k: " + k);
			var v;
			if (vals[k])
			{
				v = (0x1 << m);
			}
			else
			{
				v = (0x0 << m);
			}
//			console.log("v: " + v);
			b += v;
		}
		//console.log(b);
		//console.log(String.fromCharCode(b));
		result.push(b);
	}
	return result;
};

/**
 * Convert the BitArray to an Array of boolean values.
 */
BitArray.prototype.toArray = function () 
{
	var result = [];
    for (var i = 0; i < this.values.length * 8; i++) 
    {
        result.push(this.get(i));
    }
	return result;
};

