// XXX: all queries are executed asynchronously
// XXX: when their results are available, QueryAnswers are generated and put into this list
// TODO: we need to have a function to empty this list periodically
var pendingQueryAnswers = [];

function getClientId()
{
	var cid = localStorage['clientId'];
	if (cid == null)
	{
		cid = generateRand(8);
		cid = new BigInteger(cid, 256);
		console.log("new client id: " + cid);
		localStorage['clientId'] = cid;
	}
	return cid;
}


// XXX: send query request to the mixes and retrieve queries
function sendQueryRequestAndRetrieveQueries(mix_url, list)
{
	var transport = new Thrift.Transport(mix_url);
	var protocol  = new Thrift.Protocol(transport);
	var client = new MixProxyingServiceClient(protocol);
	
	var clientId = getClientId();
	clientId = localStorage['clientId'];
	var response = client.forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses(clientId, list);
	
	return response;	
}

// XXX: second split's one message; dstMixId should be set for the one that is split
function sendQueryAnswersToMix(mix_url, list)
{
	var transport = new Thrift.Transport(mix_url);
	var protocol  = new Thrift.Protocol(transport);
	var client = new MixProxyingServiceClient(protocol);

	client.forward_ClientToMix_QueryAnswers(list);

}

// XXX: third split's messages
function sendQueryAnswersToAggregator(mix_url, list)
{
	var transport = new Thrift.Transport(mix_url);
	var protocol  = new Thrift.Protocol(transport);
	var client = new MixProxyingServiceClient(protocol);

	client.forward_ClientToAggregator_QueryAnswers(list);
	
}

function getLatestVersionId(aid)
{
	var latestVersionId = localStorage[aid + '_latestVersionId'];
	if (latestVersionId == null)
	{
		latestVersionId = -1;
	}
	return latestVersionId;
}

function checkQueryEndTime(qet)
{
	var now = new Date().getTime();
	if ((qet - now) < 0)
	{
		return true;
	}
	return false;
}

function answeredBefore(aid, qid)
{
	var answeredQueries = localStorage[aid + '_answeredQueries'];
	if (answeredQueries == null)
	{
		answeredQueries = [];
	}
	else
	{
		answeredQueries = JSON.parse(answeredQueries);
	}

	if (answeredQueries.indexOf(qid) != -1)
	{
		return true;
	}
	answeredQueries.push(qid);
	localStorage[aid + '_answeredQueries'] = JSON.stringify(answeredQueries);

	return false;
}

function updateDeficit(aid, epsilon)
{
	var deficit = localStorage["deficit_" + aid];
	if (deficit == null)
	{
		deficit = 0.0;
	}
	else
	{
		deficit = parseFloat(deficit);
	}
	deficit += parseFloat(epsilon);
	localStorage["deficit_" + aid] = deficit;
}

function serialize(obj)
{
	var serializer = new Thrift.Protocol(null);
	serializer.tstack = [];
	serializer.tpos = [];
	obj.write(serializer);
	var msg = serializer.tstack.pop();
	return msg;
}

function deserialize(obj, str)
{
	var deserializer = new Thrift.Protocol(null);
	deserializer.rstack = [];
	deserializer.rpos = [];
	deserializer.rstack.push(JSON.parse(str));
	obj.read(deserializer);
	deserializer.readMessageEnd();
}

function generateRand(len)
{
	var rng = new SecureRandom();
	var rand = [];
	for (var i = 0; i < len; i++)
	{
		rand.push(rng.rng_get_byte());
	}
	
	return rand;
}

function generateSplitId()
{
	// 64-bit splitId
	var sid = generateRand(8);
	sid = new BigInteger(sid, 256);
	return sid;
}

function getXOR(msg, rand)
{
	if (msg.length != rand.length)
	{
		console.log("[ERROR] lengths of the message and the random string do not match!");
		return null;
	}
	
	var xor = [];
	
	var len = msg.length;
	for (var i = 0; i < len; i++)
	{
		xor.push(msg[i] ^ rand[i]);
	}
	
	return xor;
}

function getOldSplitMessage(sid)
{
	for (var i = 0; i < unpairedSplitMessages.length; i++)
	{
		if (unpairedSplitMessages[i].splitId == sid)
		{
//			console.log("got old split message");
			var old_sm = unpairedSplitMessages.splice(i, i+1)[0];
			unpairedSplitMessageIds.splice(i, i+1);
			return old_sm;
		}
	}

	// XXX: shouldn't happen
	return null;
}

function joinSplitMessages(sm, old_sm)
{
	if (sm == null || old_sm == null)
	{
		console.log("an old split message that is not there any more");
		var index;
		if (sm == null)
		{
			index = unpairedSplitMessageIds.indexOf(old_sm);
		}
		else
		{
			index = unpairedSplitMessageIds.indexOf(sm);
		}
		unpairedSplitMessageIds.splice(index, index+1);
		return "";
	}
	var rmsg = sm.message;
	rmsg = pidCryptUtil.toByteArray(rmsg);
	var rmsg2 = old_sm.message;
	rmsg2 = pidCryptUtil.toByteArray(rmsg2);
	
	var actual = getXOR(rmsg, rmsg2);
	var actualstr = pidCryptUtil.byteArray2String(actual);
//	console.log("joined: " + actualstr);
	
	return actualstr;	
}

function split(msg, convert)
{
	var pieces = [];

	msg = pidCryptUtil.toByteArray(msg);
	
	var rand = generateRand(msg.length);
	var xor = getXOR(msg, rand);

	if (convert)
	{
		var randstr = pidCryptUtil.byteArray2String(rand);
		pieces.push(randstr);
//		console.log("randstr: " + randstr);
	
		var xorstr = pidCryptUtil.byteArray2String(xor);
		pieces.push(xorstr);
//		console.log("xorstr: " + xorstr);
	}
	else
	{
		pieces.push(rand);
		pieces.push(xor);
	}
	
	return pieces;
}

