var loc;
var prevLoc = "";

function sendToBackgroundPage(str, con)
{
	chrome.extension.sendMessage({msg: str, content: con}, function(response){
					console.log(response.value);
			});
}

function continueExecution()
{
	loc = window.location.href;

	if (loc != prevLoc)
	{
    	var ref = document.referrer;
    	console.log("referrer: " + ref);
		sendToBackgroundPage("referrer", ref);
		prevLoc = loc;
	}

	
	setTimeout(doStuff, 300);
}


function doStuff()
{
    //do some things
    continueExecution();
}

setTimeout(doStuff, 300);

//window.addEventListener("load", function() {doStuff();}, false);

