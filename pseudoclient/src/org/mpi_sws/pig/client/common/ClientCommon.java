package org.mpi_sws.pig.client.common;

public class ClientCommon {
	
	public static String mix_1_host = "localhost";
	public static int mix_1_receiving_port = 51084;
	public static int mix_1_proxying_port = 51085;
	
	public static String mix_2_host = "localhost";
	public static int mix_2_receiving_port = 51084;
	public static int mix_2_proxying_port = 51085;

}
