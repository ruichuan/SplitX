package org.mpi_sws.pig.client;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TJSONProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.mpi_sws.pig.common.message.SplitMessage;
import org.mpi_sws.pig.common.service.MixProxyingService;
import org.mpi_sws.pig.common.utility.TJSONProtocolFixedB64;

public class Client {

	/**
	 * @param args
	 * @throws TException 
	 */
	public static void main(String[] args) throws TException {
		// TODO Auto-generated method stub
		
		TTransport transport = new TFramedTransport(new TSocket("localhost", 51084));
		TProtocol protocol = new TJSONProtocolFixedB64(transport);
		MixProxyingService.Client client = new MixProxyingService.Client(protocol);
		
		transport.open();
		
		String clientId = "123";
		List<SplitMessage> list = new ArrayList<SplitMessage>();
		
		SplitMessage split = new SplitMessage((long)1, ByteBuffer.wrap(new byte[]{1, 2}));
		SplitMessage split1 = new SplitMessage((long)2, ByteBuffer.wrap(new byte[]{1, 2}));
		
		list.add(split);
		list.add(split1);
		
		client.forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses(clientId, list);
		
		transport.close();

	}

}
