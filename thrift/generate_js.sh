#!/bin/bash

curdir=$(pwd)
echo $curdir

bdir="$curdir"/"gen-js"

mkdir -p $bdir

filelist=$(find . -name *.thrift)

for f in $filelist
do
	echo $f
	d=$(dirname ${f})
	curdir=$bdir"${d:1}"
	echo $curdir
	
	mkdir -p $curdir
	
	thrift --gen js -out $curdir $f
	
done
