namespace java org.mpi_sws.pig.common.message

struct QueryResult {
	1: i64 queryId,
	2: list<i32> counts,
	3: optional list<i32> bucketSizes
}
