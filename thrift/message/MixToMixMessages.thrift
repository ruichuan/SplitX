namespace java org.mpi_sws.pig.common.message

struct SidList {
	1: string analystId,
	2: i64 queryId,
	3: list<i64> splitIds,
	4: i64 seed
}

struct SidListResponse {
	1: string analystId,
	2: i64 queryId,
	3: list<i64> splitIds
}
