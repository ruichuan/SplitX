namespace java org.mpi_sws.pig.common.message

struct QueryRequest {
	1: string analystId,
	2: i64 latestVersionId
}
