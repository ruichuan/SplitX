namespace java org.mpi_sws.pig.common.message

struct QueryNoisyAnswers {
	1: string analystId,
	2: i64 queryId,
	3: list<binary> answers,
	4: i32 noise
}
