namespace java org.mpi_sws.pig.common.message

enum BucketType {
	NUMERICAL_MATCHING,
	STRING_MATCHING
}

struct Buckets {
	1: BucketType type,
	2: list<string> ranges
}

struct Query {
	1: i64 queryId,
	2: optional i64 queryStartTime,
	3: i64 queryEndTime,
	4: optional i64 repeatInterval,
	5: double epsilon,
	6: string sql,
	7: Buckets buckets,
	8: optional double percentileBound,
	9: optional i32 absoluteBound
}

struct QueryFile {
	1: string analystId,
	2: i64 versionId,
	3: optional i64 time,
	4: list<Query> queries
}
