namespace java org.mpi_sws.pig.common.message

struct QueryAnswer {
	1: string analystId,
	2: i64 queryId,
	3: i64 splitId,
	4: binary answerBits
}
