namespace java org.mpi_sws.pig.common.message

struct SplitMessage {
	1: i64 splitId,
	2: binary message,
	3: optional byte dstMixId
}
