namespace java org.mpi_sws.pig.common.service

include "../../message/SplitMessages.thrift"

service MixProxyingService {
	list<SplitMessages.SplitMessage> forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses (1: string clientId, 2: list<SplitMessages.SplitMessage> queryRequests),
	void forward_ClientToAggregator_QueryAnswers (1: list<SplitMessages.SplitMessage> queryAnswers),
	void forward_ClientToMix_QueryAnswers (1: list<SplitMessages.SplitMessage> queryAnswers)
}
