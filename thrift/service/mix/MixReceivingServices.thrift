namespace java org.mpi_sws.pig.common.service

include "../../message/SplitMessages.thrift"
include "../../message/MixToMixMessages.thrift"

service MixReceivingService {
	void forward_AggregatorToClient_QueryResponses (1: list<SplitMessages.SplitMessage> queryResponses),
	void send_ClientToMix_QueryAnswers (1: list<SplitMessages.SplitMessage> queryAnswers),
	void send_AggregatorToMix_QueryAnswers (1: list<SplitMessages.SplitMessage> queryAnswers),
	MixToMixMessages.SidListResponse negotiate_MixToMix_SidList (1: MixToMixMessages.SidList sidList),
	list<binary> resize_AggregatorToMix_Buckets (1: string analystId, 2: i64 queryId, 3: list<i32> bucketSizes, 4: i64 resizingTimes)
}
