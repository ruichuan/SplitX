namespace java org.mpi_sws.pig.common.service

include "../../message/SplitMessages.thrift"
include "../../message/MixToAggregatorMessages.thrift"
include "../../message/QueryDescription.thrift"
include "../../message/AggregatorToAnalystMessages.thrift"

service AggregatorReceivingService {
	void send_ClientToAggregator_QueryRequests (1: list<SplitMessages.SplitMessage> queryRequests),
	void send_ClientToAggregator_QueryAnswers (1: list<SplitMessages.SplitMessage> queryAnswers),
	void send_MixToAggregator_QueryNoisyAnswers (1: MixToAggregatorMessages.QueryNoisyAnswers noisyAnswers),
	list<QueryDescription.QueryFile> retrieve_MixToAggregator_QueryDescription (1: i64 latestVersionId),
	void upload_AnalystToAggregator_Queries (1: string analystId, 2: list<QueryDescription.Query> queries),
	list<AggregatorToAnalystMessages.QueryResult> download_AggregatorToAnalyst_QueryResults (1: string analystId)
}
