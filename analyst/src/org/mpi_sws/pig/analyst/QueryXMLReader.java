package org.mpi_sws.pig.analyst;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.thrift.TException;
import org.mpi_sws.pig.common.message.BucketType;
import org.mpi_sws.pig.common.message.Buckets;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.message.QueryFile;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/*
 * @author Istemi Ekin Akkus
 * 
 * 
 */

/*
 * 
 * Reads an XML file and generates a Query 
 * 
 */

public class QueryXMLReader extends DefaultHandler 
{
	private QueryFile queryFile = null;
	private String lastValue = "";
	
	public QueryXMLReader(String filename)
	{
		File xmlFile = new File(filename);
		this.queryFile = new QueryFile();
		this.generate(xmlFile);
		// XXX: Test
	//	this.initQueryFile();
	}
	
	private void generate(File file)
	{
		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setNamespaceAware(true);
		spf.setValidating(true);
//		System.out.println("Parser will " + (spf.isNamespaceAware() ? "" : "not ") + "be namespace aware");
//		System.out.println("Parser will " + (spf.isValidating() ? "" : "not ") + "validate XML");
		try 
		{
			SAXParser parser = spf.newSAXParser();
//			System.out.println("Parser object is: " + parser);
			parser.parse(file, this);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.exit(1);
		}		
	}

	public void startDocument() 
	{
//		System.out.println("Start document: ");
	}

	public void endDocument() 
	{
//		System.out.println("End document: ");
	}

	public void startElement(String uri, String localName, String qname, Attributes attr) 
	{
//		System.out.println("Start element: local name: " + localName + " qname: " + qname + " uri: " + uri);
		if (qname.equalsIgnoreCase("Query"))
		{
			Query q = new Query();
			this.queryFile.addToQueries(q);			
		}
		if (qname.equalsIgnoreCase("Buckets"))
		{
			List<Query> list = this.queryFile.getQueries();
			Query q = list.get(list.size()-1);
			Buckets b = new Buckets();
			q = q.setBuckets(b);
		}
	}

	public void endElement(String uri, String localName, String qname) 
	{
//		System.out.println("End element: local name: " + localName + " qname: " + qname + " uri: " + uri);
		if (qname.equalsIgnoreCase("analystId"))
		{
			this.queryFile.setAnalystId(lastValue);
		}
		else if (qname.equalsIgnoreCase("versionId"))
		{
			this.queryFile.setVersionId(Long.parseLong(lastValue));
		}
		else if (qname.equalsIgnoreCase("time"))
		{
			if (!lastValue.equalsIgnoreCase(""))
			{
				this.queryFile.setTime(Long.parseLong(lastValue));
			}
		}
		else if (qname.equalsIgnoreCase("queryId"))
		{
			List<Query> list = this.queryFile.getQueries();
			Query q = list.get(list.size()-1);
			q.setQueryId(Long.parseLong(lastValue));
		}
		else if (qname.equalsIgnoreCase("queryStartTime"))
		{
			if (!lastValue.equalsIgnoreCase(""))
			{
				List<Query> list = this.queryFile.getQueries();
				Query q = list.get(list.size()-1);
				q = q.setQueryStartTime(Long.parseLong(lastValue));
			}			
		}
		else if (qname.equalsIgnoreCase("queryEndTime"))
		{
			List<Query> list = this.queryFile.getQueries();
			Query q = list.get(list.size()-1);
			q = q.setQueryEndTime(Long.parseLong(lastValue));
		}
		else if (qname.equalsIgnoreCase("repeatInterval"))
		{
			if (!lastValue.equalsIgnoreCase(""))
			{
				List<Query> list = this.queryFile.getQueries();
				Query q = list.get(list.size()-1);
				q = q.setRepeatInterval(Long.parseLong(lastValue));
			}
		}
		else if (qname.equalsIgnoreCase("epsilon"))
		{
			List<Query> list = this.queryFile.getQueries();
			Query q = list.get(list.size()-1);
			q = q.setEpsilon(Double.parseDouble(lastValue));			
		}
		else if (qname.equalsIgnoreCase("sql"))
		{
			List<Query> list = this.queryFile.getQueries();
			Query q = list.get(list.size()-1);
			q = q.setSql(lastValue);
		}
		else if (qname.equalsIgnoreCase("BucketType"))
		{
			List<Query> list = this.queryFile.getQueries();
			Query q = list.get(list.size()-1);
			Buckets b = q.getBuckets();
			int bt = Integer.parseInt(lastValue);
			if (bt == 0)
			{
				b = b.setType(BucketType.NUMERICAL_MATCHING);
			}
			else if (bt == 1)
			{
				b = b.setType(BucketType.STRING_MATCHING);				
			}
		}
		else if (qname.equalsIgnoreCase("range"))
		{
			List<Query> list = this.queryFile.getQueries();
			Query q = list.get(list.size()-1);
			Buckets b = q.getBuckets();
			b.addToRanges(lastValue);
		}
		else if (qname.equalsIgnoreCase("percentileBound"))
		{
			if (!lastValue.equalsIgnoreCase(""))
			{
				List<Query> list = this.queryFile.getQueries();
				Query q = list.get(list.size()-1);
				q = q.setPercentileBound(Double.parseDouble(lastValue));
			}
		}
		else if (qname.equalsIgnoreCase("absoluteBound"))
		{
			if (!lastValue.equalsIgnoreCase(""))
			{
				List<Query> list = this.queryFile.getQueries();
				Query q = list.get(list.size()-1);
				q = q.setAbsoluteBound(Integer.parseInt(lastValue));
			}
		}
	}

	public void characters(char[] ch, int start, int length) 
	{
//		System.out.println("Characters: " + new String(ch, start, length));
		this.lastValue = new String(ch, start, length).trim();
	}

	public void ignorableWhitespace(char[] ch, int start, int length) 
	{
//		System.out.println("Ignorable whitespace: " + new String(ch, start, length));
	}
	
	public QueryFile getQueryFile()
	{
		return this.queryFile;
	}


	// XXX: Test
	private void initQueryFile()
	{
		// XXX: Let's create a dummy QueryFile so that we can test our client
		// XXX: We'll assume that both split messages are distributed from the same mix
		// for testing purposes. Normally, they will be forwarded through separate mixes
		
		// XXX: sample query for numerical matching
		// bucket ranges
		List<String> ranges1 = new ArrayList<String>();
		ranges1.add("0,5");
		ranges1.add("6,10");
		ranges1.add("11,15");
		ranges1.add("16,20");
		ranges1.add("21,25");
		ranges1.add("26,30");
		ranges1.add("0,15");
		ranges1.add("16,30");
		
		// buckets
		Buckets buckets1 = new Buckets(BucketType.NUMERICAL_MATCHING, ranges1);
		// query
		Query q1 = new Query((long)1, (long)1000000, 2.0, "SELECT COUNT(extension_id) FROM Extensions", buckets1);
		
		// XXX: sample query for string matching
		List<String> ranges2 = new ArrayList<String>();
		ranges2.add("Google");
		ranges2.add("Bing");
		ranges2.add("Amazon");
		ranges2.add("Yahoo");
		ranges2.add("Ebay");
		ranges2.add("Ask.com");
		ranges2.add("Baidu");
		ranges2.add("Shopping.com");
		
		Buckets buckets2 = new Buckets(BucketType.STRING_MATCHING, ranges2);
		
		Query q2 = new Query((long)2, (long)1000000, 2.0, "SELECT DISTINCT search_engine FROM SearchItems", buckets2);

		// XXX: Query list
		List<Query> queries = new ArrayList<Query>();
		queries.add(q1);
		queries.add(q2);
		
		
		// XXX: Query file
		this.queryFile = new QueryFile("analyst_1", 1, queries);
	}

}