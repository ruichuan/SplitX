package org.mpi_sws.pig.analyst.common;

import org.mpi_sws.pig.common.Common;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class AnalystCommon {
	public static String aggregator_host = Common.aggregator_host;
	public static int aggregator_receiving_port = Common.aggregator_receiving_port;
}
