package org.mpi_sws.pig.analyst;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.mpi_sws.pig.analyst.common.AnalystCommon;
import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.message.QueryResult;
import org.mpi_sws.pig.common.service.AggregatorReceivingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class ResultDownloader {
	
	private static final Logger logger = LoggerFactory.getLogger(ResultDownloader.class);
	
	public void download (String analystId, String path) {
		TTransport transport = new TFramedTransport(new TSocket(AnalystCommon.aggregator_host, AnalystCommon.aggregator_receiving_port), Common.max_frame_size);
		TProtocol protocol = new TCompactProtocol(transport);
		AggregatorReceivingService.Client client = new AggregatorReceivingService.Client(protocol);
		
		try {
			transport.open();
			List<QueryResult> results = client.download_AggregatorToAnalyst_QueryResults(analystId);
			logger.info("Downloaded the results of {} queries for analyst {}.", results.size(), analystId);
			
			for (QueryResult result : results) {
				long queryId = result.getQueryId();
				List<Integer> counts = result.getCounts();
				List<Integer> bucketSizes = null;
				if (result.isSetBucketSizes()) {
					bucketSizes = result.getBucketSizes();
				}
				
				write_result(path, analystId, queryId, counts, bucketSizes);
			}
		} catch (TTransportException e) {
			logger.error("Connecting to aggregator.", e);
		} catch (TException e) {
			logger.error("Downloading query results for analyst {}.", analystId, e);
		} finally {
			if (transport != null) {
				transport.close();
			}
		}
	}
	
	private void write_result (String path, String analystId, long queryId, List<Integer> counts, List<Integer> bucketSizes) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(path + analystId + "_" + queryId + "_" + System.currentTimeMillis() + ".txt"));
			
			if (bucketSizes == null) {
				for (int i = 0; i < counts.size(); ++i) {
					bw.write("1, " + String.valueOf(counts.get(i)));
					bw.newLine();
				}
			} else {
				for (int i = 0; i < counts.size(); ++i) {
					bw.write(String.valueOf(bucketSizes.get(i)) + ", " + String.valueOf(counts.get(i)));
					bw.newLine();
				}
			}
			
			bw.flush();
			logger.info("Wrote result for analyst {}'s query {} into disk.", analystId, queryId);
		} catch (IOException e) {
			logger.error("Writing result for analyst {}'s query {} into disk.", analystId, queryId, e);
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {}
			}
		}
	}

	public static void main(String[] args) {
		
		if (args.length != 2) {
			System.err.println("Usage: need to specify the analyst ID and the result folder.");
			System.exit(1);
		}
		
		String analystId = args[0].trim();
		if (analystId.isEmpty()) {
			System.err.println("Usage: need to specify the analyst ID.");
			System.exit(1);
		}
		
		String path = args[1].trim();
		if (path.isEmpty()) {
			System.err.println("Usage: need to specify the result folder.");
			System.exit(1);
		}
		
		new ResultDownloader().download(analystId, path);
	}
}
