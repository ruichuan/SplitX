package org.mpi_sws.pig.analyst;

import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.mpi_sws.pig.analyst.common.AnalystCommon;
import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.message.BucketType;
import org.mpi_sws.pig.common.message.Buckets;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.message.QueryFile;
import org.mpi_sws.pig.common.service.AggregatorReceivingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 * @author Istemi Ekin Akkus
 *
 */
public class QueryUploader {
	
	private static final Logger logger = LoggerFactory.getLogger(QueryUploader.class);
	
	public void upload_one_query (String analystId, long queryId, long queryEndTime, double epsilon, String sql, int bucketType, List<String> bucketRanges) {
		List<Query> queries = new ArrayList<Query>(1);
		queries.add(new Query(queryId, queryEndTime, epsilon, sql, new Buckets(BucketType.findByValue(bucketType), bucketRanges)));
		
		TTransport transport = new TFramedTransport(new TSocket(AnalystCommon.aggregator_host, AnalystCommon.aggregator_receiving_port), Common.max_frame_size);
		TProtocol protocol = new TCompactProtocol(transport);
		AggregatorReceivingService.Client client = new AggregatorReceivingService.Client(protocol);
		
		try {
			transport.open();
			client.upload_AnalystToAggregator_Queries(analystId, queries);
			logger.info("Uploaded analyst {}'s query {} to aggregator.", analystId, queryId);
		} catch (TTransportException e) {
			logger.error("Connecting to aggregator.", e);
		} catch (TException e) {
			logger.error("Uploading analyst {}'s query {} to aggregator.", analystId, queryId, e);
		} finally {
			if (transport != null) {
				transport.close();
			}
		}
	}
	
	public void upload_queries(QueryFile qf, int timeout)
	{
		String analystId = qf.getAnalystId();
		List<Query> queries = qf.getQueries();
		
		long now = System.currentTimeMillis();
		long qet = now + timeout * 1000;
		for (int i = 0; i < queries.size(); i++)
		{
			queries.get(i).setQueryId(now + i);
			System.out.println("query id: " + (now + i));
			queries.get(i).setQueryEndTime(qet);
		}
		
		TTransport transport = new TFramedTransport(new TSocket(AnalystCommon.aggregator_host, AnalystCommon.aggregator_receiving_port), Common.max_frame_size);
		TProtocol protocol = new TCompactProtocol(transport);
		AggregatorReceivingService.Client client = new AggregatorReceivingService.Client(protocol);
		
		try {
			transport.open();
			client.upload_AnalystToAggregator_Queries(analystId, queries);
//			logger.info("Uploaded analyst {}'s query {} to aggregator.", analystId, queries.toString());
		} catch (TTransportException e) {
			logger.error("Connecting to aggregator.", e);
		} catch (TException e) {
			logger.error("Uploading analyst {}'s query {} to aggregator.", analystId, queries.toString(), e);
		} finally {
			if (transport != null) {
				transport.close();
			}
		}
		
	}

	public static void main(String[] args) 
	{
		QueryFile qf = null;
		try 
		{
			if (args.length < 2)
			{
				System.err.println("Usage: QueryUploader <query xml filename> <timeout (sec)>\n");
				System.err.println("The xml file can contain more than one query.");
				return;
			}
			
			// TODO: sanity checking on the queries
			
			String filename = args[0];
			QueryXMLReader qxr = new QueryXMLReader(filename);
			qf = qxr.getQueryFile();
			
			int timeout = Integer.parseInt(args[1]);
			
			new QueryUploader().upload_queries(qf, timeout);
			
//			String analystId = args[0].trim();
//			long queryId = Long.valueOf(args[1]);
//			long queryEndTime = Long.valueOf(args[2]);
//			double epsilon = Double.valueOf(args[3]);
//			String sql = args[4].trim();
//			int bucketType = Integer.valueOf(args[5]);
//			List<String> bucketRanges = new ArrayList<String>(args.length - 6);
//			for (int i = 6; i < args.length; ++i) {
//				bucketRanges.add(args[i].trim());
//			}
//			
//			if (analystId.isEmpty() || queryEndTime <= System.currentTimeMillis() || epsilon <= 0 || sql.isEmpty() || (bucketType != 0 && bucketType != 1) || bucketRanges.size() <= 0) {
//				System.err.println("Usage: need to specify the following parameters -- analystId queryId queryEndTime epsilon sql bucketType bucketRanges");
//				System.exit(1);
//			}
			
//			new QueryUploader().upload_one_query(analystId, queryId, queryEndTime, epsilon, sql, bucketType, bucketRanges);
			
		} catch (NumberFormatException e) {
			System.err.println("Usage: need to specify the following parameters -- analystId queryId queryEndTime epsilon sql bucketType bucketRanges");
			System.exit(1);
		}
	}
}
