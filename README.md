Codebase of the SplitX system.

Ruichuan Chen, Istemi Ekin Akkus, and Paul Francis, "SplitX: High-Performance Private Analytics". In SIGCOMM 2013, Hong Kong, China, August 2013.