package test;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;

import test.CourseService.Iface;

public class Server {

	/**
	 * @param arg
	 * @throws TException 
	 */
	public static void main(String[] arg) throws TException {
		// TODO Auto-generated method stub
		
        TNonblockingServerTransport trans = new TNonblockingServerSocket(9999);
        TThreadedSelectorServer.Args args = new TThreadedSelectorServer.Args(trans);
        args.transportFactory(new TFramedTransport.Factory(Integer.MAX_VALUE));
        args.protocolFactory(new TCompactProtocol.Factory());
        args.processor(new CourseService.Processor<Iface>(new CourseServiceImpl()));
        args.selectorThreads(4);
        args.workerThreads(4);
        TServer server = new TThreadedSelectorServer(args);
        
        server.serve();
	}
}
