package test;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

public class Client {

	/**
	 * @param args
	 * @throws TException 
	 */
	public static void main(String[] args) throws TException {
		// TODO Auto-generated method stub
		
		
        TTransport transport = new TFramedTransport(new TSocket("localhost", 9999), Integer.MAX_VALUE);
        TProtocol protocol = new TCompactProtocol(transport);
        CourseService.Client client = new CourseService.Client(protocol);
        transport.open();
        int bean = client.getCourse();
        transport.close();
        System.out.println(bean);
	}

}
