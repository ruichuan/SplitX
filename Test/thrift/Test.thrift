namespace java test

struct Course {
	1: i32 id
}

service CourseService {
	i32 getCourse()
}
