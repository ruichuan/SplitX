package org.mpi_sws.pig.mix.database;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class Answers {
  
  private int size = 0;
  private List<Long> sid_list = new ArrayList<Long>();
  private List<byte[]> answer_list = new ArrayList<byte[]>();
  
  /**
   * add answer
   * @param sid split ID
   * @param answer answer bits
   */
  public synchronized void add_answer (long sid, byte[] answer) {
    sid_list.add(sid);
    answer_list.add(answer);
    ++size;
  }
  
  /**
   * remove answer
   * @param index position (0 <= position < size)
   */
  public synchronized void remove_answer (int index) {
    if (index < 0 || index >= size) {
      throw new IndexOutOfBoundsException();
    }
    
    sid_list.remove(index);
    answer_list.remove(index);
    --size;
  }
  
  /**
   * # answers
   * @return # answers
   */
  public synchronized int size () {
    return size;
  }
  
  /**
   * clear all answers
   */
  public synchronized void clear () {
    sid_list.clear();
    answer_list.clear();
    size = 0;
  }
  
  /**
   * get all SIDs
   * @return all SIDs
   */
  public List<Long> get_sid_list () {
    return sid_list;
  }
  
  /**
   * get all answer bits
   * @return all answer bits
   */
  public List<byte[]> get_answer_list () {
    return answer_list;
  }
}
