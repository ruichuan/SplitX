package org.mpi_sws.pig.mix.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.mpi_sws.pig.mix.common.MixCommon;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class CommonAnswerDBAccess {

	private Connection conn = null;
	private PreparedStatement stmt_insert = null;
	
	public void open (String dbName, boolean isNew) throws ClassNotFoundException, SQLException {
		Class.forName(MixCommon.jdbc_driver);
		conn = DriverManager.getConnection(MixCommon.db_name_prefix + dbName + ".db");
		
		Statement stmt = null;
		
		try {
			if (isNew) {
				stmt = conn.createStatement();
				stmt.executeUpdate("DROP TABLE IF EXISTS Common");
				stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Common (Answer_Bits BLOB NOT NULL)");
			}
			
			if (stmt_insert == null) {
				stmt_insert = conn.prepareStatement("INSERT INTO Common (Answer_Bits) VALUES (?)");
			}
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}
	
	public void close () throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
	
	protected void finalize() throws Throwable {
		try {
			this.close();
		} finally {
			super.finalize();
		}
	}
	
	public synchronized List<byte[]> retrieveAll () throws SQLException {
		Statement stmt = null;
		List<byte[]> answers = null;
		
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT Answer_Bits FROM Common");
			answers = new ArrayList<byte[]>();
			
			while (rs.next()) {
				byte[] answer_bits = rs.getBytes(1);
				answers.add(answer_bits);
			}
			
			return answers;
			
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}
	
	public synchronized void insertAnswers (List<byte[]> answers) throws SQLException {
		try {
			conn.setAutoCommit(false);
			
			for (byte[] answer : answers) {
				stmt_insert.setBytes(1, answer);
				stmt_insert.addBatch();
			}
			stmt_insert.executeBatch();
			
			conn.commit();
			
		} catch (SQLException e) {
			if (conn != null) {
				conn.rollback();
			}
			throw e;
		} finally {
			conn.setAutoCommit(true);
		}
	}
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		
		CommonAnswerDBAccess dba = new CommonAnswerDBAccess();
		dba.open("test", true);
		
		List<byte[]> answers = new ArrayList<byte[]>();
		answers.add("a1234".getBytes());
		answers.add("b2345".getBytes());
		answers.add("c3456".getBytes());
		
		dba.insertAnswers(answers);
		
		List<byte[]> results = dba.retrieveAll();
		
		for (byte[] result : results) {
			System.out.println(result);
		}
		
		dba.close();
	}
}
