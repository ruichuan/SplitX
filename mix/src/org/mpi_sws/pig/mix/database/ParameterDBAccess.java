package org.mpi_sws.pig.mix.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.mpi_sws.pig.mix.common.MixCommon;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class ParameterDBAccess {

	private Connection conn = null;
	
	public void open (String dbName, boolean isNew) throws ClassNotFoundException, SQLException {
		Class.forName(MixCommon.jdbc_driver);
		conn = DriverManager.getConnection(MixCommon.db_name_prefix + dbName + ".db");
		
		Statement stmt = null;
		
		try {
			if (isNew) {
				stmt = conn.createStatement();
				stmt.executeUpdate("DROP TABLE IF EXISTS Parameters");
				stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Parameters (Name TEXT PRIMARY KEY, Value INTEGER NOT NULL)");
			}
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}
	
	public void close () throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
	
	protected void finalize() throws Throwable {
		try {
			this.close();
		} finally {
			super.finalize();
		}
	}
	
	public long getParameter (String name) throws SQLException {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT Value FROM Parameters WHERE Name='" + name + "'");
			rs.next();
			return rs.getLong(1);
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}
	
	public void setParameter (String name, long value) throws SQLException {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate("INSERT OR REPLACE INTO Parameters (Name, Value) VALUES ('" + name + "', " + value + ")");
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		
		ParameterDBAccess dba = new ParameterDBAccess();
		dba.open("test", true);
		
		dba.setParameter("abc", 123);
		dba.setParameter("xyz", 789);
		
		System.out.println(dba.getParameter("abc"));
		System.out.println(dba.getParameter("xyz"));
		
		dba.close();
	}
}
