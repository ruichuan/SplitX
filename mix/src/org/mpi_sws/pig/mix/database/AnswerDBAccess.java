package org.mpi_sws.pig.mix.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.mpi_sws.pig.mix.common.MixCommon;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class AnswerDBAccess {

	private Connection conn = null;
	private PreparedStatement stmt_insert = null;
	
	/**
	 * open an answer database
	 * @param dbName name of the answer database
	 * @param isNew whether this database is new
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void open (String dbName, boolean isNew) throws ClassNotFoundException, SQLException {
		Class.forName(MixCommon.jdbc_driver);
		conn = DriverManager.getConnection(MixCommon.db_name_prefix + dbName + ".db");
		
		Statement stmt = null;
		
		try {
			if (isNew) {
				stmt = conn.createStatement();
				stmt.executeUpdate("DROP TABLE IF EXISTS Answers");
				stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Answers (Split_ID INTEGER PRIMARY KEY ASC, Answer_Bits BLOB NOT NULL)");
			}
			
			if (stmt_insert == null) {
				stmt_insert = conn.prepareStatement("INSERT OR IGNORE INTO Answers (Split_ID, Answer_Bits) VALUES (?, ?)");
			}
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}
	
	/**
	 * close a database connection
	 * @throws SQLException
	 */
	public void close () throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
	
	protected void finalize() throws Throwable {
		try {
			this.close();
		} finally {
			super.finalize();
		}
	}
	
	/**
	 * retrieve all answers from database
	 * @return all answers
	 * @throws SQLException
	 */
	public synchronized Answers retrieveAll () throws SQLException {
		Statement stmt = null;
		Answers answers = null;
		
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT Split_ID, Answer_Bits FROM Answers");
			answers = new Answers();
			
			while (rs.next()) {
				long split_ID = rs.getLong(1);
				byte[] answer_bits = rs.getBytes(2);
				answers.add_answer(split_ID, answer_bits);
			}
			
			return answers;
			
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}
	
	/**
	 * insert a number of answers into the database
	 * @param answers answers being inserted
	 * @throws SQLException
	 */
	public synchronized void insertAnswers (ConcurrentMap<Long, byte[]> answers) throws SQLException {
		try {
			conn.setAutoCommit(false);
			
			for (Iterator<Entry<Long, byte[]>> it_answer = answers.entrySet().iterator(); it_answer.hasNext(); ) {
				Entry<Long, byte[]> answer = it_answer.next();
				
				stmt_insert.setLong(1, answer.getKey());
				stmt_insert.setBytes(2, answer.getValue());
				stmt_insert.addBatch();
				
				it_answer.remove();
			}
			
			stmt_insert.executeBatch();
			conn.commit();
			
		} catch (SQLException e) {
			if (conn != null) {
				conn.rollback();
			}
			throw e;
		} finally {
			conn.setAutoCommit(true);
		}
	}
	
	/**
	 * @param args
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		
		AnswerDBAccess dba = new AnswerDBAccess();
		dba.open("test", true);
		
		ConcurrentMap<Long, byte[]> answers = new ConcurrentHashMap<Long, byte[]>();
		answers.put(Long.MAX_VALUE, "max".getBytes());
		answers.put((long)1234, "a1234".getBytes());
		answers.put((long)3456, "c3456".getBytes());
		answers.put(Long.MIN_VALUE, "min".getBytes());
    System.out.println(answers.size());
		dba.insertAnswers(answers);
		System.out.println(answers.size());
		
    answers.put((long)4567, "d4567".getBytes());
    answers.put((long)2345, "b2345".getBytes());
    System.out.println(answers.size());
    dba.insertAnswers(answers);
    System.out.println(answers.size());
    
    ConcurrentMap<String, AnswerDBAccess> map = new ConcurrentHashMap<String, AnswerDBAccess>();
    map.put("test", dba);
    
    AnswerDBAccess dba1 = map.get("test");
    
    answers.put((long)5544, "5544".getBytes());
    answers.put((long)2211, "2211".getBytes());
    System.out.println(answers.size());
    dba1.insertAnswers(answers);
    System.out.println(answers.size());
    
		Answers results = dba1.retrieveAll();
		
		List<Long> sid_list = results.get_sid_list();
		List<byte[]> answer_list = results.get_answer_list();
		
		for (int i = 0; i < results.size(); ++i) {
		  System.out.println(sid_list.get(i) + " --> " + answer_list.get(i));
		}
		
		System.out.println(dba == dba1);
		
		dba.close();
	}
}
