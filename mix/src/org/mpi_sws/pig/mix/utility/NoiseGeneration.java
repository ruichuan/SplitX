package org.mpi_sws.pig.mix.utility;

import java.util.List;
import java.util.Random;

import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.utility.SeedGeneration;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class NoiseGeneration {
	
	private static final byte[] mask = new byte[]{(byte)0x7F, (byte)0xBF, (byte)0xDF, (byte)0xEF, (byte)0xF7, (byte)0xFB, (byte)0xFD, (byte)0xFE};
	
	public static int generate (List<byte[]> answer_list, Query query) {
		int c = answer_list.size();
		double epsilon = query.getEpsilon();
		if (c <= 0 || epsilon <= 0) {
			return 0;
		}
		
		int length = Common.special_bytes_in_answer + (query.getBuckets().getRangesSize() + 7) / 8;
		int noise = (int)(64 * Math.log(2 * c) / epsilon / epsilon + 1);
		if (noise % 2 != 0) {
			++noise;
		}
		Random rnd = new Random (SeedGeneration.generate());
		
		for (int i = 0; i < noise; ++i) {
			byte[] answer = new byte[length];
			rnd.nextBytes(answer);
			answer_list.add(answer);
		}
		
		return noise;
	}
	
	public static void generate_for_resizing (List<byte[]> answer_list, int length, int noise, List<Integer> sizes) {
		Random rnd = new Random (SeedGeneration.generate());
		for (int i = 0; i < noise; ++i) {
			byte[] answer = new byte[length];
			rnd.nextBytes(answer);
			answer_list.add(answer);
		}
		
		int start = 0;
		int end = 0;
		for (int size : sizes) {
			end = start + size;
			for (int i = start; i < end - 1; ++i) {
				clear_bit (answer_list, noise, i);
			}
			start = end;
		}
	}
	
	private static void clear_bit (List<byte[]> answer_list, int noise, int pos) {
		int end = answer_list.size();
		int start = end - noise;
		int pos_byte = pos / 8;
		int pos_bit = pos % 8;
		
		for (int i = start; i < end; ++i) {
			byte[] answer = answer_list.get(i);
			answer[pos_byte] &= mask[pos_bit];
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
