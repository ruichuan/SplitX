package org.mpi_sws.pig.mix.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class AnswerShuffling {
	
	private static final byte[] mask = new byte[]{(byte)(1 << 7), (byte)(1 << 6), (byte)(1 << 5), (byte)(1 << 4), (byte)(1 << 3), (byte)(1 << 2), (byte)(1 << 1), (byte)(1 << 0)};
	
	public static Random shuffle (List<byte[]> answer_list, long seed) {
		Random rnd = new Random (seed); // synchronize the shuffling between two mixes

		int n_answers = answer_list.size(); // # answers
		if (n_answers < 2) {
			return rnd;
		}
		int n_bytes = answer_list.get(0).length;  // # bytes in each answer
		
		// traverse every byte
		for (int byte_pos = 0; byte_pos < n_bytes; ++byte_pos) {
			
			// traverse every bit in each byte
			for (int bit_pos = 0; bit_pos < 8; ++bit_pos) {
				
				// traverse every answer
				for (int answer_pos = 0; answer_pos < n_answers - 1; ++answer_pos) {
					
					// randomly select an answer to swap the corresponding bits
					int answer_rnd = answer_pos + rnd.nextInt(n_answers - answer_pos);
					
					// check if swap with the answer itself
					if (answer_pos == answer_rnd) {
						continue;
					}
					
					// check if the two corresponding answer bits are the same
					if ((answer_list.get(answer_pos)[byte_pos] & mask[bit_pos]) == (answer_list.get(answer_rnd)[byte_pos] & mask[bit_pos])) {
						continue;
					}
					
					// swap the two corresponding answer bits
					answer_list.get(answer_pos)[byte_pos] ^= mask[bit_pos];
					answer_list.get(answer_rnd)[byte_pos] ^= mask[bit_pos];
				}
			}
		}
		
		return rnd;
	}
	
	public static void shuffle_for_resizing (List<byte[]> answer_list, long seed, List<Integer> sizes) {
		Random rnd = shuffle (answer_list, seed);
		
		for (byte[] answer : answer_list) {
			int start = 0;
			int end = 0;
			
			for (int size : sizes) {
				end = start + size;
				
				if (size > 1) {
					for (int pos = start; pos < end - 1; ++pos) {
						int pos_rnd = pos + rnd.nextInt(end - pos);
						
						if (pos == pos_rnd) {
							continue;
						}
						
						int pos_byte = pos / 8;
						int pos_bit = pos % 8;
						int pos_rnd_byte = pos_rnd / 8;
						int pos_rnd_bit = pos_rnd % 8;
						
						boolean flag1 = ((answer[pos_byte] & mask[pos_bit]) == 0);
						boolean flag2 = ((answer[pos_rnd_byte] & mask[pos_rnd_bit]) == 0);
						
						if (flag1 == flag2) {
							continue;
						}
						
						answer[pos_byte] ^= mask[pos_bit];
						answer[pos_rnd_byte] ^= mask[pos_rnd_bit];
					}
				}
				
				start = end;
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		byte[] b1 = new byte[]{0, 2};
		byte[] b2 = new byte[]{3, 3};
		List<byte[]> b = new ArrayList<byte[]>();
		b.add(b1);
//		b.add(b2);
		
//		shuffle(b, (long)4);
		
		List<Integer> sizes = new ArrayList<Integer>();
		sizes.add(13);
		sizes.add(3);
		
		shuffle_for_resizing (b, (long)19, sizes);
		
		System.out.println(b1[0]);
		System.out.println(b2[0]);
		System.out.println(b1[1]);
		System.out.println(b2[1]);
		
		long l = Long.MAX_VALUE + 1;
		System.out.println(l);
		l = Long.MIN_VALUE - 1;
		System.out.println(l);
	}

}
