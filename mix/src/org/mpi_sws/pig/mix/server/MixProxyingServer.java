package org.mpi_sws.pig.mix.server;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;

import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.transport.TTransportException;
import org.mpi_sws.pig.common.entity.Server;
import org.mpi_sws.pig.common.message.SplitMessage;
import org.mpi_sws.pig.mix.common.MixCommon;
import org.mpi_sws.pig.common.service.MixProxyingService;
import org.mpi_sws.pig.common.service.MixProxyingService.Iface;
import org.mpi_sws.pig.common.utility.TJSONProtocolFixedB64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class MixProxyingServer implements Iface, Runnable {
	
	private static final Logger logger = LoggerFactory.getLogger(MixProxyingServer.class);
	
	public static ConcurrentLinkedQueue<SplitMessage> queue_ClientToAggregator_QueryRequests = new ConcurrentLinkedQueue<SplitMessage>();
	public static ConcurrentLinkedQueue<SplitMessage> queue_AggregatorToClient_QueryResponses = new ConcurrentLinkedQueue<SplitMessage>();
	public static ConcurrentLinkedQueue<SplitMessage> queue_ClientToAggregator_QueryAnswers = new ConcurrentLinkedQueue<SplitMessage>();
	public static ConcurrentLinkedQueue<SplitMessage> queue_ClientToMix_QueryAnswers = new ConcurrentLinkedQueue<SplitMessage>();
	
	public static ConcurrentMap<Long, String> map_SplitToClient = new ConcurrentHashMap<Long, String>();
	public static ConcurrentMap<String, ConcurrentLinkedQueue<SplitMessage>> map_ClientToPendingQueryResponses = new ConcurrentHashMap<String, ConcurrentLinkedQueue<SplitMessage>>();

	@Override
	public List<SplitMessage> forward_ClientToAggregator_QueryRequests_and_retrieve_AggregatorToClient_PendingQueryResponses(
			String clientId, List<SplitMessage> queryRequests)
			throws TException {
		logger.info("Received {} split query requests from client {} (to aggregator).", queryRequests.size(), clientId);
		for (SplitMessage split : queryRequests) {
			long splitId = split.getSplitId();
			if (map_SplitToClient.putIfAbsent(splitId, clientId) == null) {
				queue_ClientToAggregator_QueryRequests.offer(split);
			} else {
				logger.warn("Discarded the split query request {} from client {} (to aggregator) -- ID collision with previous split query request.", splitId, clientId);
			}
		}
		
		List<SplitMessage> queryResponses = new ArrayList<SplitMessage>();
		ConcurrentLinkedQueue<SplitMessage> queue = map_ClientToPendingQueryResponses.get(clientId);
		
		if (queue != null) {
			SplitMessage split = null;
			while ((split = queue.poll()) != null) {
				queryResponses.add(split);
			}
		}
		
		if (queryResponses.size() > 0) {
			logger.info("Forwarded {} split query responses (from aggregator) to client {}.", queryResponses.size(), clientId);
		}
		
		return queryResponses;
	}
	
	@Override
	public void forward_ClientToAggregator_QueryAnswers(
			List<SplitMessage> queryAnswers) throws TException {
		logger.info("Received {} split query answers from client (to aggregator).", queryAnswers.size());
		queue_ClientToAggregator_QueryAnswers.addAll(queryAnswers);
	}

	@Override
	public void forward_ClientToMix_QueryAnswers(List<SplitMessage> queryAnswers)
			throws TException {
		logger.info("Received {} split query answers from client (to the peer mix).", queryAnswers.size());
		queue_ClientToMix_QueryAnswers.addAll(queryAnswers);
	}

	@Override
	public void run() {
		logger.info("Starting the mix proxying server.");
		
		InetSocketAddress bindAddr = new InetSocketAddress(MixCommon.local_mix_host, MixCommon.local_mix_proxying_port);
		int clientTimeout = 0;
		TProtocolFactory protocol = new TJSONProtocolFixedB64.Factory();
		TProcessor processor = new MixProxyingService.Processor<Iface>(new MixProxyingServer());
		int nSelectorThreads = MixCommon.mix_proxying_server_selector_threads;
		int nWorkerThreads = MixCommon.mix_proxying_server_worker_threads;
		
		Server server = null;
		try {
			server = new Server(bindAddr, clientTimeout, protocol, processor, nSelectorThreads, nWorkerThreads);
			server.start();
		} catch (TTransportException e) {
			logger.error("Starting the mix proxying server.", e);
		} finally {
			if (server != null) {
				server.stop();
			}
		}
	}
	
	public static void main(String args[]) {
		(new Thread(new MixProxyingServer())).start();
	}
}
