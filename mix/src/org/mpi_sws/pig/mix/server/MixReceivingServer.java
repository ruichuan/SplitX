package org.mpi_sws.pig.mix.server;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListSet;

import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.transport.TTransportException;
import org.mpi_sws.pig.common.entity.Server;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.message.QueryAnswer;
import org.mpi_sws.pig.common.message.SidList;
import org.mpi_sws.pig.common.message.SidListResponse;
import org.mpi_sws.pig.common.message.SplitMessage;
import org.mpi_sws.pig.common.utility.MessageProcessing;
import org.mpi_sws.pig.common.utility.TJSONProtocolFixedB64;
import org.mpi_sws.pig.mix.common.MixCommon;
import org.mpi_sws.pig.mix.database.AnswerDBAccess;
import org.mpi_sws.pig.mix.logic.BucketResizingHandler;
import org.mpi_sws.pig.mix.logic.QueryAnswerHandler;
import org.mpi_sws.pig.mix.logic.SidListHandler;
import org.mpi_sws.pig.common.service.MixReceivingService;
import org.mpi_sws.pig.common.service.MixReceivingService.Iface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class MixReceivingServer implements Iface, Runnable {
	
	private static final Logger logger = LoggerFactory.getLogger(MixReceivingServer.class);
	public static ConcurrentMap<Long, byte[]> map_SplitAnswers = new ConcurrentHashMap<Long, byte[]>();
	
	public static long latestVersionId = -1;
	public static ConcurrentSkipListSet<String> set_SeenVersions = new ConcurrentSkipListSet<String>();
	
	public static ConcurrentMap<String, Query> map_ActiveQueries = new ConcurrentHashMap<String, Query>();
	public static ConcurrentMap<String, Query> map_InactiveQueries = new ConcurrentHashMap<String, Query>();
	
	public static ConcurrentMap<String, ConcurrentMap<Long, byte[]>> map_AnswerBatches = new ConcurrentHashMap<String, ConcurrentMap<Long, byte[]>>();
	public static ConcurrentMap<String, AnswerDBAccess> map_AnswerDbAccesses = new ConcurrentHashMap<String, AnswerDBAccess>();

	@Override
	public void forward_AggregatorToClient_QueryResponses(
			List<SplitMessage> queryResponses) throws TException {
		logger.info("Received {} split query responses from aggregator (to clients).", queryResponses.size());
		MixProxyingServer.queue_AggregatorToClient_QueryResponses.addAll(queryResponses);
	}
	
	@Override
	public void send_ClientToMix_QueryAnswers(List<SplitMessage> queryAnswers)
			throws TException {
		logger.info("Received {} split query answers via the peer mix.", queryAnswers.size());
		receive_QueryAnswers(queryAnswers);
	}

	@Override
	public void send_AggregatorToMix_QueryAnswers(
			List<SplitMessage> queryAnswers) throws TException {
		logger.info("Received {} split query answers via aggregator.", queryAnswers.size());
		receive_QueryAnswers(queryAnswers);
	}
	
	private void receive_QueryAnswers(List<SplitMessage> queryAnswers) {
		TDeserializer deserializer = new TDeserializer(new TJSONProtocolFixedB64.Factory());
		QueryAnswerHandler handler = new QueryAnswerHandler();
		
		for (SplitMessage split : queryAnswers) {
			long splitId = split.getSplitId();
			byte[] splitMessage = split.getMessage();
			
			if (map_SplitAnswers.putIfAbsent(splitId, splitMessage) == null) {
				continue;
			}
			
			byte[] exSplitMessage = map_SplitAnswers.remove(splitId);
			if (exSplitMessage != null && splitMessage.length == exSplitMessage.length) {
				try {
					byte[] message = MessageProcessing.join(splitMessage, exSplitMessage);
					QueryAnswer answer = new QueryAnswer();
					deserializer.deserialize(answer, message);
					handler.process(answer);
				} catch (TException e) {
					logger.error("Deserializing the query answer {}.", splitId, e);
				}
			} else {
				logger.warn("Two split query answers {} do not match.", splitId);
			}
		}
	}

	@Override
	public SidListResponse negotiate_MixToMix_SidList(SidList sidList)
			throws TException {
		logger.info("Received {} split IDs for analyst {}'s query {} (for negotiation).", sidList.getSplitIdsSize(), sidList.getAnalystId(), sidList.getQueryId());
		SidListHandler handler = new SidListHandler();
		SidListResponse sidListResponse = handler.process(sidList);
		logger.info("Returned {} split IDs for analyst {}'s query {} (for negotiation).", sidListResponse.getSplitIdsSize(), sidListResponse.getAnalystId(), sidListResponse.getQueryId());
		return sidListResponse;
	}

	@Override
	public List<ByteBuffer> resize_AggregatorToMix_Buckets(String analystId,
			long queryId, List<Integer> bucketSizes, long resizingTimes)
			throws TException {
		logger.info("Received the bucket resizing request for analyst {}'s query {}.", analystId, queryId);
		BucketResizingHandler handler = new BucketResizingHandler();
		List<ByteBuffer> ans_list = handler.process(analystId, queryId, bucketSizes, resizingTimes);
		if (ans_list == null) {
			ans_list = new ArrayList<ByteBuffer>();
		}
		logger.info("Returned the bucket resizing response for analyst {}'s query {}.", analystId, queryId);
		return ans_list;
	}

	@Override
	public void run() {
		logger.info("Starting the mix receiving server.");
		
		InetSocketAddress bindAddr = new InetSocketAddress(MixCommon.local_mix_host, MixCommon.local_mix_receiving_port);
		int clientTimeout = 0;
		TProtocolFactory protocol = new TCompactProtocol.Factory();
		TProcessor processor = new MixReceivingService.Processor<Iface>(new MixReceivingServer());
		int nSelectorThreads = MixCommon.mix_receiving_server_selector_threads;
		int nWorkerThreads = MixCommon.mix_receiving_server_worker_threads;
		
		Server server = null;
		try {
			server = new Server(bindAddr, clientTimeout, protocol, processor, nSelectorThreads, nWorkerThreads);
			server.start();
		} catch (TTransportException e) {
			logger.error("Starting the mix receiving server.", e);
		} finally {
			if (server != null) {
				server.stop();
			}
		}
	}
}
