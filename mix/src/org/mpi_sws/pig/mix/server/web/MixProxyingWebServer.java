package org.mpi_sws.pig.mix.server.web;

import org.apache.thrift.TProcessor;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.mpi_sws.pig.common.service.MixProxyingService;
import org.mpi_sws.pig.common.service.MixProxyingService.Iface;
import org.mpi_sws.pig.mix.common.MixCommon;
import org.mpi_sws.pig.mix.server.MixProxyingServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class MixProxyingWebServer implements Runnable {
	
	private static final Logger logger = LoggerFactory.getLogger(MixProxyingWebServer.class);
	private TProcessor processor = new MixProxyingService.Processor<Iface>(new MixProxyingServer());
	
	@Override
	public void run() {
		logger.info("Starting the mix proxying web server.");
		
		Server server = null;
		try {
			server = new Server();
			ServerConnector connector = new ServerConnector(server);
			connector.setHost(MixCommon.local_mix_host);
			connector.setPort(MixCommon.local_mix_proxying_port);
			connector.setIdleTimeout(MixCommon.client_connection_idle_timeout);
			server.setConnectors(new Connector[]{connector});
			server.setHandler(new MixProxyingWebHandler(processor));
			server.start();
			server.join();
		} catch (Exception e) {
			logger.error("Starting the mix proxying web server.", e);
		} finally {
			if (server != null) {
				try {
					server.stop();
				} catch (Exception e) {}
			}
		}
	}
}
