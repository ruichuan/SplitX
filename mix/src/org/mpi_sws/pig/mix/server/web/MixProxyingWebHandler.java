package org.mpi_sws.pig.mix.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TIOStreamTransport;
import org.apache.thrift.transport.TTransport;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.mpi_sws.pig.common.utility.TJSONProtocolFixedB64;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class MixProxyingWebHandler extends AbstractHandler {
	
	private TProcessor processor = null;
	
	public MixProxyingWebHandler (TProcessor processor) {
		this.processor = processor;
	}

	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		try {
			TTransport transport = new TIOStreamTransport(request.getInputStream(), response.getOutputStream());
			TProtocol protocol = new TJSONProtocolFixedB64(transport);
			processor.process(protocol, protocol);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} finally {
			baseRequest.setHandled(true);
		}
	}
}
