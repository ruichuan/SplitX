package org.mpi_sws.pig.mix.common;

import org.mpi_sws.pig.common.Common;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class MixCommon {
	
	public static int mix_proxying_server_selector_threads = 16;
	public static int mix_proxying_server_worker_threads = 512;
	public static int mix_receiving_server_selector_threads = 16;
	public static int mix_receiving_server_worker_threads = 256;
	
	public static long forwarding_interval = 1 * 60 * 1000;	// the mix as a proxy forwards messages every once a while
	public static int forwarding_threshold = 256;	// a forwarded message contains at most 256 split messages
	public static long QET_grace_time = 1 * 60 * 1000;	// query end grace time
	public static long max_time_error = 1 * 60 * 1000;	// the greatest error/difference between two mixes' clocks
	public static long client_connection_idle_timeout = 10 * 60 * 1000;	// the time that the client connection can be idle before it is closed
	public static long query_description_retrieval_interval = 1 * 60 * 1000;	// the interval for mix to retrieve query description from the aggregator
	public static int answer_batch_to_db_threshold = 10;	// # answers (for one query) that can be batched in the memory before they are written into the database
	
	// Below should never be modified.
	
	public static String aggregator_host = Common.aggregator_host;
	public static int aggregator_receiving_port = Common.aggregator_receiving_port;
	
	public static String local_mix_host = null;
	public static int local_mix_receiving_port = -1;
	public static int local_mix_proxying_port = -1;
	
	public static String peer_mix_host = null;
	public static int peer_mix_receiving_port = -1;
	
	public static boolean is_even_query_master;	// false --> master mix for query ID 1, 3, 5, ...; true for 0, 2, 4, ...
	
	public static String jdbc_driver = "org.sqlite.JDBC";	// use sqlite database to store client answers
	public static String db_name_prefix = "jdbc:sqlite:";
}
