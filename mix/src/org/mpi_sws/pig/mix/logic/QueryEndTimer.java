package org.mpi_sws.pig.mix.logic;

import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentMap;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.message.SidList;
import org.mpi_sws.pig.common.message.SidListResponse;
import org.mpi_sws.pig.common.utility.SeedGeneration;
import org.mpi_sws.pig.mix.common.MixCommon;
import org.mpi_sws.pig.mix.database.AnswerDBAccess;
import org.mpi_sws.pig.mix.database.Answers;
import org.mpi_sws.pig.mix.server.MixReceivingServer;
import org.mpi_sws.pig.common.service.MixReceivingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class QueryEndTimer {
	
	private static final Logger logger = LoggerFactory.getLogger(QueryEndTimer.class);
	
	private Timer timer = new Timer();
	private String analystId = null;
	private Query query = null;
	
	public QueryEndTimer (String analystId, Query query) {
		this.analystId = analystId;
		this.query = query;
	}
	
	public void schedule (long delay) {
		timer.schedule(new QueryEndTask(), delay);
	}
	
	private class QueryEndTask extends TimerTask {

		@Override
		public void run() {
			long queryId = query.getQueryId();
			logger.info("Time's up for analyst {}'s query {}.", analystId, queryId);
			String id = analystId + "_" + queryId;
			
			MixReceivingServer.map_ActiveQueries.remove(id);
			ConcurrentMap<Long, byte[]> answer_batch = MixReceivingServer.map_AnswerBatches.remove(id);
			
			try {
				int size = answer_batch.size();
				MixReceivingServer.map_AnswerDbAccesses.get(id).insertAnswers(answer_batch);
				logger.info("Wrote approximately {} query answers for analyst {}'s query {} into the database.", size, analystId, queryId);
			} catch (SQLException e) {
				logger.error("Writing query answers for analyst {}'s query {} into the database.", analystId, queryId, e);
			}
			
			if (MixCommon.is_even_query_master == (queryId % 2 == 0)) {
				try {
					Thread.sleep(MixCommon.max_time_error);
				} catch (InterruptedException e) {
					logger.error("Sleeping {}ms to make sure the slave mix is ready for the split ID negotiation for analyst {}'s query {}.", MixCommon.max_time_error, analystId, queryId, e);
				}
				
				Answers answers = null;
				AnswerDBAccess dba = MixReceivingServer.map_AnswerDbAccesses.remove(id);
				
				try {
					answers = dba.retrieveAll();
					logger.info("Retrieved all {} answers for analyst {}'s query {} from the database.", answers.size(), analystId, queryId);
				} catch (SQLException e) {
					logger.error("Retrieving all answers for analyst {}'s query {} from the database.", analystId, queryId, e);
					answers = new Answers();
				} finally {
					try {
						dba.close();
					} catch (SQLException e) {
						logger.error("Closing the database for analyst {}'s query {}.", analystId, queryId, e);
					}
				}
				
				SidList request = new SidList(analystId, queryId, answers.get_sid_list(), SeedGeneration.generate());
				
				TTransport transport = new TFramedTransport(new TSocket(MixCommon.peer_mix_host, MixCommon.peer_mix_receiving_port), Common.max_frame_size);
				TProtocol protocol = new TCompactProtocol(transport);
				MixReceivingService.Client client = new MixReceivingService.Client(protocol);
				
				try {
					transport.open();
					SidListResponse response = client.negotiate_MixToMix_SidList(request);
					logger.info("Negotiated split IDs with the peer mix for analyst {}'s query {}.", analystId, queryId);
					SidListResponseHandler handler = new SidListResponseHandler();
					handler.process(request, response, query, answers);
				} catch (TTransportException e) {
					logger.error("Connecting to the peer mix.", e);
				} catch (TException e) {
					logger.error("Negotiating split IDs with the peer mix for analyst {}'s query {}.", analystId, queryId, e);
				} finally {
					if (transport != null) {
						transport.close();
					}
				}
			} else {
				MixReceivingServer.map_InactiveQueries.putIfAbsent(id, query);
			}
		}
	}
}
