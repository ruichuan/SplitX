package org.mpi_sws.pig.mix.logic;

import java.sql.SQLException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.service.AggregatorReceivingService;
import org.mpi_sws.pig.common.message.Buckets;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.message.QueryFile;
import org.mpi_sws.pig.mix.common.MixCommon;
import org.mpi_sws.pig.mix.database.AnswerDBAccess;
import org.mpi_sws.pig.mix.server.MixReceivingServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class QueryDescriptionRetrievalTimer {
	
	private static final Logger logger = LoggerFactory.getLogger(QueryDescriptionRetrievalTimer.class);
	
	private Timer timer = new Timer();
	
	public void schedule () {
		timer.schedule(new QueryDescriptionRetrievalTask(), 0, MixCommon.query_description_retrieval_interval);
		logger.info("Scheduled the query description retrieval timer.");
	}
	
	private class QueryDescriptionRetrievalTask extends TimerTask {

		@Override
		public void run() {
			TTransport transport = new TFramedTransport(new TSocket(MixCommon.aggregator_host, MixCommon.aggregator_receiving_port), Common.max_frame_size);
			TProtocol protocol = new TCompactProtocol(transport);
			AggregatorReceivingService.Client client = new AggregatorReceivingService.Client(protocol);
			
			try {
				transport.open();
				List<QueryFile> query_files = client.retrieve_MixToAggregator_QueryDescription(MixReceivingServer.latestVersionId);
				logger.info("Received {} query files from aggregator.", query_files.size());
				process(query_files);
			} catch (TTransportException e) {
				logger.error("Connecting to aggregator.", e);
			} catch (TException e) {
				logger.error("Retrieving the latest query description from aggregator (current version: {}).", MixReceivingServer.latestVersionId, e);
			} finally {
				if (transport != null) {
					transport.close();
				}
			}
		}
		
		private void process(List<QueryFile> query_files) {
			for (QueryFile query_file : query_files) {
				String analystId = query_file.getAnalystId();
				long versionId = query_file.getVersionId();
				List<Query> queries = query_file.getQueries();
				
				String version = analystId + "_" + versionId;
				
				if (MixReceivingServer.set_SeenVersions.contains(version)) {
					logger.warn("Analyst {}'s queries in version {} have been received before.", analystId, versionId);
					continue;
				}
				
				for (Query query : queries) {
					long queryId = query.getQueryId();
					long queryEndTime = query.getQueryEndTime();
					double epsilon = query.getEpsilon();
					String sql = query.getSql();
					Buckets buckets = query.getBuckets();
					
					String id = analystId + "_" + queryId;
					
					if (MixReceivingServer.map_ActiveQueries.containsKey(id) || MixReceivingServer.map_InactiveQueries.containsKey(id)) {
						logger.warn("Analyst {}'s query {} is being processed.", analystId, queryId);
						continue;
					}
					
					if (queryEndTime <= System.currentTimeMillis()) {
						logger.warn("Analyst {}'s query {} is outdated.", analystId, queryId);
						continue;
					}
					
					if (epsilon <= 0) {
						logger.warn("Analyst {}'s query {} has a non-positive epsilon.", analystId, queryId);
						continue;
					}
					
					if (sql.trim().isEmpty()) {
						logger.warn("Analyst {}'s query {} does not contain a SQL query.", analystId, queryId);
						continue;
					}
					
					if (buckets.getRangesSize() <= 0) {
						logger.warn("Analyst {}'s query {} does not specify any buckets.", analystId, queryId);
						continue;
					}
					
					AnswerDBAccess dba = new AnswerDBAccess ();
					try {
						dba.open(id, true);
						MixReceivingServer.map_AnswerDbAccesses.putIfAbsent(id, dba);
						ConcurrentMap<Long, byte[]> answer_batch = new ConcurrentHashMap<Long, byte[]>();
						MixReceivingServer.map_AnswerBatches.putIfAbsent(id, answer_batch);
						MixReceivingServer.map_ActiveQueries.putIfAbsent(id, query);
						
						QueryEndTimer query_end_timer = new QueryEndTimer(analystId, query);
						long delay = queryEndTime + MixCommon.QET_grace_time - System.currentTimeMillis();
						query_end_timer.schedule(delay);
						logger.info("Accepted analyst {}'s new query {}.", analystId, queryId);
					} catch (ClassNotFoundException e) {
						logger.error("Opening the database for analyst {}'s query {}.", analystId, queryId, e);
						if (dba != null) {
							try {
								dba.close();
							} catch (SQLException e1) {}
						}
					} catch (SQLException e) {
						logger.error("Opening the database for analyst {}'s query {}.", analystId, queryId, e);
						if (dba != null) {
							try {
								dba.close();
							} catch (SQLException e1) {}
						}
					}
				}
				
				MixReceivingServer.set_SeenVersions.add(version);
				if (MixReceivingServer.latestVersionId < versionId) {
					MixReceivingServer.latestVersionId = versionId;
				}
			}
		}
	}
}
