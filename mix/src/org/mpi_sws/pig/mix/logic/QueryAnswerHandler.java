package org.mpi_sws.pig.mix.logic;

import java.sql.SQLException;
import java.util.concurrent.ConcurrentMap;

import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.message.QueryAnswer;
import org.mpi_sws.pig.mix.common.MixCommon;
import org.mpi_sws.pig.mix.server.MixReceivingServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class QueryAnswerHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(QueryAnswerHandler.class);
	
	public void process (QueryAnswer answer) {
		String analystId = answer.getAnalystId();
		long queryId = answer.getQueryId();
		long splitId = answer.getSplitId();
		byte[] answerBits = answer.getAnswerBits();
		
		String id = analystId + "_" + queryId;
		
		Query query = MixReceivingServer.map_ActiveQueries.get(id);
		if (query == null) {
			logger.warn("The query answer {} associates with analyst {}'s outdated query {}.", splitId, analystId, queryId);
			return;
		}
		
		if (answerBits.length != Common.special_bytes_in_answer + (query.getBuckets().getRangesSize() + 7) / 8) {
			logger.warn("The query answer {} associated with analyst {}'s query {} has an illegitimate length of {}.", splitId, analystId, queryId, answerBits.length);
			return;
		}
		
		ConcurrentMap<Long, byte[]> answerBatch = MixReceivingServer.map_AnswerBatches.get(id);
		if (answerBatch == null || answerBatch.putIfAbsent(splitId, answerBits) != null) {
			logger.warn("Discarded the query answer {} to analyst {}'s query {} -- ID collision with previous query answer.", splitId, analystId, queryId);
			return;
		}
		
		if (answerBatch.size() >= MixCommon.answer_batch_to_db_threshold) {
			try {
				int size = answerBatch.size();
				MixReceivingServer.map_AnswerDbAccesses.get(id).insertAnswers(answerBatch);
				logger.info("Wrote approximately {} query answers for analyst {}'s query {} into the database.", size, analystId, queryId);
			} catch (SQLException e) {
				logger.error("Writing query answers for analyst {}'s query {} into the database.", analystId, queryId, e);
			}
		}
	}
}
