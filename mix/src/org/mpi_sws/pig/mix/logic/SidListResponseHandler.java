package org.mpi_sws.pig.mix.logic;

import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.service.AggregatorReceivingService;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.message.QueryNoisyAnswers;
import org.mpi_sws.pig.common.message.SidList;
import org.mpi_sws.pig.common.message.SidListResponse;
import org.mpi_sws.pig.mix.common.MixCommon;
import org.mpi_sws.pig.mix.database.Answers;
import org.mpi_sws.pig.mix.database.CommonAnswerDBAccess;
import org.mpi_sws.pig.mix.database.ParameterDBAccess;
import org.mpi_sws.pig.mix.utility.AnswerShuffling;
import org.mpi_sws.pig.mix.utility.NoiseGeneration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class SidListResponseHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(SidListResponseHandler.class);
	
	public void process (SidList request, SidListResponse response, Query query, Answers answers) {
		String analystId = response.getAnalystId();
		long queryId = response.getQueryId();
		List<Long> splitIds = response.getSplitIds();
		
		if (! analystId.equalsIgnoreCase(request.getAnalystId()) || queryId != request.getQueryId()) {
			logger.warn("The identities of the split ID negotiation request (analyst {}'s query {}) and its response (analyst {}'s query {}) do not match.", request.getAnalystId(), request.getQueryId(), analystId, queryId);
			return;
		}
		
		if (! remove_uncommon_answers(answers, splitIds)) {
			logger.warn("The mix, as a master, cannot successfully remove uncommon answers for analyst {}'s query {}.", analystId, queryId);
			return;
		}
		
		String id = analystId + "_" + queryId;
		List<byte[]> answer_list = answers.get_answer_list();
		
		CommonAnswerDBAccess dba_common = new CommonAnswerDBAccess();
		try {
			dba_common.open(id, true);
			dba_common.insertAnswers(answer_list);
			logger.info("Wrote common answers for analyst {}'s query {} into the database.", analystId, queryId);
		} catch (Exception e) {
			logger.error("Writing common answers for analyst {}'s query {} into the database.", analystId, queryId, e);
		} finally {
			try {
				dba_common.close();
			} catch (SQLException e) {}
		}
		
		int noise = NoiseGeneration.generate(answer_list, query);
		AnswerShuffling.shuffle(answer_list, request.getSeed());
		
		ParameterDBAccess dba_para = new ParameterDBAccess();
		try {
			dba_para.open(id, true);
			dba_para.setParameter("Bucket_Count", Common.special_bytes_in_answer * 8 + query.getBuckets().getRangesSize());
			dba_para.setParameter("Noise_Count", noise);
			dba_para.setParameter("Shuffling_Seed", request.getSeed());
			dba_para.setParameter("Resizing_Times", 0);
			logger.info("Wrote parameters for analyst {}'s query {} into the database.", analystId, queryId);
		} catch (Exception e) {
			logger.error("Writing parameters for analyst {}'s query {} into the database.", analystId, queryId, e);
		} finally {
			try {
				dba_para.close();
			} catch (SQLException e) {}
		}
		
		List<ByteBuffer> ans_list = new ArrayList<ByteBuffer>(answer_list.size());
		for (byte[] answer : answer_list) {
			ans_list.add(ByteBuffer.wrap(answer));
		}
		QueryNoisyAnswers noisyAnswers = new QueryNoisyAnswers (analystId, queryId, ans_list, noise);
		
		TTransport transport = new TFramedTransport(new TSocket(MixCommon.aggregator_host, MixCommon.aggregator_receiving_port), Common.max_frame_size);
		TProtocol protocol = new TCompactProtocol(transport);
		AggregatorReceivingService.Client client = new AggregatorReceivingService.Client(protocol);
		
		try {
			transport.open();
			client.send_MixToAggregator_QueryNoisyAnswers(noisyAnswers);
			logger.info("Sent {} noisy answers for analyst {}'s query {} to aggregator.", ans_list.size(), analystId, queryId);
		} catch (TTransportException e) {
			logger.error("Connecting to aggregator.", e);
		} catch (TException e) {
			logger.error("Sending noisy answers for analyst {}'s query {} to aggregator.", analystId, queryId, e);
		} finally {
			if (transport != null) {
				transport.close();
			}
		}
	}
	
	// remove those answers whose SIDs are included in sid_list
	private boolean remove_uncommon_answers (Answers answers, List<Long> sid_list) {
		
		// # answers must >= the length of sid_list
		if (answers.size() < sid_list.size()) {
			return false;
		}
		
		// no answers
		if (answers.size() <= 0) {
			return true;
		}
		
		int pos = 0;
		
		for (long sid : sid_list) {
			pos = binary_search (answers.get_sid_list(), pos, answers.size() - 1, sid);
			
			// if not found
			if (pos < 0) {
				return false;
			}
			
			answers.remove_answer(pos);
		}
		
		return true;
	}
	
	// find key's position in sid_list[min, max].  The reason of using binary search is that the number of uncommon SIDs is usually small.
	private int binary_search (List<Long> sid_list, int min, int max, long key) {
		while (min <= max) {
			int mid = (min + max) / 2;
			long sid = sid_list.get(mid);
			
			if (sid < key) {
				min = mid + 1;
			} else if (sid > key) {
				max = mid - 1;
			} else {
				return mid;
			}
		}
		
		return -1;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
