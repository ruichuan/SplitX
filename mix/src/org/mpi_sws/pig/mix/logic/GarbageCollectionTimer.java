package org.mpi_sws.pig.mix.logic;

import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.utility.GarbageCollection;
import org.mpi_sws.pig.mix.server.MixProxyingServer;
import org.mpi_sws.pig.mix.server.MixReceivingServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class GarbageCollectionTimer {
	
	private static final Logger logger = LoggerFactory.getLogger(GarbageCollectionTimer.class);
	
	private Timer timer = new Timer();
	
	private Set<Long> set_SplitToClient = new HashSet<Long>();
	private Set<Long> set_SplitAnswers = new HashSet<Long>();
	private Set<String> set_ClientToPendingQueryResponses = new HashSet<String>();
	
	private GarbageCollection<Long, String> gc_SplitToClient = new GarbageCollection<Long, String>();
	private GarbageCollection<Long, byte[]> gc_SplitAnswers = new GarbageCollection<Long, byte[]>();
	private SpecialGarbageCollection gc_Special = new SpecialGarbageCollection();
	
	public void schedule () {
		timer.schedule(new GarbageCollectionTask(), 0, Common.garbage_collection_interval);
		logger.info("Scheduled the garbage collection timer.");
	}
	
	private class GarbageCollectionTask extends TimerTask {

		@Override
		public void run() {
			logger.info("Garbage collection started.");
			set_SplitToClient = gc_SplitToClient.collect(MixProxyingServer.map_SplitToClient, set_SplitToClient);
			set_SplitAnswers = gc_SplitAnswers.collect(MixReceivingServer.map_SplitAnswers, set_SplitAnswers);
			set_ClientToPendingQueryResponses = gc_Special.collect_ClientToPendingQueryResponses(set_ClientToPendingQueryResponses);
			logger.info("Garbage collection finished.");
		}
	}
}
