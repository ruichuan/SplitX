package org.mpi_sws.pig.mix.logic;

import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.service.AggregatorReceivingService;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.message.QueryNoisyAnswers;
import org.mpi_sws.pig.common.message.SidList;
import org.mpi_sws.pig.common.message.SidListResponse;
import org.mpi_sws.pig.mix.common.MixCommon;
import org.mpi_sws.pig.mix.database.AnswerDBAccess;
import org.mpi_sws.pig.mix.database.Answers;
import org.mpi_sws.pig.mix.database.CommonAnswerDBAccess;
import org.mpi_sws.pig.mix.database.ParameterDBAccess;
import org.mpi_sws.pig.mix.server.MixReceivingServer;
import org.mpi_sws.pig.mix.utility.AnswerShuffling;
import org.mpi_sws.pig.mix.utility.NoiseGeneration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class SidListHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(SidListHandler.class);
	
	public SidListResponse process (SidList request) {
		
		String analystId = request.getAnalystId();
		long queryId = request.getQueryId();
		List<Long> splitIds = request.getSplitIds();
		long seed = request.getSeed();
		
		logger.info("Received the split ID negotiation request for analyst {}'s query {} from the peer mix.", analystId, queryId);
		
		String id = analystId + "_" + queryId;
		Query query = MixReceivingServer.map_InactiveQueries.get(id);
		
		if (query == null) {
			logger.warn("Analyst {}'s query {} is not in the split ID negotiation stage, or this mix is not the slave mix for this query.", analystId, queryId);
			SidListResponse response = new SidListResponse (analystId, queryId, splitIds);
			return response;
		}
		
		Answers answers = null;
		AnswerDBAccess dba = MixReceivingServer.map_AnswerDbAccesses.remove(id);
		
		try {
			answers = dba.retrieveAll();
			logger.info("Retrieved all {} answers for analyst {}'s query {} from the database.", answers.size(), analystId, queryId);
		} catch (SQLException e) {
			logger.error("Retrieving all query answers for analyst {}'s query {} from the database.", analystId, queryId, e);
			answers = new Answers();
		} finally {
			try {
				dba.close();
			} catch (SQLException e) {
				logger.error("Closing the database for analyst {}'s query {}.", analystId, queryId, e);
			}
		}
		
		MixReceivingServer.map_InactiveQueries.remove(id);
		List<Long> uncommon = remove_uncommon_answers (answers, splitIds);
		List<byte[]> answer_list = answers.get_answer_list();
		
		CommonAnswerDBAccess dba_common = new CommonAnswerDBAccess();
		try {
			dba_common.open(id, true);
			dba_common.insertAnswers(answer_list);
			logger.info("Wrote common answers for analyst {}'s query {} into the database.", analystId, queryId);
		} catch (Exception e) {
			logger.error("Writing common answers for analyst {}'s query {} into the database.", analystId, queryId, e);
		} finally {
			try {
				dba_common.close();
			} catch (SQLException e) {}
		}
		
		int noise = NoiseGeneration.generate(answer_list, query);
		AnswerShuffling.shuffle(answer_list, seed);
		
		ParameterDBAccess dba_para = new ParameterDBAccess();
		try {
			dba_para.open(id, true);
			dba_para.setParameter("Bucket_Count", Common.special_bytes_in_answer * 8 + query.getBuckets().getRangesSize());
			dba_para.setParameter("Noise_Count", noise);
			dba_para.setParameter("Shuffling_Seed", seed);
			dba_para.setParameter("Resizing_Times", 0);
			logger.info("Wrote parameters for analyst {}'s query {} into the database.", analystId, queryId);
		} catch (Exception e) {
			logger.error("Writing parameters for analyst {}'s query {} into the database.", analystId, queryId, e);
		} finally {
			try {
				dba_para.close();
			} catch (SQLException e) {}
		}
		
		List<ByteBuffer> ans_list = new ArrayList<ByteBuffer>(answer_list.size());
		for (byte[] answer : answer_list) {
			ans_list.add(ByteBuffer.wrap(answer));
		}
		QueryNoisyAnswers noisyAnswers = new QueryNoisyAnswers (analystId, queryId, ans_list, noise);
		
		TTransport transport = new TFramedTransport(new TSocket(MixCommon.aggregator_host, MixCommon.aggregator_receiving_port), Common.max_frame_size);
		TProtocol protocol = new TCompactProtocol(transport);
		AggregatorReceivingService.Client client = new AggregatorReceivingService.Client(protocol);
		
		try {
			transport.open();
			client.send_MixToAggregator_QueryNoisyAnswers(noisyAnswers);
			logger.info("Sent {} noisy answers for analyst {}'s query {} to aggregator.", ans_list.size(), analystId, queryId);
		} catch (TTransportException e) {
			logger.error("Connecting to aggregator.", e);
		} catch (TException e) {
			logger.error("Sending noisy answers for analyst {}'s query {} to aggregator.", analystId, queryId, e);
		} finally {
			if (transport != null) {
				transport.close();
			}
		}
		
		SidListResponse response = new SidListResponse (analystId, queryId, uncommon);
		logger.info("Sent the split ID negotiation response for analyst {}'s query {} to the peer mix.", analystId, queryId);
		return response;
	}
	
	private List<Long> remove_uncommon_answers (Answers answers, List<Long> sid_list) {
		if (answers.size() <= 0) {
			return sid_list;
		}
		
		if (sid_list.size() <= 0) {
			answers.clear();
			return sid_list;
		}
		
		List<Long> uncommon = new ArrayList<Long>();
		
		int a = 0;
		int s = 0;
		
		while (a < answers.size() && s < sid_list.size()) {
			long sid_a = answers.get_sid_list().get(a);
			long sid_s = sid_list.get(s);
			
			if (sid_a == sid_s) {
				++a;
				++s;
			} else if (sid_a < sid_s) {
				answers.remove_answer(a);
			} else {
				uncommon.add(sid_s);
				++s;
			}
		}
		
		while (a < answers.size()) {
			answers.remove_answer(a);
		}
		
		while (s < sid_list.size()) {
			uncommon.add(sid_list.get(s));
			++s;
		}
		
		return uncommon;
	}
}
