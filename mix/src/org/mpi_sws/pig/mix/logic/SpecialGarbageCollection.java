package org.mpi_sws.pig.mix.logic;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.mpi_sws.pig.common.message.SplitMessage;
import org.mpi_sws.pig.mix.server.MixProxyingServer;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class SpecialGarbageCollection {
	
	public Set<String> collect_ClientToPendingQueryResponses (Set<String> set) {
		Set<String> newSet = new HashSet<String>();
		
		for (Iterator<Entry<String, ConcurrentLinkedQueue<SplitMessage>>> it_client = MixProxyingServer.map_ClientToPendingQueryResponses.entrySet().iterator(); it_client.hasNext(); ) {
			Entry<String, ConcurrentLinkedQueue<SplitMessage>> entry = it_client.next();
			String clientId = entry.getKey();
			ConcurrentLinkedQueue<SplitMessage> queryResponses = entry.getValue();
			
			for (Iterator<SplitMessage> it_response = queryResponses.iterator(); it_response.hasNext(); ) {
				long splitId = it_response.next().getSplitId();
				String id = clientId + "_" + splitId;
				
				if (set.contains(id)) {
					it_response.remove();
				} else {
					newSet.add(id);
				}
			}
		}
		
		return newSet;
	}
}
