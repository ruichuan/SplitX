package org.mpi_sws.pig.mix.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.thrift.TException;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.service.AggregatorReceivingService;
import org.mpi_sws.pig.common.message.SplitMessage;
import org.mpi_sws.pig.mix.common.MixCommon;
import org.mpi_sws.pig.mix.server.MixProxyingServer;
import org.mpi_sws.pig.common.service.MixReceivingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class MixProxyingTimer {
	
	private static final Logger logger = LoggerFactory.getLogger(MixProxyingTimer.class);
	
	private Timer timer_ClientToAggregator_QueryRequests = new Timer();
	private Timer timer_AggregatorToClient_QueryResponses = new Timer();
	private Timer timer_ClientToAggregator_QueryAnswers = new Timer();
	private Timer timer_ClientToMix_QueryAnswers = new Timer();
	
	public void schedule() {
		timer_ClientToAggregator_QueryRequests.schedule(
				new MixProxyingTask(MixProxyingTask.ClientToAggregator_QueryRequests),
				0,
				MixCommon.forwarding_interval);
		timer_AggregatorToClient_QueryResponses.schedule(
				new MixProxyingTask(MixProxyingTask.AggregatorToClient_QueryResponses),
				MixCommon.forwarding_interval / 4,
				MixCommon.forwarding_interval);
		timer_ClientToAggregator_QueryAnswers.schedule(
				new MixProxyingTask(MixProxyingTask.ClientToAggregator_QueryAnswers),
				MixCommon.forwarding_interval / 2,
				MixCommon.forwarding_interval);
		timer_ClientToMix_QueryAnswers.schedule(
				new MixProxyingTask(MixProxyingTask.ClientToMix_QueryAnswers),
				MixCommon.forwarding_interval * 3 / 4,
				MixCommon.forwarding_interval);
		logger.info("Scheduled the mix proxying timer.");
	}
	
	private class MixProxyingTask extends TimerTask {
		
		private static final byte ClientToAggregator_QueryRequests = 0;
		private static final byte AggregatorToClient_QueryResponses = 1;
		private static final byte ClientToAggregator_QueryAnswers = 2;
		private static final byte ClientToMix_QueryAnswers = 3;
		
		private byte type = -1;
		
		private MixProxyingTask (byte type) {
			this.type = type;
		}

		@Override
		public void run() {
			
			if (type == AggregatorToClient_QueryResponses) {
				SplitMessage split = null;
				while ((split = MixProxyingServer.queue_AggregatorToClient_QueryResponses.poll()) != null) {
					long splitId = split.getSplitId();
					String clientId = MixProxyingServer.map_SplitToClient.remove(splitId);
					if (clientId == null) {
						logger.warn("Received the split query response {} from aggregator (to client), but no matching.", splitId);
						continue;
					}
					
					ConcurrentLinkedQueue<SplitMessage> queue = MixProxyingServer.map_ClientToPendingQueryResponses.get(clientId);
					if (queue == null) {
						queue = new ConcurrentLinkedQueue<SplitMessage>();
						ConcurrentLinkedQueue<SplitMessage> exQueue = MixProxyingServer.map_ClientToPendingQueryResponses.putIfAbsent(clientId, queue);
						if (exQueue != null) {
							queue = exQueue;
						}
					}
					queue.offer(split);
				}
				
				return;
			}
			
			TTransport transport = null;
			TProtocol protocol = null;
			TServiceClient client = null;
			
			switch (type) {
			case ClientToAggregator_QueryRequests:
				transport = new TFramedTransport(new TSocket(MixCommon.aggregator_host, MixCommon.aggregator_receiving_port), Common.max_frame_size);
				protocol = new TCompactProtocol(transport);
				client = new AggregatorReceivingService.Client(protocol);
				break;
			case ClientToAggregator_QueryAnswers:
				transport = new TFramedTransport(new TSocket(MixCommon.aggregator_host, MixCommon.aggregator_receiving_port), Common.max_frame_size);
				protocol = new TCompactProtocol(transport);
				client = new AggregatorReceivingService.Client(protocol);
				break;
			case ClientToMix_QueryAnswers:
				transport = new TFramedTransport(new TSocket(MixCommon.peer_mix_host, MixCommon.peer_mix_receiving_port), Common.max_frame_size);
				protocol = new TCompactProtocol(transport);
				client = new MixReceivingService.Client(protocol);
				break;
			default:
				return;
			}
			
			try {
				transport.open();
				ConcurrentLinkedQueue<SplitMessage> queue = null;
				
				switch (type) {
				case ClientToAggregator_QueryRequests:
					queue = MixProxyingServer.queue_ClientToAggregator_QueryRequests;
					break;
				case ClientToAggregator_QueryAnswers:
					queue = MixProxyingServer.queue_ClientToAggregator_QueryAnswers;
					break;
				case ClientToMix_QueryAnswers:
					queue = MixProxyingServer.queue_ClientToMix_QueryAnswers;
					break;
				}
				
				List<SplitMessage> list = new ArrayList<SplitMessage>(MixCommon.forwarding_threshold);
				SplitMessage split = null;
				
				while ((split = queue.poll()) != null) {
					list.add(split);
					
					if (list.size() >= MixCommon.forwarding_threshold) {
						switch (type) {
						case ClientToAggregator_QueryRequests:
							((AggregatorReceivingService.Client)client).send_ClientToAggregator_QueryRequests(list);
							logger.info("Forwarded {} split query requests (from clients) to aggregator.", list.size());
							break;
						case ClientToAggregator_QueryAnswers:
							((AggregatorReceivingService.Client)client).send_ClientToAggregator_QueryAnswers(list);
							logger.info("Forwarded {} split query answers (from clients) to aggregator.", list.size());
							break;
						case ClientToMix_QueryAnswers:
							((MixReceivingService.Client)client).send_ClientToMix_QueryAnswers(list);
							logger.info("Forwarded {} split query answers (from clients) to the peer mix.", list.size());
							break;
						}

						list.clear();
					}
				}
				
				if (list.size() > 0) {
					switch (type) {
					case ClientToAggregator_QueryRequests:
						((AggregatorReceivingService.Client)client).send_ClientToAggregator_QueryRequests(list);
						logger.info("Forwarded {} split query requests (from clients) to aggregator.", list.size());
						break;
					case ClientToAggregator_QueryAnswers:
						((AggregatorReceivingService.Client)client).send_ClientToAggregator_QueryAnswers(list);
						logger.info("Forwarded {} split query answers (from clients) to aggregator.", list.size());
						break;
					case ClientToMix_QueryAnswers:
						((MixReceivingService.Client)client).send_ClientToMix_QueryAnswers(list);
						logger.info("Forwarded {} split query answers (from clients) to the peer mix.", list.size());
						break;
					}
				}
			} catch (TTransportException e) {
				switch (type) {
				case ClientToAggregator_QueryRequests:
					logger.error("Connecting to aggregator.", e);
					break;
				case ClientToAggregator_QueryAnswers:
					logger.error("Connecting to aggregator.", e);
					break;
				case ClientToMix_QueryAnswers:
					logger.error("Connecting to the peer mix.", e);
					break;
				}
			} catch (TException e) {
				switch (type) {
				case ClientToAggregator_QueryRequests:
					logger.error("Forwarding split query requests (from clients) to aggregator.", e);
					break;
				case ClientToAggregator_QueryAnswers:
					logger.error("Forwarding split query answers (from clients) to aggregator.", e);
					break;
				case ClientToMix_QueryAnswers:
					logger.error("Forwarding split query answers (from clients) to the peer mix.", e);
					break;
				}
			} finally {
				if (transport != null) {
					transport.close();
				}
			}
		}
	}
}
