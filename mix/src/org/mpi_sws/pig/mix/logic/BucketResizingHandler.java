package org.mpi_sws.pig.mix.logic;

import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mpi_sws.pig.mix.database.CommonAnswerDBAccess;
import org.mpi_sws.pig.mix.database.ParameterDBAccess;
import org.mpi_sws.pig.mix.utility.AnswerShuffling;
import org.mpi_sws.pig.mix.utility.NoiseGeneration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class BucketResizingHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(BucketResizingHandler.class);
	
	public List<ByteBuffer> process (String analystId, long queryId, List<Integer> bucketSizes, long resizingTimes) {
		String id = analystId + "_" + queryId;
		int bucket_count = -1;
		int noise_count = -1;
		long shuffling_seed = -1;
		long resizing_times = -1;
		
		ParameterDBAccess dba_para = new ParameterDBAccess();
		try {
			dba_para.open(id, false);
			bucket_count = (int) dba_para.getParameter("Bucket_Count");
			noise_count = (int) dba_para.getParameter("Noise_Count");
			shuffling_seed = dba_para.getParameter("Shuffling_Seed");
			resizing_times = dba_para.getParameter("Resizing_Times");
			logger.info("Read parameters for analyst {}'s query {} from the database.", analystId, queryId);
			
			if (resizingTimes > resizing_times) {
				dba_para.setParameter("Resizing_Times", resizingTimes);
			} else {
				logger.warn("The buckets for analyst {}'s query {} have been resized {} times.", analystId, queryId, resizing_times);
				return null;
			}
		} catch (Exception e) {
			logger.error("Reading parameters for analyst {}'s query {} from the database.", analystId, queryId, e);
			return null;
		} finally {
			try {
				dba_para.close();
			} catch (SQLException e) {}
		}
		
		int count = 0;
		for (int size : bucketSizes) {
			if (size < 1) {
				logger.warn("Resized buckets for analyst {}'s query {} have non-positive sizes.", analystId, queryId);
				return null;
			}
			count += size;
		}
		
		if (bucket_count != count) {
			logger.warn("Resized buckets' total size for analyst {}'s query {} does not match the original buckets' total size.", analystId, queryId);
			return null;
		}
		
		List<byte[]> answer_list = null;
		
		CommonAnswerDBAccess dba_common = new CommonAnswerDBAccess();
		try {
			dba_common.open(id, false);
			answer_list = dba_common.retrieveAll();
			logger.info("Read common answers for analyst {}'s query {} from the database.", analystId, queryId);
		} catch (Exception e) {
			logger.error("Reading common answers for analyst {}'s query {} from the database.", analystId, queryId, e);
			return null;
		} finally {
			try {
				dba_common.close();
			} catch (SQLException e) {}
		}
		
		NoiseGeneration.generate_for_resizing(answer_list, (bucket_count + 7) / 8, noise_count, bucketSizes);
		AnswerShuffling.shuffle_for_resizing(answer_list, shuffling_seed + resizingTimes, bucketSizes);
		
		List<ByteBuffer> ans_list = new ArrayList<ByteBuffer>(answer_list.size());
		for (byte[] answer : answer_list) {
			ans_list.add(ByteBuffer.wrap(answer));
		}
		
		return ans_list;
	}
}
