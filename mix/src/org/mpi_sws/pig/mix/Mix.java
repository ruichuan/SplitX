package org.mpi_sws.pig.mix;

import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.mix.common.MixCommon;
import org.mpi_sws.pig.mix.logic.GarbageCollectionTimer;
import org.mpi_sws.pig.mix.logic.MixProxyingTimer;
import org.mpi_sws.pig.mix.logic.QueryDescriptionRetrievalTimer;
//import org.mpi_sws.pig.mix.server.MixProxyingServer;
import org.mpi_sws.pig.mix.server.MixReceivingServer;
import org.mpi_sws.pig.mix.server.web.MixProxyingWebServer;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class Mix {
	
	public void start (byte mixId, String dbPath) {
		switch (mixId) {
		case 0:
			MixCommon.local_mix_host = Common.mix_1_host;
			MixCommon.local_mix_receiving_port = Common.mix_1_receiving_port;
			MixCommon.local_mix_proxying_port = Common.mix_1_proxying_port;
			MixCommon.peer_mix_host = Common.mix_2_host;
			MixCommon.peer_mix_receiving_port = Common.mix_2_receiving_port;
			MixCommon.is_even_query_master = true;
			break;
		case 1:
			MixCommon.local_mix_host = Common.mix_2_host;
			MixCommon.local_mix_receiving_port = Common.mix_2_receiving_port;
			MixCommon.local_mix_proxying_port = Common.mix_2_proxying_port;
			MixCommon.peer_mix_host = Common.mix_1_host;
			MixCommon.peer_mix_receiving_port = Common.mix_1_receiving_port;
			MixCommon.is_even_query_master = false;
			break;
		}
		MixCommon.db_name_prefix += dbPath;
		
		new QueryDescriptionRetrievalTimer().schedule();
		new Thread(new MixReceivingServer()).start();
//		new Thread(new MixProxyingServer()).start();
		new Thread(new MixProxyingWebServer()).start();
		new MixProxyingTimer().schedule();
		new GarbageCollectionTimer().schedule();
	}
	
	public static void main(String[] args) {
		
		if (args.length != 2) {
			System.err.println("Usage: need to specify the Mix ID and the database folder.");
			System.exit(1);
		}
		
		try {
			byte mixId = Byte.valueOf(args[0]);
			if (mixId != 0 && mixId != 1) {
				System.err.println("Usage: the Mix ID specified must be 0 or 1.");
				System.exit(1);
			}
			
			String dbPath = args[1].trim();
			if (dbPath.isEmpty()) {
				System.err.println("Usage: the database folder needs to be specified.");
				System.exit(1);
			}
			
			new Mix().start(mixId, dbPath);
			
		} catch (NumberFormatException e) {
			System.err.println("Usage: the Mix ID specified must be 0 or 1.");
			System.exit(1);
		}
	}
}
