package org.mpi_sws.pig.aggregator;

import org.mpi_sws.pig.aggregator.logic.GarbageCollectionTimer;
import org.mpi_sws.pig.aggregator.logic.QueryDescriptionGenerationTimer;
import org.mpi_sws.pig.aggregator.server.AggregatorReceivingServer;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class Aggregator {
	
	public void start (long startVersionId) {
		AggregatorReceivingServer.real_global_latest_version_id = startVersionId;
		new Thread(new AggregatorReceivingServer()).start();
		new QueryDescriptionGenerationTimer().schedule();
		new GarbageCollectionTimer().schedule();
	}
	
	public static void main(String[] args) {
		
		if (args.length != 1) {
			System.err.println("Usage: need to specify the starting version ID (>= 0).");
			System.exit(1);
		}
		
		try {
			long startVersionId = Long.valueOf(args[0]);
			if (startVersionId < 0) {
				System.err.println("Usage: the starting version ID specified must be non-negative (>= 0).");
				System.exit(1);
			}
			
			new Aggregator().start(startVersionId);
			
		} catch (NumberFormatException e) {
			System.err.println("Usage: the starting version ID specified must be non-negative (>= 0).");
			System.exit(1);
		}
	}
}
