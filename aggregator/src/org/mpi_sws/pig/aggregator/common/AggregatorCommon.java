package org.mpi_sws.pig.aggregator.common;

import org.mpi_sws.pig.common.Common;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class AggregatorCommon {
	
	public static int receiving_server_selector_threads = 16;
	public static int receiving_server_worker_threads = 256;
	
	public static long active_query_description_generation_interval = 1 * 60 * 1000;
	
	// Below should never be modified.
	
	public static String local_host = Common.aggregator_host;
	public static int local_receiving_port = Common.aggregator_receiving_port;
	public static String mix_1_host = Common.mix_1_host;
	public static int mix_1_receiving_port = Common.mix_1_receiving_port;
	public static String mix_2_host = Common.mix_2_host;
	public static int mix_2_receiving_port = Common.mix_2_receiving_port;
}
