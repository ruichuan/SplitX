package org.mpi_sws.pig.aggregator.logic;

import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.mpi_sws.pig.aggregator.server.AggregatorReceivingServer;
import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.message.QueryNoisyAnswers;
import org.mpi_sws.pig.common.utility.GarbageCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class GarbageCollectionTimer {
	
	private static final Logger logger = LoggerFactory.getLogger(GarbageCollectionTimer.class);
	
	private Timer timer = new Timer();
	
	private Set<Long> set_SplitRequests = new HashSet<Long>();
	private Set<Long> set_SplitAnswers = new HashSet<Long>();
	private Set<String> set_NoisyAnswers = new HashSet<String>();
	
	private GarbageCollection<Long, byte[]> gc_SplitRequests = new GarbageCollection<Long, byte[]>();
	private GarbageCollection<Long, byte[]> gc_SplitAnswers = new GarbageCollection<Long, byte[]>();
	private GarbageCollection<String, QueryNoisyAnswers> gc_NoisyAnswers = new GarbageCollection<String, QueryNoisyAnswers>();
	
	public void schedule () {
		timer.schedule(new GarbageCollectionTask(), 0, Common.garbage_collection_interval);
		logger.info("Scheduled the garbage collection timer.");
	}
	
	private class GarbageCollectionTask extends TimerTask {

		@Override
		public void run() {
			logger.info("Garbage collection started.");
			set_SplitRequests = gc_SplitRequests.collect(AggregatorReceivingServer.map_SplitRequests, set_SplitRequests);
			set_SplitAnswers = gc_SplitAnswers.collect(AggregatorReceivingServer.map_SplitAnswers, set_SplitAnswers);
			set_NoisyAnswers = gc_NoisyAnswers.collect(AggregatorReceivingServer.map_NoisyAnswers, set_NoisyAnswers);
			logger.info("Garbage collection finished.");
		}
	}
}
