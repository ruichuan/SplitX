package org.mpi_sws.pig.aggregator.logic;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.mpi_sws.pig.aggregator.server.AggregatorReceivingServer;
import org.mpi_sws.pig.common.message.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class QueryEndTimer {
	
	private static final Logger logger = LoggerFactory.getLogger(QueryEndTimer.class);
	
	private Timer timer = new Timer();
	private String analystId = null;
	private long queryId = -1;
	
	public QueryEndTimer (String analystId, long queryId) {
		this.analystId = analystId;
		this.queryId = queryId;
	}
	
	public void schedule (long delay) {
		timer.schedule(new QueryEndTask(), delay);
	}
	
	private class QueryEndTask extends TimerTask {

		@Override
		public void run() {
			logger.info("Time's up for analyst {}'s query {}.", analystId, queryId);
			Query query = AggregatorReceivingServer.map_ActiveQueries.get(analystId).remove(queryId);
			
			ConcurrentMap<Long, Query> queries = AggregatorReceivingServer.map_InactiveQueries.get(analystId);
			if (queries == null) {
				queries = new ConcurrentHashMap<Long, Query>();
				ConcurrentMap<Long, Query> exQueries = AggregatorReceivingServer.map_InactiveQueries.putIfAbsent(analystId, queries);
				if (exQueries != null) {
					queries = exQueries;
				}
			}
			queries.putIfAbsent(queryId, query);
		}
	}
}
