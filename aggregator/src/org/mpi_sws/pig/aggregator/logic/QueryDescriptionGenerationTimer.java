package org.mpi_sws.pig.aggregator.logic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.mpi_sws.pig.aggregator.common.AggregatorCommon;
import org.mpi_sws.pig.aggregator.server.AggregatorReceivingServer;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.message.QueryFile;
import org.mpi_sws.pig.common.utility.TJSONProtocolFixedB64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class QueryDescriptionGenerationTimer {
	
	private static final Logger logger = LoggerFactory.getLogger(QueryDescriptionGenerationTimer.class);
	
	private Timer timer = new Timer();
	
	public void schedule () {
		timer.schedule(new QueryDescriptionGenerationTask(), 0, AggregatorCommon.active_query_description_generation_interval);
	}
	
	private class QueryDescriptionGenerationTask extends TimerTask {
		
		private TSerializer serializer = new TSerializer(new TJSONProtocolFixedB64.Factory());

		@Override
		public void run() {
			
			long global_latest_version_id = -1;
			ConcurrentMap<String, QueryFile> map_ActiveQueryFiles = new ConcurrentHashMap<String, QueryFile>();
			ConcurrentMap<String, byte[]> map_ActiveSerializedQueryFiles = new ConcurrentHashMap<String, byte[]>();
			
			for (Iterator<Entry<String, ConcurrentMap<Long, Query>>> it_analyst = AggregatorReceivingServer.map_ActiveQueries.entrySet().iterator(); it_analyst.hasNext(); ) {
				Entry<String, ConcurrentMap<Long, Query>> analyst_to_queries = it_analyst.next();
				String analystId = analyst_to_queries.getKey();
				ConcurrentMap<Long, Query> queries = analyst_to_queries.getValue();
				
				if (queries.isEmpty()) {
					continue;
				}
				
				long latest_version_id = -1;
				List<Query> query_list = new ArrayList<Query>(queries.size());
				
				for (Iterator<Entry<Long, Query>> it_query = queries.entrySet().iterator(); it_query.hasNext(); ) {
					Entry<Long, Query> id_to_query = it_query.next();
					long queryId = id_to_query.getKey();
					Query query = id_to_query.getValue();
					long version_id = AggregatorReceivingServer.map_QueryVersions.get(analystId + "_" + queryId);
					
					if (latest_version_id < version_id) {
						latest_version_id = version_id;
					}
					query_list.add(query);
				}
				
				QueryFile file = new QueryFile(analystId, latest_version_id, query_list);
				
				try {
					byte[] b = serializer.serialize(file);
					map_ActiveSerializedQueryFiles.put(analystId, b);
					map_ActiveQueryFiles.put(analystId, file);
					if (global_latest_version_id < latest_version_id) {
						global_latest_version_id = latest_version_id;
					}
					logger.info("Generated analyst {}'s active query file (version ID = {}).", analystId, latest_version_id);
				} catch (TException e) {
					logger.error("Serializing analyst {}'s active query file (version ID = {}).", analystId, latest_version_id, e);
				}
			}
			
			AggregatorReceivingServer.map_ActiveSerializedQueryFiles = map_ActiveSerializedQueryFiles;
			AggregatorReceivingServer.map_ActiveQueryFiles = map_ActiveQueryFiles;
			AggregatorReceivingServer.temp_global_latest_version_id = global_latest_version_id;
		}
	}
}
