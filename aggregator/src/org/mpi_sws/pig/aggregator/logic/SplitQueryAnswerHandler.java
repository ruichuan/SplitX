package org.mpi_sws.pig.aggregator.logic;

import java.util.List;

import org.mpi_sws.pig.common.message.SplitMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class SplitQueryAnswerHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(SplitQueryAnswerHandler.class);
	
	public void process (SplitMessage answer, List<List<SplitMessage>> responses) {
		
		if (! answer.isSetDstMixId()) {
			logger.warn("The joint (still split) query answer {} does not specify the destination mix.", answer.getSplitId());
			return;
		}
		
		byte dstMixId = answer.getDstMixId();
		
		if (dstMixId != 0 && dstMixId != 1) {
			logger.warn("The joint (still split) query answer {}'s destination mix {} does not exist.", answer.getSplitId(), dstMixId);
			return;
		}
		
		responses.get(dstMixId).add(answer);
	}
}
