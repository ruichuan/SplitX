package org.mpi_sws.pig.aggregator.logic;

import java.nio.ByteBuffer;
import java.util.List;

import org.mpi_sws.pig.aggregator.server.AggregatorReceivingServer;
import org.mpi_sws.pig.common.message.QueryFile;
import org.mpi_sws.pig.common.message.QueryRequest;
import org.mpi_sws.pig.common.message.SplitMessage;
import org.mpi_sws.pig.common.utility.MessageProcessing;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class QueryRequestHandler {
	
	private static final ByteBuffer empty = ByteBuffer.wrap(new byte[0]);
	private MessageProcessing messageProcessing = new MessageProcessing();
	
	public void process (long splitId, QueryRequest request, List<List<SplitMessage>> responses) {
		String analystId = request.getAnalystId();
		long versionId = request.getLatestVersionId();
		
		QueryFile file = AggregatorReceivingServer.map_ActiveQueryFiles.get(analystId);
		if (file == null) {
			SplitMessage split = new SplitMessage(splitId, empty);
			responses.get(0).add(split);
			responses.get(1).add(split);
			return;
		}
		
		long latestVersionId = file.getVersionId();
		if (versionId >= latestVersionId) {
			SplitMessage split = new SplitMessage(splitId, empty);
			responses.get(0).add(split);
			responses.get(1).add(split);
			return;
		}
		
		byte[] b = AggregatorReceivingServer.map_ActiveSerializedQueryFiles.get(analystId);
		List<byte[]> s = messageProcessing.split(b);
		
		responses.get(0).add(new SplitMessage(splitId, ByteBuffer.wrap(s.get(0))));
		responses.get(1).add(new SplitMessage(splitId, ByteBuffer.wrap(s.get(1))));
	}
}
