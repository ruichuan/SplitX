package org.mpi_sws.pig.aggregator.logic;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.mpi_sws.pig.aggregator.common.AggregatorCommon;
import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.message.QueryResult;
import org.mpi_sws.pig.common.service.MixReceivingService;
import org.mpi_sws.pig.common.utility.MessageProcessing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class BucketResizingHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(BucketResizingHandler.class);
	private static final byte[] mask = new byte[]{(byte)(1 << 7), (byte)(1 << 6), (byte)(1 << 5), (byte)(1 << 4), (byte)(1 << 3), (byte)(1 << 2), (byte)(1 << 1), (byte)(1 << 0)};
	
	public QueryResult process (String analystId, long queryId, Query query, List<Integer> count_list, int noise) {
		int total = 0;
		for (int i = Common.special_bytes_in_answer * 8; i < count_list.size(); ++i) {
			total += count_list.get(i);
		}
		
		if (total <= 0) {
			logger.warn("(Approximately) no valid answers for analyst {}'s query {}.", analystId, queryId);
			return null;
		}
		
		int bound = 0;
		if (query.isSetAbsoluteBound()) {
			bound = query.getAbsoluteBound();
		}
		if (query.isSetPercentileBound()) {
			int bound_p = (int) Math.ceil(total * query.getPercentileBound());
			if (bound < bound_p) {
				bound = bound_p;
			}
		}
		
		List<Integer> bucketSizes = new ArrayList<Integer>();
		for (int i = 0; i < Common.special_bytes_in_answer * 8; ++i) {
			bucketSizes.add(1);
		}
		
		int sub_size = 0;
		int sub_total = 0;
		
		for (int i = Common.special_bytes_in_answer * 8; i < count_list.size(); ++i) {
			++sub_size;
			sub_total += count_list.get(i);
			
			if (sub_total >= bound) {
				bucketSizes.add(sub_size);
				sub_size = 0;
				sub_total = 0;
			}
		}
		
		if (sub_size > 0) {
			bucketSizes.add(sub_size);
		}
		
		long resizingTimes = 1;	// 1 for 1st resizing, 2 for 2nd resizing, and so on...
		
		List<ByteBuffer> answers_1 = retrieve_ResizedAnswers (AggregatorCommon.mix_1_host, AggregatorCommon.mix_1_receiving_port, analystId, queryId, bucketSizes, resizingTimes);
		if (answers_1 == null || answers_1.size() < noise) {
			logger.warn("Mix 1's resized noisy answers for analyst {}'s query {} are emtpy or incomplete.", analystId, queryId);
			return null;
		}
		
		List<ByteBuffer> answers_2 = retrieve_ResizedAnswers (AggregatorCommon.mix_2_host, AggregatorCommon.mix_2_receiving_port, analystId, queryId, bucketSizes, resizingTimes);
		if (answers_2 == null || answers_2.size() < noise) {
			logger.warn("Mix 2's resized noisy answers for analyst {}'s query {} are emtpy or incomplete.", analystId, queryId);
			return null;
		}
		
		int size_1 = answers_1.size();
		int size_2 = answers_2.size();
		
		if (size_1 != size_2) {
			logger.warn("The two halves of the resized noisy answers for analyst {}'s query {} do not match.", analystId, queryId);
			return null;
		}
		
		int counts_byte_length = (count_list.size() + 7) / 8;
		int counts_bit_length = count_list.size();
		int[] counts = new int[counts_bit_length];
		
		for (int i = 0; i < size_1; ++i) {
			byte[] answer_1 = answers_1.get(i).array();
			byte[] answer_2 = answers_2.get(i).array();
			if (answer_1.length != counts_byte_length || answer_2.length != counts_byte_length) {
				logger.warn("The two halves of the resized noisy answers for analyst {}'s query {} contain illegitimate answers.", analystId, queryId);
				return null;
			}
			byte[] answer = MessageProcessing.join(answer_1, answer_2);
			
			for (int pos = 0; pos < counts_bit_length; ++pos) {
				if ((answer[pos / 8] & mask[pos % 8]) != 0) {
					++counts[pos];
				}
			}
		}
		
		List<Integer> list = new ArrayList<Integer>(bucketSizes.size());
		
		int start = 0;
		int end = 0;
		for (int size : bucketSizes) {
			end = start + size;
			
			int count = 0;
			for (int i = start; i < end; ++i) {
				count += counts[i];
			}
			list.add(count - noise / 2);
			
			start = end;
		}
		
		QueryResult result = new QueryResult (queryId, list);
		result.setBucketSizes(bucketSizes);
		
		return result;
	}
	
	private List<ByteBuffer> retrieve_ResizedAnswers (String mix_host, int mix_receiving_port, String analystId, long queryId, List<Integer> bucketSizes, long resizingTimes) {
		TTransport transport = new TFramedTransport(new TSocket(mix_host, mix_receiving_port), Common.max_frame_size);
		TProtocol protocol = new TCompactProtocol(transport);
		MixReceivingService.Client client = new MixReceivingService.Client(protocol);
		
		try {
			transport.open();
			List<ByteBuffer> answers = client.resize_AggregatorToMix_Buckets(analystId, queryId, bucketSizes, resizingTimes);
			logger.info("Retrieved half of the resized noisy answers for analyst {}'s query {}.", analystId, queryId);
			return answers;
		} catch (TTransportException e) {
			logger.error("Connecting to mix {}.", mix_host, e);
			return null;
		} catch (TException e) {
			logger.error("Retrieving half of the resized noisy answers for analyst {}'s query {}.", analystId, queryId, e);
			return null;
		} finally {
			if (transport != null) {
				transport.close();
			}
		}
	}
}
