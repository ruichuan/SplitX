package org.mpi_sws.pig.aggregator.logic;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.mpi_sws.pig.aggregator.server.AggregatorReceivingServer;
import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.message.QueryNoisyAnswers;
import org.mpi_sws.pig.common.message.QueryResult;
import org.mpi_sws.pig.common.utility.MessageProcessing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class QueryNoisyAnswersHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(QueryNoisyAnswersHandler.class);
	private static final byte[] mask = new byte[]{(byte)(1 << 7), (byte)(1 << 6), (byte)(1 << 5), (byte)(1 << 4), (byte)(1 << 3), (byte)(1 << 2), (byte)(1 << 1), (byte)(1 << 0)};
	
	public void process (Query query, QueryNoisyAnswers one, QueryNoisyAnswers two) {
		String analystId = one.getAnalystId();
		long queryId = one.getQueryId();
		List<ByteBuffer> answers_1 = one.getAnswers();
		List<ByteBuffer> answers_2 = two.getAnswers();
		int size_1 = answers_1.size();
		int size_2 = answers_2.size();
		int noise_1 = one.getNoise();
		int noise_2 = two.getNoise();
		
		if (size_1 != size_2 || noise_1 != noise_2 || size_1 < noise_1 || noise_1 < 0) {
			logger.warn("The two halves of the noisy answers for analyst {}'s query {} do not match.", analystId, queryId);
			return;
		}
		
		int counts_byte_length = Common.special_bytes_in_answer + (query.getBuckets().getRangesSize() + 7) / 8;
		int counts_bit_length = 8 * Common.special_bytes_in_answer + query.getBuckets().getRangesSize();
		int[] counts = new int[counts_bit_length];
		
		for (int i = 0; i < size_1; ++i) {
			byte[] answer_1 = answers_1.get(i).array();
			byte[] answer_2 = answers_2.get(i).array();
			if (answer_1.length != counts_byte_length || answer_2.length != counts_byte_length) {
				logger.warn("The two halves of the noisy answers for analyst {}'s query {} contain illegitimate answers.", analystId, queryId);
				return;
			}
			byte[] answer = MessageProcessing.join(answer_1, answer_2);
			
			for (int pos = 0; pos < counts_bit_length; ++pos) {
				if ((answer[pos / 8] & mask[pos % 8]) != 0) {
					++counts[pos];
				}
			}
		}
		
		List<Integer> list = new ArrayList<Integer>(counts_bit_length);
		for (int c : counts) {
			list.add(c - noise_1 / 2);
		}
		
		QueryResult result = null;
		
		if (query.isSetPercentileBound() || query.isSetAbsoluteBound()) {
			BucketResizingHandler handler = new BucketResizingHandler();
			if ((result = handler.process(analystId, queryId, query, list, noise_1)) == null) {
				logger.warn("Bucket resizing for analyst {}'s query {} failed.", analystId, queryId);
			} else {
				logger.info("Bucket resizing for analyst {}'s query {} succeeded.", analystId, queryId);
			}
		}
		
		if (result == null) {
			result = new QueryResult (queryId, list);
		}
		
		ConcurrentMap<Long, QueryResult> map = AggregatorReceivingServer.map_Results.get(analystId);
		if (map == null) {
			map = new ConcurrentHashMap<Long, QueryResult>();
			ConcurrentMap<Long, QueryResult> exMap = AggregatorReceivingServer.map_Results.putIfAbsent(analystId, map);
			if (exMap != null) {
				map = exMap;
			}
		}
		
		if (map.putIfAbsent(queryId, result) == null) {
			logger.info("Recorded the result of analyst {}'s query {}.", analystId, queryId);
		} else {
			logger.warn("The result of analyst {}'s query {} has existed.", analystId, queryId);
		}
	}
}
