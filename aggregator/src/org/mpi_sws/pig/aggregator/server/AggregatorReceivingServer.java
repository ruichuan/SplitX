package org.mpi_sws.pig.aggregator.server;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.mpi_sws.pig.aggregator.common.AggregatorCommon;
import org.mpi_sws.pig.aggregator.logic.QueryEndTimer;
import org.mpi_sws.pig.aggregator.logic.QueryNoisyAnswersHandler;
import org.mpi_sws.pig.aggregator.logic.QueryRequestHandler;
import org.mpi_sws.pig.aggregator.logic.SplitQueryAnswerHandler;
import org.mpi_sws.pig.common.Common;
import org.mpi_sws.pig.common.service.AggregatorReceivingService;
import org.mpi_sws.pig.common.service.AggregatorReceivingService.Iface;
import org.mpi_sws.pig.common.entity.Server;
import org.mpi_sws.pig.common.message.Buckets;
import org.mpi_sws.pig.common.message.Query;
import org.mpi_sws.pig.common.message.QueryFile;
import org.mpi_sws.pig.common.message.QueryNoisyAnswers;
import org.mpi_sws.pig.common.message.QueryRequest;
import org.mpi_sws.pig.common.message.QueryResult;
import org.mpi_sws.pig.common.message.SplitMessage;
import org.mpi_sws.pig.common.utility.MessageProcessing;
import org.mpi_sws.pig.common.utility.TJSONProtocolFixedB64;
import org.mpi_sws.pig.common.service.MixReceivingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class AggregatorReceivingServer implements Iface, Runnable {
	
	private static final Logger logger = LoggerFactory.getLogger(AggregatorReceivingServer.class);
	
	public static volatile long real_global_latest_version_id = -1;
	public static ConcurrentMap<String, ConcurrentMap<Long, Query>> map_ActiveQueries = new ConcurrentHashMap<String, ConcurrentMap<Long, Query>>();
	public static ConcurrentMap<String, ConcurrentMap<Long, Query>> map_InactiveQueries = new ConcurrentHashMap<String, ConcurrentMap<Long, Query>>();
	public static ConcurrentMap<String, Long> map_QueryVersions = new ConcurrentHashMap<String, Long>();
	
	public static volatile long temp_global_latest_version_id = -1;
	public static ConcurrentMap<String, QueryFile> map_ActiveQueryFiles = new ConcurrentHashMap<String, QueryFile>();
	public static ConcurrentMap<String, byte[]> map_ActiveSerializedQueryFiles = new ConcurrentHashMap<String, byte[]>();
	
	public static ConcurrentMap<String, ConcurrentMap<Long, QueryResult>> map_Results = new ConcurrentHashMap<String, ConcurrentMap<Long, QueryResult>>();

	public static ConcurrentMap<Long, byte[]> map_SplitRequests = new ConcurrentHashMap<Long, byte[]>();
	public static ConcurrentMap<Long, byte[]> map_SplitAnswers = new ConcurrentHashMap<Long, byte[]>();
	public static ConcurrentMap<String, QueryNoisyAnswers> map_NoisyAnswers = new ConcurrentHashMap<String, QueryNoisyAnswers>();
	
	@Override
	public void send_ClientToAggregator_QueryRequests(
			List<SplitMessage> queryRequests) throws TException {
		logger.info("Received {} split query requests from clients via mix.", queryRequests.size());
		
		TDeserializer deserializer = new TDeserializer(new TJSONProtocolFixedB64.Factory());
		QueryRequestHandler handler = new QueryRequestHandler();
		
		List<List<SplitMessage>> queryResponses = new ArrayList<List<SplitMessage>>(2);
		queryResponses.add(new ArrayList<SplitMessage>());
		queryResponses.add(new ArrayList<SplitMessage>());
		
		for (SplitMessage split : queryRequests) {
			long splitId = split.getSplitId();
			byte[] splitMessage = split.getMessage();
			
			if (map_SplitRequests.putIfAbsent(splitId, splitMessage) == null) {
				continue;
			}
			
			byte[] exSplitMessage = map_SplitRequests.remove(splitId);
			if (exSplitMessage != null && splitMessage.length == exSplitMessage.length) {
				try {
					byte[] message = MessageProcessing.join(splitMessage, exSplitMessage);
					QueryRequest request = new QueryRequest();
					deserializer.deserialize(request, message);
					handler.process(splitId, request, queryResponses);
				} catch (TException e) {
					logger.error("Deserializing the query request {}.", splitId, e);
				}
			} else {
				logger.warn("Two split query requests {} do not match.", splitId);
			}
		}
		
		if (queryResponses.get(0).size() > 0) {
			send_QueryResponses(AggregatorCommon.mix_1_host, AggregatorCommon.mix_1_receiving_port, queryResponses.get(0));
		}
		if (queryResponses.get(1).size() > 0) {
			send_QueryResponses(AggregatorCommon.mix_2_host, AggregatorCommon.mix_2_receiving_port, queryResponses.get(1));
		}
	}
	
	private void send_QueryResponses (String mix_host, int mix_receiving_port, List<SplitMessage> response) {
		TTransport transport = new TFramedTransport(new TSocket(mix_host, mix_receiving_port), Common.max_frame_size);
		TProtocol protocol = new TCompactProtocol(transport);
		MixReceivingService.Client client = new MixReceivingService.Client(protocol);
		
		try {
			transport.open();
			// TRICK!!!  Send split query response one by one to avoid sending a huge message
			for (int i = 0; i < response.size(); ++i) {
				client.forward_AggregatorToClient_QueryResponses(response.subList(i, i + 1));
			}
			logger.info("Sent {} split query responses to clients via mix {}.", response.size(), mix_host);
		} catch (TTransportException e) {
			logger.error("Connecting to mix {}.", mix_host, e);
		} catch (TException e) {
			logger.error("Sending {} split query responses to clients via mix {}.", response.size(), mix_host, e);
		} finally {
			if (transport != null) {
				transport.close();
			}
		}
	}

	@Override
	public void send_ClientToAggregator_QueryAnswers(
			List<SplitMessage> queryAnswers) throws TException {
		logger.info("Received {} split query answers from clients via mix.", queryAnswers.size());
		
		TDeserializer deserializer = new TDeserializer(new TJSONProtocolFixedB64.Factory());
		SplitQueryAnswerHandler handler = new SplitQueryAnswerHandler();
		
		List<List<SplitMessage>> responses = new ArrayList<List<SplitMessage>>(2);
		responses.add(new ArrayList<SplitMessage>());
		responses.add(new ArrayList<SplitMessage>());
		
		for (SplitMessage split : queryAnswers) {
			long splitId = split.getSplitId();
			byte[] splitMessage = split.getMessage();
			
			if (map_SplitAnswers.putIfAbsent(splitId, splitMessage) == null) {
				continue;
			}
			
			byte[] exSplitMessage = map_SplitAnswers.remove(splitId);
			if (exSplitMessage != null && splitMessage.length == exSplitMessage.length) {
				try {
					byte[] message = MessageProcessing.join(splitMessage, exSplitMessage);
					SplitMessage answer = new SplitMessage();
					deserializer.deserialize(answer, message);
					handler.process(answer, responses);
				} catch (TException e) {
					logger.error("Deserializing the split query answer {}.", splitId, e);
				}
			} else {
				logger.warn("Two split query answers {} do not match.", splitId);
			}
		}
		
		if (responses.get(0).size() > 0) {
			send_QueryAnswers(AggregatorCommon.mix_1_host, AggregatorCommon.mix_1_receiving_port, responses.get(0));
		}
		if (responses.get(1).size() > 0) {
			send_QueryAnswers(AggregatorCommon.mix_2_host, AggregatorCommon.mix_2_receiving_port, responses.get(1));
		}
	}
	
	private void send_QueryAnswers (String mix_host, int mix_receiving_port, List<SplitMessage> response) {
		TTransport transport = new TFramedTransport(new TSocket(mix_host, mix_receiving_port), Common.max_frame_size);
		TProtocol protocol = new TCompactProtocol(transport);
		MixReceivingService.Client client = new MixReceivingService.Client(protocol);
		
		try {
			transport.open();
			client.send_AggregatorToMix_QueryAnswers(response);
			logger.info("Sent {} split query answers to mix {}.", response.size(), mix_host);
		} catch (TTransportException e) {
			logger.error("Connecting to mix {}.", mix_host, e);
		} catch (TException e) {
			logger.error("Sending {} split query answers to mix {}.", response.size(), mix_host, e);
		} finally {
			if (transport != null) {
				transport.close();
			}
		}
	}

	@Override
	public void send_MixToAggregator_QueryNoisyAnswers(
			QueryNoisyAnswers noisyAnswers) throws TException {
		String analystId = noisyAnswers.getAnalystId();
		long queryId = noisyAnswers.getQueryId();
		logger.info("Received half of the noisy answers for analyst {}'s query {}.", analystId, queryId);
		
		ConcurrentMap<Long, Query> queries = map_InactiveQueries.get(analystId);
		if (queries == null) {
			logger.warn("Analyst {}'s query {} has not reached the stage of answer collection.", analystId, queryId);
			return;
		}
		
		Query query = queries.get(queryId);
		if (query == null) {
			logger.warn("Analyst {}'s query {} has not reached the stage of answer collection.", analystId, queryId);
			return;
		}
		
		String id = analystId + "_" + queryId;
		if (map_NoisyAnswers.putIfAbsent(id, noisyAnswers) == null) {
			return;
		}
		QueryNoisyAnswers exNoisyAnswers = map_NoisyAnswers.remove(id);
		
		QueryNoisyAnswersHandler handler = new QueryNoisyAnswersHandler();
		handler.process(query, noisyAnswers, exNoisyAnswers);
		queries.remove(queryId);
		map_QueryVersions.remove(id);
	}

	@Override
	public List<QueryFile> retrieve_MixToAggregator_QueryDescription(
			long latestVersionId) throws TException {
		logger.info("Received mix's query description retrival message with its latest version ID of {}.", latestVersionId);
		
		List<QueryFile> files = new ArrayList<QueryFile>();
		if (latestVersionId < temp_global_latest_version_id) {
			for (QueryFile file : map_ActiveQueryFiles.values()) {
				if (latestVersionId < file.getVersionId()) {
					files.add(file);
				}
			}
		}
		
		logger.info("Sent {} query files to mix.", files.size());
		return files;
	}

	@Override
	public void upload_AnalystToAggregator_Queries(String analystId,
			List<Query> queries) throws TException {
		logger.info("Received {} queries from analyst {}.", queries.size(), analystId);
		
		for (Query query : queries) {
			long queryId = query.getQueryId();
			long queryEndTime = query.getQueryEndTime();
			double epsilon = query.getEpsilon();
			String sql = query.getSql();
			Buckets buckets = query.getBuckets();
			
			String id = analystId + "_" + queryId;
			
			if (map_QueryVersions.containsKey(id)) {
				logger.warn("Analyst {}'s query {} is being processed.", analystId, queryId);
				continue;
			}
			
			if (queryEndTime <= System.currentTimeMillis()) {
				logger.warn("Analyst {}'s query {} is outdated.", analystId, queryId);
				continue;
			}
			
			if (epsilon <= 0) {
				logger.warn("Analyst {}'s query {} has a non-positive epsilon.", analystId, queryId);
				continue;
			}
			
			if (sql.trim().isEmpty()) {
				logger.warn("Analyst {}'s query {} does not contain a SQL query.", analystId, queryId);
				continue;
			}
			
			if (buckets.getRangesSize() <= 0) {
				logger.warn("Analyst {}'s query {} does not specify any buckets.", analystId, queryId);
				continue;
			}
			
			if (query.isSetPercentileBound()) {
				double bound = query.getPercentileBound();
				if (bound < 0 || bound > 1) {
					logger.warn("Analyst {}'s query {} has an illegitimate percentile bound.", analystId, queryId);
					continue;
				}
			}
			
			if (query.isSetAbsoluteBound()) {
				int bound = query.getAbsoluteBound();
				if (bound < 0) {
					logger.warn("Analyst {}'s query {} has an illegitimate absolute bound.", analystId, queryId);
					continue;
				}
			}
			
			map_QueryVersions.putIfAbsent(id, real_global_latest_version_id++);
			ConcurrentMap<Long, Query> map = map_ActiveQueries.get(analystId);
			if (map == null) {
				map = new ConcurrentHashMap<Long, Query>();
				ConcurrentMap<Long, Query> exMap = map_ActiveQueries.putIfAbsent(analystId, map);
				if (exMap != null) {
					map = exMap;
				}
			}
			map.putIfAbsent(queryId, query);
			
			QueryEndTimer query_end_timer = new QueryEndTimer(analystId, queryId);
			long delay = queryEndTime - System.currentTimeMillis();
			query_end_timer.schedule(delay);
			logger.info("Accepted analyst {}'s new query {}.", analystId, queryId);
		}
	}

	@Override
	public List<QueryResult> download_AggregatorToAnalyst_QueryResults(
			String analystId) throws TException {
		logger.info("Received analyst {}'s request for downloading query results.", analystId);
		
		List<QueryResult> response = new ArrayList<QueryResult>();
		ConcurrentMap<Long, QueryResult> results = map_Results.get(analystId);
		
		if (results == null) {
			logger.info("Analyst {} does not have any query results.", analystId);
			return response;
		}
		
		for (Iterator<Entry<Long, QueryResult>> it_result = results.entrySet().iterator(); it_result.hasNext(); ) {
			response.add(it_result.next().getValue());
			it_result.remove();
		}
		
		logger.info("Returned the results of {} queries to analyst {}.", response.size(), analystId);
		return response;
	}

	@Override
	public void run() {
		logger.info("Starting the aggregator receiving server.");
		
		InetSocketAddress bindAddr = new InetSocketAddress(AggregatorCommon.local_host, AggregatorCommon.local_receiving_port);
		int clientTimeout = 0;
		TProtocolFactory protocol = new TCompactProtocol.Factory();
		TProcessor processor = new AggregatorReceivingService.Processor<Iface>(new AggregatorReceivingServer());
		int nSelectorThreads = AggregatorCommon.receiving_server_selector_threads;
		int nWorkerThreads = AggregatorCommon.receiving_server_worker_threads;
		
		Server server = null;
		try {
			server = new Server(bindAddr, clientTimeout, protocol, processor, nSelectorThreads, nWorkerThreads);
			server.start();
		} catch (TTransportException e) {
			logger.error("Starting the aggregator receiving server.", e);
		} finally {
			if (server != null) {
				server.stop();
			}
		}
	}
}
