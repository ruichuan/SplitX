/**
 * Autogenerated by Thrift Compiler (0.9.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package org.mpi_sws.pig.common.message;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueryFile implements org.apache.thrift.TBase<QueryFile, QueryFile._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("QueryFile");

  private static final org.apache.thrift.protocol.TField ANALYST_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("analystId", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField VERSION_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("versionId", org.apache.thrift.protocol.TType.I64, (short)2);
  private static final org.apache.thrift.protocol.TField TIME_FIELD_DESC = new org.apache.thrift.protocol.TField("time", org.apache.thrift.protocol.TType.I64, (short)3);
  private static final org.apache.thrift.protocol.TField QUERIES_FIELD_DESC = new org.apache.thrift.protocol.TField("queries", org.apache.thrift.protocol.TType.LIST, (short)4);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new QueryFileStandardSchemeFactory());
    schemes.put(TupleScheme.class, new QueryFileTupleSchemeFactory());
  }

  public String analystId; // required
  public long versionId; // required
  public long time; // optional
  public List<Query> queries; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    ANALYST_ID((short)1, "analystId"),
    VERSION_ID((short)2, "versionId"),
    TIME((short)3, "time"),
    QUERIES((short)4, "queries");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // ANALYST_ID
          return ANALYST_ID;
        case 2: // VERSION_ID
          return VERSION_ID;
        case 3: // TIME
          return TIME;
        case 4: // QUERIES
          return QUERIES;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __VERSIONID_ISSET_ID = 0;
  private static final int __TIME_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  private _Fields optionals[] = {_Fields.TIME};
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.ANALYST_ID, new org.apache.thrift.meta_data.FieldMetaData("analystId", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.VERSION_ID, new org.apache.thrift.meta_data.FieldMetaData("versionId", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    tmpMap.put(_Fields.TIME, new org.apache.thrift.meta_data.FieldMetaData("time", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    tmpMap.put(_Fields.QUERIES, new org.apache.thrift.meta_data.FieldMetaData("queries", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.ListMetaData(org.apache.thrift.protocol.TType.LIST, 
            new org.apache.thrift.meta_data.StructMetaData(org.apache.thrift.protocol.TType.STRUCT, Query.class))));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(QueryFile.class, metaDataMap);
  }

  public QueryFile() {
  }

  public QueryFile(
    String analystId,
    long versionId,
    List<Query> queries)
  {
    this();
    this.analystId = analystId;
    this.versionId = versionId;
    setVersionIdIsSet(true);
    this.queries = queries;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public QueryFile(QueryFile other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetAnalystId()) {
      this.analystId = other.analystId;
    }
    this.versionId = other.versionId;
    this.time = other.time;
    if (other.isSetQueries()) {
      List<Query> __this__queries = new ArrayList<Query>();
      for (Query other_element : other.queries) {
        __this__queries.add(new Query(other_element));
      }
      this.queries = __this__queries;
    }
  }

  public QueryFile deepCopy() {
    return new QueryFile(this);
  }

  @Override
  public void clear() {
    this.analystId = null;
    setVersionIdIsSet(false);
    this.versionId = 0;
    setTimeIsSet(false);
    this.time = 0;
    this.queries = null;
  }

  public String getAnalystId() {
    return this.analystId;
  }

  public QueryFile setAnalystId(String analystId) {
    this.analystId = analystId;
    return this;
  }

  public void unsetAnalystId() {
    this.analystId = null;
  }

  /** Returns true if field analystId is set (has been assigned a value) and false otherwise */
  public boolean isSetAnalystId() {
    return this.analystId != null;
  }

  public void setAnalystIdIsSet(boolean value) {
    if (!value) {
      this.analystId = null;
    }
  }

  public long getVersionId() {
    return this.versionId;
  }

  public QueryFile setVersionId(long versionId) {
    this.versionId = versionId;
    setVersionIdIsSet(true);
    return this;
  }

  public void unsetVersionId() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __VERSIONID_ISSET_ID);
  }

  /** Returns true if field versionId is set (has been assigned a value) and false otherwise */
  public boolean isSetVersionId() {
    return EncodingUtils.testBit(__isset_bitfield, __VERSIONID_ISSET_ID);
  }

  public void setVersionIdIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __VERSIONID_ISSET_ID, value);
  }

  public long getTime() {
    return this.time;
  }

  public QueryFile setTime(long time) {
    this.time = time;
    setTimeIsSet(true);
    return this;
  }

  public void unsetTime() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __TIME_ISSET_ID);
  }

  /** Returns true if field time is set (has been assigned a value) and false otherwise */
  public boolean isSetTime() {
    return EncodingUtils.testBit(__isset_bitfield, __TIME_ISSET_ID);
  }

  public void setTimeIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __TIME_ISSET_ID, value);
  }

  public int getQueriesSize() {
    return (this.queries == null) ? 0 : this.queries.size();
  }

  public java.util.Iterator<Query> getQueriesIterator() {
    return (this.queries == null) ? null : this.queries.iterator();
  }

  public void addToQueries(Query elem) {
    if (this.queries == null) {
      this.queries = new ArrayList<Query>();
    }
    this.queries.add(elem);
  }

  public List<Query> getQueries() {
    return this.queries;
  }

  public QueryFile setQueries(List<Query> queries) {
    this.queries = queries;
    return this;
  }

  public void unsetQueries() {
    this.queries = null;
  }

  /** Returns true if field queries is set (has been assigned a value) and false otherwise */
  public boolean isSetQueries() {
    return this.queries != null;
  }

  public void setQueriesIsSet(boolean value) {
    if (!value) {
      this.queries = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case ANALYST_ID:
      if (value == null) {
        unsetAnalystId();
      } else {
        setAnalystId((String)value);
      }
      break;

    case VERSION_ID:
      if (value == null) {
        unsetVersionId();
      } else {
        setVersionId((Long)value);
      }
      break;

    case TIME:
      if (value == null) {
        unsetTime();
      } else {
        setTime((Long)value);
      }
      break;

    case QUERIES:
      if (value == null) {
        unsetQueries();
      } else {
        setQueries((List<Query>)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case ANALYST_ID:
      return getAnalystId();

    case VERSION_ID:
      return Long.valueOf(getVersionId());

    case TIME:
      return Long.valueOf(getTime());

    case QUERIES:
      return getQueries();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case ANALYST_ID:
      return isSetAnalystId();
    case VERSION_ID:
      return isSetVersionId();
    case TIME:
      return isSetTime();
    case QUERIES:
      return isSetQueries();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof QueryFile)
      return this.equals((QueryFile)that);
    return false;
  }

  public boolean equals(QueryFile that) {
    if (that == null)
      return false;

    boolean this_present_analystId = true && this.isSetAnalystId();
    boolean that_present_analystId = true && that.isSetAnalystId();
    if (this_present_analystId || that_present_analystId) {
      if (!(this_present_analystId && that_present_analystId))
        return false;
      if (!this.analystId.equals(that.analystId))
        return false;
    }

    boolean this_present_versionId = true;
    boolean that_present_versionId = true;
    if (this_present_versionId || that_present_versionId) {
      if (!(this_present_versionId && that_present_versionId))
        return false;
      if (this.versionId != that.versionId)
        return false;
    }

    boolean this_present_time = true && this.isSetTime();
    boolean that_present_time = true && that.isSetTime();
    if (this_present_time || that_present_time) {
      if (!(this_present_time && that_present_time))
        return false;
      if (this.time != that.time)
        return false;
    }

    boolean this_present_queries = true && this.isSetQueries();
    boolean that_present_queries = true && that.isSetQueries();
    if (this_present_queries || that_present_queries) {
      if (!(this_present_queries && that_present_queries))
        return false;
      if (!this.queries.equals(that.queries))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(QueryFile other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    QueryFile typedOther = (QueryFile)other;

    lastComparison = Boolean.valueOf(isSetAnalystId()).compareTo(typedOther.isSetAnalystId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetAnalystId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.analystId, typedOther.analystId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetVersionId()).compareTo(typedOther.isSetVersionId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetVersionId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.versionId, typedOther.versionId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetTime()).compareTo(typedOther.isSetTime());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetTime()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.time, typedOther.time);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetQueries()).compareTo(typedOther.isSetQueries());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetQueries()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.queries, typedOther.queries);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("QueryFile(");
    boolean first = true;

    sb.append("analystId:");
    if (this.analystId == null) {
      sb.append("null");
    } else {
      sb.append(this.analystId);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("versionId:");
    sb.append(this.versionId);
    first = false;
    if (isSetTime()) {
      if (!first) sb.append(", ");
      sb.append("time:");
      sb.append(this.time);
      first = false;
    }
    if (!first) sb.append(", ");
    sb.append("queries:");
    if (this.queries == null) {
      sb.append("null");
    } else {
      sb.append(this.queries);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class QueryFileStandardSchemeFactory implements SchemeFactory {
    public QueryFileStandardScheme getScheme() {
      return new QueryFileStandardScheme();
    }
  }

  private static class QueryFileStandardScheme extends StandardScheme<QueryFile> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, QueryFile struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // ANALYST_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.analystId = iprot.readString();
              struct.setAnalystIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // VERSION_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.versionId = iprot.readI64();
              struct.setVersionIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // TIME
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.time = iprot.readI64();
              struct.setTimeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // QUERIES
            if (schemeField.type == org.apache.thrift.protocol.TType.LIST) {
              {
                org.apache.thrift.protocol.TList _list8 = iprot.readListBegin();
                struct.queries = new ArrayList<Query>(_list8.size);
                for (int _i9 = 0; _i9 < _list8.size; ++_i9)
                {
                  Query _elem10; // required
                  _elem10 = new Query();
                  _elem10.read(iprot);
                  struct.queries.add(_elem10);
                }
                iprot.readListEnd();
              }
              struct.setQueriesIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, QueryFile struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.analystId != null) {
        oprot.writeFieldBegin(ANALYST_ID_FIELD_DESC);
        oprot.writeString(struct.analystId);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(VERSION_ID_FIELD_DESC);
      oprot.writeI64(struct.versionId);
      oprot.writeFieldEnd();
      if (struct.isSetTime()) {
        oprot.writeFieldBegin(TIME_FIELD_DESC);
        oprot.writeI64(struct.time);
        oprot.writeFieldEnd();
      }
      if (struct.queries != null) {
        oprot.writeFieldBegin(QUERIES_FIELD_DESC);
        {
          oprot.writeListBegin(new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.STRUCT, struct.queries.size()));
          for (Query _iter11 : struct.queries)
          {
            _iter11.write(oprot);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class QueryFileTupleSchemeFactory implements SchemeFactory {
    public QueryFileTupleScheme getScheme() {
      return new QueryFileTupleScheme();
    }
  }

  private static class QueryFileTupleScheme extends TupleScheme<QueryFile> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, QueryFile struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetAnalystId()) {
        optionals.set(0);
      }
      if (struct.isSetVersionId()) {
        optionals.set(1);
      }
      if (struct.isSetTime()) {
        optionals.set(2);
      }
      if (struct.isSetQueries()) {
        optionals.set(3);
      }
      oprot.writeBitSet(optionals, 4);
      if (struct.isSetAnalystId()) {
        oprot.writeString(struct.analystId);
      }
      if (struct.isSetVersionId()) {
        oprot.writeI64(struct.versionId);
      }
      if (struct.isSetTime()) {
        oprot.writeI64(struct.time);
      }
      if (struct.isSetQueries()) {
        {
          oprot.writeI32(struct.queries.size());
          for (Query _iter12 : struct.queries)
          {
            _iter12.write(oprot);
          }
        }
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, QueryFile struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(4);
      if (incoming.get(0)) {
        struct.analystId = iprot.readString();
        struct.setAnalystIdIsSet(true);
      }
      if (incoming.get(1)) {
        struct.versionId = iprot.readI64();
        struct.setVersionIdIsSet(true);
      }
      if (incoming.get(2)) {
        struct.time = iprot.readI64();
        struct.setTimeIsSet(true);
      }
      if (incoming.get(3)) {
        {
          org.apache.thrift.protocol.TList _list13 = new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.STRUCT, iprot.readI32());
          struct.queries = new ArrayList<Query>(_list13.size);
          for (int _i14 = 0; _i14 < _list13.size; ++_i14)
          {
            Query _elem15; // required
            _elem15 = new Query();
            _elem15.read(iprot);
            struct.queries.add(_elem15);
          }
        }
        struct.setQueriesIsSet(true);
      }
    }
  }

}

