package org.mpi_sws.pig.common.entity;

import java.net.InetSocketAddress;

import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.apache.thrift.transport.TTransportException;
import org.mpi_sws.pig.common.Common;

/**
 * A non-blocking server
 * @author Ruichuan Chen
 *
 */
public class Server {
	
	private TServer server = null;
	
	public Server (InetSocketAddress bindAddr, int clientTimeout,
			TProtocolFactory protocol, TProcessor processor,
			int nSelectorThreads, int nWorkerThreads) throws TTransportException {
		
		TNonblockingServerTransport transport = new TNonblockingServerSocket(bindAddr, clientTimeout);
		TThreadedSelectorServer.Args args = new TThreadedSelectorServer.Args(transport);
		args.transportFactory(new TFramedTransport.Factory(Common.max_frame_size));
		args.protocolFactory(protocol);
		args.processor(processor);
		args.selectorThreads(nSelectorThreads);
		args.workerThreads(nWorkerThreads);
		server = new TThreadedSelectorServer(args);
	}
	
	public void start () {
		if (server != null) {
			server.serve();
		}
	}
	
	public void stop () {
		if (server != null) {
			server.stop();
		}
	}
	
	protected void finalize () throws Throwable {
		try {
			this.stop();
		} finally {
			super.finalize();
		}
	}
}
