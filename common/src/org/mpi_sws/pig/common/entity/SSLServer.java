package org.mpi_sws.pig.common.entity;

import java.net.InetAddress;

import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TTransportException;

/**
 * A blocking SSL server
 * @author Ruichuan Chen
 *
 */
public class SSLServer {
	
	private TServer server = null;
	
	public SSLServer (String keyStore, String keyPass,
			InetAddress ifAddress, int port, int clientTimeout,
			TProtocolFactory protocol, TProcessor processor,
			int minWorkerThreads, int maxWorkerThreads) throws TTransportException {
		
		TSSLTransportFactory.TSSLTransportParameters parameters = new TSSLTransportFactory.TSSLTransportParameters();
		parameters.setKeyStore(keyStore, keyPass);
		
		TServerTransport transport = TSSLTransportFactory.getServerSocket(port, clientTimeout, ifAddress, parameters);
		TThreadPoolServer.Args args = new TThreadPoolServer.Args(transport);
		args.protocolFactory(protocol);
		args.processor(processor);
		args.minWorkerThreads(minWorkerThreads);
		args.maxWorkerThreads(maxWorkerThreads);
		server = new TThreadPoolServer(args);
	}
	
	public void start () {
		if (server != null) {
			server.serve();
		}
	}
	
	public void stop () {
		if (server != null) {
			server.stop();
		}
	}
	
	protected void finalize () throws Throwable {
		try {
			this.stop();
		} finally {
			super.finalize();
		}
	}
}
