package org.mpi_sws.pig.common.utility;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentMap;

/**
 * 
 * @author Ruichuan Chen
 *
 * @param <K>
 * @param <V>
 */
public class GarbageCollection<K, V> {
	
	public Set<K> collect (ConcurrentMap<K, V> map, Set<K> set) {
		Set<K> newSet = new HashSet<K>();
		for (Iterator<Entry<K, V>> it = map.entrySet().iterator(); it.hasNext(); ) {
			K key = it.next().getKey();
			if (set.contains(key)) {
				it.remove();
			} else {
				newSet.add(key);
			}
		}
		return newSet;
	}
}
