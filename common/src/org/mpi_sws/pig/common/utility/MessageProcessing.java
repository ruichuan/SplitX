package org.mpi_sws.pig.common.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class MessageProcessing {
	
	private Random rnd = new Random (SeedGeneration.generate());
	
	public static byte[] join (byte[] b1, byte[] b2) {
		byte[] bJoin = new byte[b1.length];
		for (int i = 0; i < b1.length; ++i) {
			bJoin[i] = (byte) (b1[i] ^ b2[i]);
		}
		return bJoin;
	}
	
	public List<byte[]> split (byte[] b) {
		byte[] b1 = new byte[b.length];
		byte[] b2 = new byte[b.length];
		
		rnd.nextBytes(b1);
		for (int i = 0; i < b.length; ++i) {
			b2[i] = (byte) (b[i] ^ b1[i]);
		}
		
		List<byte[]> s = new ArrayList<byte[]>(2);
		s.add(b1);
		s.add(b2);
		return s;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		byte[] b1 = new byte[]{1, 2};
		byte[] b2 = new byte[]{-128, 2};
		byte[] b3 = join(b1, b2);
		
		System.out.println(b3[0]);
		System.out.println(b3[1]);
		System.out.println(b3.length);

	}

}
