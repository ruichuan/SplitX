package org.mpi_sws.pig.common.utility;

import java.security.SecureRandom;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class SeedGeneration {
	
	public static long generate () {
		byte[] s = SecureRandom.getSeed(8);
		long seed = 0;
		for (int i = 0; i < 8; ++i) {
			seed = (seed << 8) + s[i];
		}
		return seed;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		for (int i = 0; i < 10; ++i) {
			System.out.println(generate());
		}

	}

}
