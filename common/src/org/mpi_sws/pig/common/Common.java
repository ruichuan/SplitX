package org.mpi_sws.pig.common;

/**
 * 
 * @author Ruichuan Chen
 *
 */
public class Common {
	
	public static String aggregator_host = "spig-a.mpi-sws.org";
	public static int aggregator_receiving_port = 51083;
	
	public static String mix_1_host = "spig-m1.mpi-sws.org";
	public static int mix_1_receiving_port = 51083;
	public static int mix_1_proxying_port = 80;
	
	public static String mix_2_host = "spig-m2.mpi-sws.org";
	public static int mix_2_receiving_port = 51083;
	public static int mix_2_proxying_port = 80;
	
	public static int special_bytes_in_answer = 1;	// # special bytes in client answer
	public static long garbage_collection_interval = 10 * 60 * 1000;	// GC every 10 minutes
	public static int max_frame_size = Integer.MAX_VALUE;	// the maximum byte-length of a message
}
